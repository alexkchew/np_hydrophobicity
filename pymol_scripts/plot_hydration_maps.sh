# plot_hydration_maps.sh
# This script contains code to generate isosurface given hydration maps, etc.
## YOU NEED TO GENERATE NANOPARTICLE PDB WITH TRJCONV
# gmx trjconv -s sam_prod.tpr -f sam_prod.gro -o output_files/sam_prod.pdb
# Select 'non-Water'


# RUNNING CODE
# run S:\np_hydrophobicity_project\pymol_scripts\plot_hydration_maps.sh
# 
# run /Volumes/shared/np_hydrophobicity_project/pymol_scripts/plot_hydration_maps.sh
# print_png("DOD_isosurface")
## REINITIALIZE
cmd.reinitialize()

## SETTING DEFAULTS
# bg white
## BACKGROUND WHITE
cmd.bg_color(color="white")
cmd.set("line_as_cylinders", 1)
cmd.set("line_radius", 0.5)
cmd.set("stick_ball", 1)
cmd.set("stick_ball_ratio", 1.4)
cmd.set("stick_fixed_radius", 1)
cmd.set("stick_radius", 1.0)
cmd.set("sphere_scale", 1.0)
cmd.set("specular", "off")

## TURNING OFF DEPTH CUE
cmd.set("depth_cue", 0) # 1 to turn on

## IMPORTING MODULES
import os

### FUNCTION
def print_png_for_isosurface(ligand=ligand, main_sim = main_sim, suffix=suffix):
    ## PRINTING SURFACE
    print_png(main_sim + '-' + ligand + "_isosurface_" + suffix)
    cmd.hide("surface", "SURFACE")
    ## PRINTING SURFACE
    print_png(main_sim + '-' + ligand + "_alone_" + suffix)
    ## SHOWING SURFACE AGAIN
    cmd.show("surface", "SURFACE")
    return

#####################
### LOADING FILES ###
#####################

## DEFINING PATH TO SIM
path_sim=r"/Volumes/shared/np_hydrophobicity_project/simulations"
# r"S:\np_hydrophobicity_project\simulations"

## DEFINING NANOPARTICLE
np_file="sam_prod_0.pdb"
# "sam_prod_hydration_map.pdb"

## DEFINING NANOPARTICLE PATH
path_to_np=os.path.join(path_sim,
                        main_sim,
                        np_location)

## DEFINING IF YOU WANT SURFACE
want_surface=True

## LOOPING THROUGH LIST
#python

## CREATING LIST OF OUTPUT FILE
output_files_list_full = [ each_file + output_files_suffix for each_file in output_files_list]

## LOADING NP FILE
cmd.load(os.path.join(path_to_np, np_file))

## LOADING EACH OUTPUT FILE
for output_files in output_files_list_full:

    ## DEFINING NANOPARTICLE PATH
    path_to_surface=os.path.join(path_to_np,
                                 output_files,
                                 surface_file)
    ## LOADING THE SURFACE
    if want_surface is True:
        cmd.load(path_to_surface, output_files)

## HIDING EVERYTHING
cmd.hide()

#python end

#################
### MAIN CODE ###
#################

## ATOM SELECTION
if want_planar is False:
    cmd.select("AUNP", "name Au")
else:
    cmd.select("AUNP", "name Au or name BAu")

cmd.select("LIGAND", "not resname SUR and not name Au")
## DEFINING SULFUR ATOMS
cmd.select("SULFURS", "LIGAND and name S1")
cmd.select("SURFACE", "resname SUR")
#  and not AUNP and not LIGAND"

## SHOWING GOLD
cmd.show("spheres", "AUNP")
# cmd.color("gold","AUNP")
if want_planar is False:
    category_dir="EAM"
    diameter=diameter
    trial_num="1"
    ## COLOR GOLD ATOMS
    # pickle_color_gold_atoms(want_defaults=True)
    pickle_color_gold_atoms(want_defaults=False,
                            category_dir=category_dir,
                            diameter=diameter,
                            trial_num=trial_num)
else:
    ## COLOR DEFAULTS
    cmd.color("gold","AUNP")


## SHOWING SULFUR
cmd.show("spheres", "SULFURS")
cmd.color("yellow","SULFURS")

## SHOWING LIGANDS
# cmd.show("lines", "LIGAND")
# cmd.show("spheres", "LIGAND")
cmd.show("lines", "LIGAND")
cmd.hide("(hydro)")

## COLORING GRAY
cmd.color("grey50", "LIGAND and name C*")

## STORING
# print_png(ligand + "_alone")

#######################
### SHOWING SURFACE ###
#######################

## MESH
#cmd.show("mesh", "SURFACE")
#cmd.set("mesh_width", 1.5)
# TRANSPARENCY
transparency=0
# 0
# ISOSURFACE
if want_surface is True:
    
    cmd.show("surface", "SURFACE") # ISOSURFACE
    ## COLORING SPECTRUM
    cmd.spectrum("b","red_white_blue", "SURFACE", minimum=0, maximum=1)
    ## SETTING TRANSPARENCY
    cmd.set("transparency", transparency, "SURFACE" )
    ## PROBE RADIUS
    cmd.set("solvent_radius", 1.4) # default is 1.4
    ## SETTING SURFACE QUALITY
    cmd.set("surface_quality", 1) # 1 is default
    ## UPDATING SURFACE
    # cmd.refresh()


## ZOOOMING
if want_planar is True:
    cmd.turn("x", -70)
    ## ZOOMING
    cmd.zoom(complete=1)
else:
    # SPHERICAL NPS
    # ORIENT ALONG PRINCIPAL AXIS
    cmd.orient("LIGAND")
    cmd.turn("z", -90)
    cmd.zoom(complete=1, buffer= 5)
