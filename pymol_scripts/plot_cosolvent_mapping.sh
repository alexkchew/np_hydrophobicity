# plot_cosolvent_mapping.sh
# The purpose of this code is to generate isosurface for peptide mapping. 
# 
# RUNNING CODE
# run S:\np_hydrophobicity_project\pymol_scripts\plot_cosolvent_mapping.sh
# # PRINTING
#   print_png_cosolvent_mapping()

# run /Volumes/shared/np_hydrophobicity_project/pymol_scripts/plot_hydration_maps.sh
# print_png("DOD_isosurface")
## REINITIALIZE
cmd.reinitialize()

## SETTING DEFAULTS
# bg white
## BACKGROUND WHITE
cmd.bg_color(color="white")
cmd.set("line_as_cylinders", 1)
cmd.set("line_radius", 0.5)
cmd.set("stick_ball", 1)
cmd.set("stick_ball_ratio", 1.4)
cmd.set("stick_fixed_radius", 1)
cmd.set("stick_radius", 1.0)
cmd.set("sphere_scale", 1.0)
cmd.set("specular", "off")

## TURNING OFF DEPTH CUE
cmd.set("depth_cue", 0) # 1 to turn on

## IMPORTING MODULES
import os

## DEFINING SIMULATIONS
path_sims=r"R:\scratch\nanoparticle_project\simulations\191017-mixed_sams_most_likely_sims"

## DEFINING COSOLVENT MAPPING
cosolvent_map_folder="cosolvent_mapping"
input_pdb="goldnp.pdb"
# "sam_prod-gold_lig_only-10000.pdb"


## DEFINING LIGAND
ligand="C11COOH"
# "dodecanethiol"
# "C11COOH"
# "dodecanethiol"
# "C11COOH"

## DEFINING LIGAND LOCATION
np_location="MostNP-EAM_300.00_K_2_nmDIAM_"+ligand+"_CHARMM36jul2017_Trial_1-lidx_1-cosf_10000-aceticacid_formate_methylammonium_propane_1_molfrac_300"

## DEFINING TYPE OF RESIDUES
residue_list=["ACA","FMT","HOH","MTA","PRO"]

## LOADING SAM 
sam_pdb_path=os.path.join(path_sims,np_location, cosolvent_map_folder, input_pdb)
## LOADING
cmd.load(sam_pdb_path)
## HIDING EVERYTHING
cmd.hide()

## PREFIX
pdb_prefix="cosolvent_map_" + ligand +"_"

##################################
### DEFINING COLORS DICTIONARY ###
##################################
## DEFINING SOLVENT DICTIONARY
SOLVENT_COLOR_DICT={
        'HOH': 'blue',
        'FMT': 'pink',
        'MTA': 'cyan',
        'PRO': 'red',
        'ACA': 'green',
        }

## LOOPING AND LOADING
for residue in residue_list:
    ## DEFINING PDB
    pdb_file_name=pdb_prefix+residue+".pdb"
    ## DEFINING PATH
    pdb_path=os.path.join(path_sims, pdb_file_name) # np_location
    ## LOADING
    cmd.load(pdb_path)
    ## HIDING EVERYTHING
    cmd.hide()

#################
### MAIN CODE ###
#################
cmd.select("AUNP", "name Au")
cmd.select("LIGAND", "not resname SUR and not name Au")
## DEFINING SULFUR ATOMS
cmd.select("SULFURS", "LIGAND and name S1")

## SHOWING GOLD
cmd.show("spheres", "AUNP")
# cmd.color("gold","AUNP")

## COLOR GOLD ATOMS
pickle_color_gold_atoms(want_defaults=True)

## SHOWING SULFUR
cmd.show("spheres", "SULFURS")
cmd.color("yellow","SULFURS")

## SHOWING LIGANDS
# cmd.show("lines", "LIGAND")
# cmd.show("spheres", "LIGAND")
cmd.show("lines", "LIGAND")
cmd.hide("(hydro)")

## COLORING GRAY
cmd.color("grey50", "LIGAND and name C*")
#
### SHOWING SULFUR
#cmd.show("spheres", "SULFURS")
#cmd.color("yellow","SULFURS")
#
### SHOWING LIGANDS
## cmd.show("lines", "LIGAND")
#cmd.show("spheres", "LIGAND")
#cmd.hide("(hydro)")

## TRANSPARENCY PLAN
transparancy_value  = 0
# 0.10
# 0.20

## ZOOMING
cmd.zoom("center", 40, state=0)


## GETTING THE SURFACE
for residue in residue_list:
    ## CREATING SELECTION
    cmd.select( "SURFACE_"+residue,pdb_prefix+residue)
    ## DEFINING SURFACE NAME
    surface_name = "SURFACE_"+residue
    ## DEFINING COLOR
    color = SOLVENT_COLOR_DICT[residue]
    ## CREATING PLOT
    cmd.spectrum("b","white_" + color , surface_name, minimum=0, maximum=1) # "white_" 
    ## SETTING TRANSPARENCY
    cmd.set("transparency", transparancy_value, surface_name)

### FUNCTION TO PRINT COSOLVENT MAPPING
def print_png_cosolvent_mapping(ligand=ligand, residue_list=residue_list):
    for idx, residue in enumerate(residue_list):
        ## DEFINING SURFACE NAME
        surface_name = "SURFACE_"+residue
        ## GENERATING
        cmd.show("surface", surface_name)
        ## ZOOMING ONE TIME
        # if idx == 0:
        #     cmd.zoom(surface_name)
        ## PRINTING SURFACE
        print_png(ligand + "_" + residue + "_isosurface")
        ## HIDING SURFACE
        cmd.hide("surface", surface_name)
    ## AT THE END, PRINT ITSELF
    print_png(ligand + "_alone")
    
    return



