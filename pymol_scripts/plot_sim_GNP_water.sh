# plot_sim_GNP_water.sh
# This script plots GNP in pure water.
## YOU NEED TO GENERATE NANOPARTICLE PDB WITH TRJCONV
# gmx trjconv -s sam_prod.tpr -f sam_prod.gro -o output_files/sam_prod.pdb
# Select 'non-Water'

#  gmx trjconv -s sam_prod.tpr -f sam_prod.xtc-o output_files_test/sam_prod_first_30ps.pdb -b 0 -e 30
# RUNNING CODE
# run S:\np_hydrophobicity_project\pymol_scripts\plot_hydration_maps.sh
# print_png("DOD_isosurface")
## REINITIALIZE
cmd.reinitialize()

## SETTING DEFAULTS
# bg white
## BACKGROUND WHITE
cmd.bg_color(color="white")
cmd.set("line_as_cylinders", 1)
cmd.set("line_radius", 0.5)
cmd.set("stick_ball", 1)
cmd.set("stick_ball_ratio", 1.4)
cmd.set("stick_fixed_radius", 1)
cmd.set("stick_radius", 1.0)
cmd.set("sphere_scale", 1.0)
cmd.set("specular", "off")

## TURNING OFF DEPTH CUE
cmd.set("depth_cue", 0) # 1 to turn on

## IMPORTING MODULES
import os

#####################
### LOADING FILES ###
#####################

## DEFINING NANOPARTICLE FOLDER NAME
np_location="np_spherical_300K_2_nm_dodecanethiol_CHARMM36_mlc"
# np_location="np_spherical_300K_2_nm_C11OH_CHARMM36_mlc"
np_location="np_spherical_300K_2_nm_C11COOH_CHARMM36_mlc"
output_files="output_files_test"

## DEFINING NANOPARTICLE PATH
path_to_np=os.path.join(r"S:\np_hydrophobicity_project\simulations\NP_SPHERICAL",
                        np_location,
                        output_files)

## DEFINING NANOPARTICLE
np_file="sam_prod.pdb"
surface_file="sam_prod_hydration_map.pdb"

## LOADING FILE
cmd.load(os.path.join(path_to_np,np_file))
cmd.load(os.path.join(path_to_np,surface_file))

## HIDING EVERYTHING
cmd.hide()

#################
### MAIN CODE ###
#################

## ATOM SELECTION
cmd.select("AUNP", "name Au")
cmd.select("LIGAND", "not resname SUR and not name Au")
## DEFINING SULFUR ATOMS
cmd.select("SULFURS", "LIGAND and name S1")
cmd.select("SURFACE", "resname SUR and not AUNP and not LIGAND")

## SHOWING GOLD
cmd.show("spheres", "AUNP")
cmd.color("gold","AUNP")

## SHOWING SULFUR
cmd.show("spheres", "SULFURS")
cmd.color("yellow","SULFURS")

## SHOWING LIGANDS
# cmd.show("lines", "LIGAND")
cmd.show("spheres", "LIGAND")
cmd.hide("(hydro)")

## SHOWING SURFACE
## MESH
#cmd.show("mesh", "SURFACE")
#cmd.set("mesh_width", 1.5)
# ISOSURFACE
cmd.show("surface", "SURFACE") # ISOSURFACE
## COLORING SPECTRUM
cmd.spectrum("b","red_white_blue", "SURFACE", minimum=0, maximum=1)
## SETTING TRANSPARENCY
cmd.set("transparency", 0.15, "SURFACE" )
## PROBE RADIUS
cmd.set("solvent_radius", 1.4) # default is 1.4
## SETTING SURFACE QUALITY
cmd.set("surface_quality", 1) # 1 is default
## UPDATING SURFACE
# cmd.refresh()

