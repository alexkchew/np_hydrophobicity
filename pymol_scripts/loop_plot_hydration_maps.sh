# loop_plot_hydration_maps.sh
# The purpose of this script is to loop through hydration maps
# RUNNING CODE
#   run S:\np_hydrophobicity_project\pymol_scripts\loop_plot_hydration_maps.sh
#   for MAC: run /Volumes/shared/np_hydrophobicity_project/pymol_scripts/loop_plot_hydration_maps.sh
# --> printing for isosurface: print_png_for_isosurface()
# print_png_for_ligand <-- for ligand only
# print_png_for_isosurface <-- for isosurface

## DEFINING MAIN SIMULATION FOLDER
## REINITIALIZE
cmd.reinitialize()

## DEFINING MAIN SIMULATION
# main_sim="NP_SPHERICAL"

## 6 NM SIMS
main_sim="191025-Most_likely_np_sims_6_nm"

## CHARGED SIMS
main_sim="191010-Most_likely_np_sims_NVT_charged_groups"

## PEG SIMS
main_sim="191103-PEG_most_likely"

# main_sim="190920-Most_likely_np_sims_NVT_mixed_solvent_ROT_ligs"

## OTHER LIKELY INDEX
main_sim = "191121-last_index_most_likely"

## PLANAR SIM
#main_sim = "191205-Planar_SAMs_frozen_trial_2_NH2" ## CURRENT WORKFLOW
#main_sim=r"191206-Planar_SAMs_frozen_trial_3_translated"
#main_sim=r"191208-Planar_SAMs_trans_unfr"
#main_sim=r"191209-anneal"
main_sim=r"191210-annealing_try4"
main_sim=r"191214-annealing_try4_ch"
main_sim = r"191010-Most_likely_np_sims_NVT_charged_groups"
main_sim=r"NP_SPHERICAL"
main_sim=r"PLANAR"
main_sim=r"20200114-NP_HYDRO_FROZEN"
main_sim=r"NP_SPHERICAL_REDO"
main_sim=r"20200215-GNPspr600"
main_sim=r"20200224-planar_SAM_spr50"
main_sim=r"20200326-Planar_SAMs_with_larger_z_frozen_with_vacuum"
main_sim=r"20200224-GNP_spr_50"
# main_sim=r"20200328-Planar_SAMs_new_protocol-shorterequil_spr50"
#main_sim=r"20200215-GNPspr600_extended"
#main_sim=r"20200227-50_doub"
# r"20200215-planar_SAM_frozen_600spring"
# r"20200215-planar_SAM_frozen_600spring"
# r"NP_ROT"

## PLANAR SIM DIREC
planar_sim_dir = [ "191205-Planar_SAMs_frozen_trial_2_NH2",
                   "191123-Frozen_SAM_gold", 
                   "191206-Planar_SAMs_frozen_trial_3_translated", 
                   "191208-Planar_SAMs_trans_unfr",
                   "191209-anneal",
                   "191210-annealing_try4",
                   "191214-annealing_try4_ch",
                   "PLANAR",
                   "20200114-NP_HYDRO_FROZEN",
                   "20200215-planar_SAM_frozen_600spring",
                   "20200224-planar_SAM_spr50",
                   "20200326-Planar_SAMs_with_larger_z_frozen_with_vacuum",
                   "20200328-Planar_SAMs_new_protocol-shorterequil_spr50"]

## DEFINING LAST INDEX
last_index = ["191121-last_index_most_likely"]

if main_sim in planar_sim_dir:
    want_planar=True
else:
    want_planar=False

## DEFINING IF YOU WANT TO SAVE IMAGE
want_save_image=True
# False
# True
# False

# "C11NCH33", "C11COO", "C11NH2", "C11CCH33", "C11CF3",
## DEFINING LIST
ligand_list=["dodecanethiol"] # "dodecanethiol",
# "C11OH", "C11NH2", "C11CF3", "C11CONH2", "C11COOH"
# ,"C11OH", "C11NH2", "C11CF3", "C11CONH2", "C11COOH"]
# "C11double67OH","dodecen-1-thiol"
# ["dodecanethiol", "C11OH"]
#  ,"C11COOH" ,"C11NH2" ,"C11CF3", "C11CONH2"
# , "C11OH" ,"C11COOH" ,"C11NH2" ,"C11CF3", "C11CONH2"
# "C11COOH",
# "C11OH"
# "ROT004"
# "C11CF3"
#  "C11CONH2", "C11COOH", "C11NH2", "C11NH3", "C11OH", "dodecanethiol"
# "dodecanethiol"
# "C11NCH33"
# "C11CCH33", 
#  "C11COO", "C11NH3"
# "C11COOH", "C11CONH2" "dodecanethiol", "C11NH2", "C11OH", "C11CONH2"]

for ligand in ligand_list:
    ## DEFINING LIGAND
    # ligand="C11OH"
    # "dodecanethiol"
    # "C11COO"
    # "dodecanethiol"
    # "C11COO"
    # "C11COOH"
    # "C11NH3"
    # "dodecanethiol"
    # "ROTPE4"
    # "ROTPE3"
    # "ROTPE2"
    # "ROTPE1"
    # "ROT001"
    # "C11COOH"
    # "C11COO"
    # "C11OH"
    # "C11COOH"
    # "ROT008"
    # ROT005
    # "dodecanethiol"
    # "ROT006"

    ## DEFINING IF YOU WANT COUNTERION
    suffix="HOH_CL_NA"
    if want_planar is False:
#        suffix="-all_heavy-0-100000" # 150000        
#        surface_file="out_hydration-2_5.pdb" # 
        suffix="-all_heavy-0-50000" # 150000        
        surface_file="out_hydration-0_6.pdb" # 

    else:
        suffix="-all_heavy-0-150000" # 150000
        surface_file="out_hydration-5_12.pdb" # 
        suffix="-all_heavy-0-50000" # 150000        
        surface_file="out_hydration-0_6.pdb" # 

    surface_file="out_hydration-5_15.pdb" # 
    suffix="-all_heavy-2000-50000"

    ## DEFINING DIAMETER
    if main_sim == "191025-Most_likely_np_sims_6_nm":
        diameter="6"
    else:
        diameter="2" # "6"

    ## DEFINING INDEX
    if main_sim in last_index:
        index="401"
        index="200"
        index="300"
    else:
        index="1"

    ## DEFINING OUTPUT FILE
    output_files_list=["26"]
    # ["norm-0.70"] 
    ## DEFINING PROBE RADIUS
    probe_radius=0.33
    # 0.25
    
    # "30"
    # "25.6"
    # "8", "16", "20.8", "25.6", "28.8"
    output_files_suffix="-0.24-0.1,0.1,0.1-%.2f"%(probe_radius) + suffix

    ## DEFINING LIGAND LOCATION (SPHERICAL)
    ## PLANAR
    if want_planar is True:
        np_location="FrozenPlanar_300.00_K_" + ligand + "_10x10_CHARMM36jul2017_intffGold_Trial_1-50000_ps"

        ## CHANGING NP location
        if main_sim == r"191208-Planar_SAMs_trans_unfr" or main_sim==r"191209-anneal" or main_sim == "191210-annealing_try4" or main_sim == "191214-annealing_try4_ch" or main_sim == "PLANAR":
            np_location=r"FrozenGoldPlanar_300.00_K_" + ligand + "_10x10_CHARMM36jul2017_intffGold_Trial_1-50000_ps"
            
        if main_sim == r"20200215-planar_SAM_frozen_600spring":
            np_location=r"NVTspr_600_Planar_300.00_K_" + ligand + "_10x10_CHARMM36jul2017_intffGold_Trial_1-50000_ps"
            # r"FrozenGoldPlanar_300.00_K_" + ligand + "_10x10_CHARMM36jul2017_intffGold_Trial_1-50000_ps"
        elif main_sim == r"20200224-planar_SAM_spr50" \
             or main_sim == r"20200326-Planar_SAMs_with_larger_z_frozen_with_vacuum" \
             or main_sim == r"20200328-Planar_SAMs_new_protocol-shorterequil_spr50":
            np_location=r"NVTspr_50_Planar_300.00_K_" + ligand + "_10x10_CHARMM36jul2017_intffGold_Trial_1-5000_ps"

    else:
        np_location="MostlikelynpNVT_EAM_300.00_K_" + diameter + "_nmDIAM_" + ligand + "_CHARMM36jul2017_Trial_1_likelyindex_" + index
        if main_sim == r"20200224-planar_SAM_spr50":
            np_location="MostlikelynpNVTspr_600-EAM_300.00_K_" + diameter + "_nmDIAM_" + ligand + "_CHARMM36jul2017_Trial_1_likelyindex_" + index
        elif main_sim == r"20200224-GNP_spr_50" or main_sim == r"20200227-50_doub":
            np_location="MostlikelynpNVTspr_50-EAM_300.00_K_" + diameter + "_nmDIAM_" + ligand + "_CHARMM36jul2017_Trial_1_likelyindex_" + index
        elif main_sim == r"20200215-GNPspr600_extended":
            np_location="MostlikelynpNVTspr_600-EAM_300.00_K_" + diameter + "_nmDIAM_" + ligand + "_CHARMM36jul2017_Trial_1_likelyindex_" + index
        
    ## RUNNING HYDRATION MAP CODE
    # cmd.run(r"S:\np_hydrophobicity_project\pymol_scripts\plot_hydration_maps.sh")
    cmd.run(r"/Volumes/shared/np_hydrophobicity_project/pymol_scripts/plot_hydration_maps.sh")

    ## CENTER
    cmd.center("AUNP", origin = 1)
    # center_gold_
    # cmd.zoom("GOLD and LIGAND", 25, -1, 0)
    
    ## SAVING IMAGE
    if want_save_image is True:
        print_png_for_isosurface()

    ### FUNCTION TO PRINT PNG FOR LIGAND
    def print_png_for_ligand(ligand=ligand, diameter = diameter ):
        ## ZOOMING
        cmd.zoom("GOLD LIGAND", 25, 0, 1)
        ## PRINTING
        print_png(diameter + "_" + ligand + "alone")
        return  
