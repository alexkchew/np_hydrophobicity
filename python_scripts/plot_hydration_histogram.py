# -*- coding: utf-8 -*-
"""
plot_hydration_histogram.py
The purpose of this function is to plot the probability distribution 
across all mu values. 

Written by: Alex K. Chew (10/28/2019)

"""
import os

## GLOBAL VARIABLES
from global_vars import MAX_N, MU_MAX, MU_MIN

## CORE FUNCTIONS
from core_functions import create_grid, calc_mu, load_datafile, flatten_list_of_list, split_list

## IMPORTING PICKLE FUNCTIONS
from pickle_functions import pickle_results, load_pickle_results

import matplotlib.pyplot as plt
import numpy as np
import plot_functions as plot_funcs

## IMPORTING GLOB
import glob

## IMPORTING FUNCTIONS
from scipy import stats
import matplotlib.colors as mcolors
# Basics of statistics in python: http://benalexkeen.com/basic-statistics-in-python/

### FUNCTION TO EXTRACT FILE INFORMATION
def extract_most_likely_name(name):
    '''
    The purpose of this function is to extract the most likely name.
    INPUTS:
        name: [str]
            name of your interest, e.g.
                MostlikelynpNVT_EAM_300.00_K_2_nmDIAM_C11OH_CHARMM36jul2017_Trial_1_likelyindex_1
    OUTPUTS:
        name_dict: [dict]
            dictionary of the name details
    '''
    ## SPLITITNG
    name_split = name.split('_')
    # RETURNS:
    #   ['MostlikelynpNVT', 'EAM', '300.00', 'K', '2', 'nmDIAM', 'C11CONH2', 'CHARMM36jul2017', 'Trial', '1', 'likelyindex', '1']
    if name_split[0] != 'FrozenPlanar' and name_split[0] != 'FrozenGoldPlanar':
        name_dict = {
                'type': 'spherical',
                'temp': float(name_split[2]),
                'diam': float(name_split[4]),
                'ligand': name_split[6],
                'index' : name_split[-1],
                }
    else:
        name_dict = {
                'type': 'planar',
                'temp': float(name_split[1]),
                'diam': float(name_split[4].split('x')[0]),
                'ligand': name_split[3],
                'index' : '1'
                }
        
    return name_dict


### FUNCTION TO COMPUTE STATISTICS
def compute_stats(mu, bins, histo_range, step_size = 0.1, density=True, statistic_type="average"):
    '''
    This function computes the statistics for mu
    INPUTS:
        mu: [np.array]
            mu value array
        bins: [int]
            number of bins
        histo_range: [tuple]
            histogram range
        step_size: [float]
            step size for histogram
        density: [logical, default=True]
            True if you want density
        statistic_type: [str]
            statistic type:
                "average" -- average of the distribution
                "std" -- standard deviation of the distribution
                "mode" -- number of occurances that occur most often
                "median" -- median of the range of values
                "range" -- low and max of mu
                "max" --max value of mu
                "max_dist" -- maximum value of mu distribution
    
    '''
    if statistic_type == "average":
        value = np.mean(mu)
    elif statistic_type == "std":
        value = np.std(mu)
    elif statistic_type == "variance":
        value = np.var(mu)
    elif statistic_type == "mode":
        mode = stats.mode(mu)
        value = mode.mode[0]
    elif statistic_type == "median":
        value = np.median(mu)
    elif statistic_type == "range":
        value = np.ptp(mu)
    elif statistic_type == "max":
        value = np.max(mu)
    elif statistic_type == "kurtosis": # Fatness of the distribution
        from scipy.stats import kurtosis
        value = kurtosis(mu)
    elif statistic_type == "skewness":
        from scipy.stats import skew
        value = skew(mu)
    elif statistic_type == "max_dist":
        step_size = 0.1
        xs = np.arange( histogram_range[0], histogram_range[1], step_size ) + 0.5 * step_size
        ps = np.histogram( mu, bins = xs.size, range = ( histogram_range[0], histogram_range[1] ),
                           density = True )[0]
        ## GETTING MAXIMUM INDEX    
        max_index = np.argmax(ps)
        ## DEFINING MAXIMA VALUE
        value = xs[max_index] + step_size/2
    return value

#%%
if __name__ == "__main__":
    
    ## DEFINING MAXIMUM DISTRIBUTION
    max_N = MAX_N
    
    ### DEFINING SIMULATION DICTIONARY
    sim_dict={
            'neutral': r"NP_SPHERICAL",
            'charged': r"191010-Most_likely_np_sims_NVT_charged_groups",
            'PEG': r"191103-PEG_most_likely",
            'rot_ligs': r"190920-Most_likely_np_sims_NVT_mixed_solvent_ROT_ligs",
            '6nm': r"191025-Most_likely_np_sims_6_nm",
            'last_frame_compare': r"191121-last_index_most_likely",
            'planar_sams': r"191210-annealing_try4",
            'planar_sams_charged': r"191214-annealing_try4_ch",
                # r"191123-Frozen_SAM_gold",
            }    
    
    ## DEFINING MAIN SIMULATION
    MAIN_SIM_FOLDER=r"S:\np_hydrophobicity_project\simulations"
    
    ## PATH FOR FIG
    store_fig_loc = r"C:\Users\akchew\Box Sync\VanLehnGroup\2.Research Documents\Alex_RVL_Meetings\20191216\images\hydrophobicity_C11COOH_configs"
    # r"C:\Users\akchew\Box Sync\VanLehnGroup\2.Research Documents\Research_Presentations\191204-Research_presentation\images"
    # r"C:\Users\akchew\Box Sync\VanLehnGroup\2.Research Documents\Alex_RVL_Meetings\20191202\Images\Output_hydrophobicity"
    
    main_sim=r"S:\np_hydrophobicity_project\simulations\NP_SPHERICAL"
    ## DEFINING SIMULATION NAME
    sim_name=r"MostlikelynpNVT_EAM_300.00_K_2_nmDIAM_C11OH_CHARMM36jul2017_Trial_1_likelyindex_1"
    # r"MostlikelynpNVT_EAM_300.00_K_2_nmDIAM_C11CONH2_CHARMM36jul2017_Trial_1_likelyindex_1"
    ## GETTING MULTIPLE  WORKING DIRECTORY
    wd_folders = glob.glob(main_sim + '/*')
    
    ## DEFINING SAVE FIG
    save_fig=True
    
    ## MU PICKLE NAME
    pickle_name_mu = "mu.pickle" # For MU VALUES
    ## DEFINING GMX SELECTION OUTPUT WORKSLACE
    workspace = "hyd_analysis"
    
    ## DEFINING JOB TYPES
    job_type_list = ["NP_planar_sam_comparison"] # , "charge", "rotello" # "neutral", "charge", "PEG"
    '''
    "neutral" -- comparison across neutral distributions
    "rotello" -- comparison with respect to rotello ligands
    "charge" -- charge comparison between COO- and NH3+ terminated
    "6nm" -- 6 nm comparison for dodecanethiol
    "charge_compare_old" -- comparison betewen charge distributions of COO- and NH3+ terminated
    "PEG" -- compares PEG chains
    "planar_sams" -- planar SAMs
    "last_frame_compare" -- compares the last frame
    "NP_planar_sam_comparison" -- comparing NPs with planar SAMs
    '''
    # "charge_compare_old"
    from functools import reduce
    
    ## LOOPING THROUGH JOB LIST
    for job_type in job_type_list:
        ## CHECKING IF CHARGED
        want_charged = False
        ## CHECK IF YOU WANT ROTELLO LIGANDS
        want_rotello = False
        
        ## DECIDING ON JOB TYPE
        if job_type == "charge":    
            ## CHECKING IF CHARGED
            want_charged = True
        elif job_type == "rotello":
            ## CHECK IF YOU WANT ROTELLO LIGANDS
            want_rotello = True
        ## INCLUDING CHARGES
        if want_charged is True or job_type == "charge_compare_old":
            ## GETTING WD FOLDERS
            ligand_names = [ extract_most_likely_name(os.path.basename(each_folder))['ligand'] for each_folder in wd_folders]
            
            ## GETTING WD FOLDERS
            wd_folders_shrink = [ wd_folders[idx] for idx, lig_name in enumerate(ligand_names) 
                                 if lig_name == 'C11COOH' or lig_name == 'C11NH2' ]
            
            ## GETTING FOLDERS FROM OTHER DIRECTORY
            sim_name_charged = os.path.join(MAIN_SIM_FOLDER, sim_dict['charged'])
            wd_folders_charged = glob.glob(sim_name_charged + '/*')
            
            ## ADDING PLANAR CHARGED
            sim_name_planar_charged = os.path.join(MAIN_SIM_FOLDER, sim_dict['planar_sams_charged'])
            wd_folders_planar_charged = glob.glob(sim_name_planar_charged + '/*')
            
            ## APPENDING
            wd_folders = wd_folders_shrink + wd_folders_charged + wd_folders_planar_charged
            # .extend(wd_folders_charged)
        
        ## LOOPING ROTELLO LIGAND
        if want_rotello is True or \
                    job_type == "6nm" or \
                    job_type == "PEG" or \
                    job_type == "compare_frames" or \
                    job_type == "NP_planar_sam_comparison":
                        
            ## DESIRED 2 NM PARTICLES
            diam =[ 2 ]
            index = ["1"]
            desired_ligands = ['dodecanethiol', 'C11NH3', 'ROT001', 'ROT002', 'ROT003', 'ROT004', 'ROT005','ROT006', 'ROT007', 'ROT008', 'ROT009'] # 'ROT005', 
            if job_type == "6nm":
                desired_ligands = ['dodecanethiol', 'C11OH']
                diam =[ 2, 6 ]
            elif job_type == "PEG":
                desired_ligands = ['dodecanethiol', 'ROTPE1', 'ROTPE2', 'ROTPE3', 'ROTPE4', 'ROT001']
            elif job_type == "compare_frames":
                desired_ligands = ['C11COOH']
                index = ["1", "200", "300", "401"]
            elif job_type == "NP_planar_sam_comparison":
                desired_ligands = ['dodecanethiol', 'C11COOH', 'C11NH2', 'C11CONH2', 'C11OH' ]
                diam =[ 2, 10 ]
                index = ["1"]
                
            # ['dodecanethiol', 'C11OH', 'C11NH3', 'C11NH2', 'ROT001']
            ## LOOPING AND GETTING ALL WORKING DIRECTORIES
            wd_folder_list = flatten_list_of_list([ glob.glob( os.path.join( MAIN_SIM_FOLDER, sim_dict[each_dir] + '/*')  )
                                                              for each_dir in sim_dict ])
            
            ## GETTING EXTRACTED DIRECTORIES
            extracted_directories = [ extract_most_likely_name(os.path.basename(each_folder)) for each_folder in wd_folder_list]
            
            ## GETTING ALL FOLDER NAMES
            ligand_names = [ each_folder['ligand'] for each_folder in extracted_directories]
            diam_list = [ each_folder['diam'] for each_folder in extracted_directories]
            index_list = [ each_folder['index'] for each_folder in extracted_directories]
            
            ## GETTING LIGANDS (ORDER)
            ligand_names_desired = [each_ligand for each_ligand in desired_ligands if each_ligand in ligand_names]
            
            ## GETTING FILES ONLY IF
            wd_folders = []
            for idx, lig_name in enumerate(ligand_names_desired):
                ## GETTING THE LIGAND INDEX
                lig_index = [ idx for idx, each_lig in enumerate(ligand_names) if each_lig == lig_name]
                diam_index = [ idx for idx, each_diam in enumerate(diam_list) if each_diam in diam]
                ndx_index = [ idx for idx, each_index in enumerate(index_list) if each_index in index]
                ## GETTING INDEX
                wd_folder_index = reduce(np.intersect1d, (lig_index, diam_index, ndx_index))
                # np.intersect1d(lig_index, diam_index, ndx_index) # [0]
                ## APPENDING
                for each_index in wd_folder_index:
                    wd_folders.append(wd_folder_list[each_index])
        
        ## PLANAR SAMS
        if job_type == "planar_sams":
            ## GETTING LIST
            wd_folder_list = flatten_list_of_list([ glob.glob( os.path.join( MAIN_SIM_FOLDER , sim_dict[each_dir]) + '/*' ) for each_dir in sim_dict ])
            sim_path = os.path.join(MAIN_SIM_FOLDER, sim_dict[job_type])
            wd_folders_planar = glob.glob(sim_path + '/*')
            wd_folders.append(wd_folders_planar)
            
        
    
        ## DROPPING FOLDER
        # del wd_folders[2]
        # .remove(r'S:\\np_hydrophobicity_project\\simulations\\NP_SPHERICAL\\MostlikelynpNVT_EAM_300.00_K_2_nmDIAM_C11NH2_CHARMM36jul2017_Trial_1_likelyindex_1')
        

        
        #%%
        ## DEFINING WORKING DIRECTORY
        # wd = os.path.join(main_sim, sim_name)
        
        ## DEFINING PLANAR DESIRED
        if job_type == "planar_sams":
            want_planar = True
            ## DEFINING PATH TO GRID
            grid_folder="grid-0_1000"
            ## DEFINING GRID FILE
            grid_file = "out_willard_chandler.dat"
            
        else:
            want_planar = False
        
        ################################
        ### EXTRACTING ALL MU VALUES ###
        ################################
        
        ## STORING MU
        mu_list = []
    
        ## LOOPING
        for wd in wd_folders:
            ## PRINTNIG
            print(wd)
            
            ## GETTING BASENAME
            wd_extracted = extract_most_likely_name(os.path.basename(wd))
            # if wd_extracted['type'] == 'planar':
                
            ## DEFINING PATH MU PICKLE
            path_mu_pickle = os.path.join(wd, workspace, pickle_name_mu)
            
            ## LOADING PICKLE RESULTS
            unnorm_p_N = load_pickle_results(path_mu_pickle)[0][0]
            
            ## CALCULATING MU
            mu = calc_mu( unnorm_p_N, d = max_N )
                        
            # ## LOADING 
            # if want_planar is True:
            #     ## DEFINING PATH TO GRID
            #     path_grid=os.path.join(wd, workspace, grid_folder, grid_file)
            #     ## LOADING GRID
            #     grid = load_datafile(path_to_file = path_grid)            
            #     ## FINDING MASK
            #     avg_z = grid[:,2].mean()
            #     mask = grid[:,2] < avg_z
            #     mu = mu[mask]
                
            ## STORING MU
            mu_list.append(mu)
            
        #%%
        ## SEEING IF YOU WANT OLD CHARGES
        if job_type == "charge_compare_old":
            ## DEFINING FOLDER 
            mapping_data = os.path.join("hydra-0_50000-0.1", "out_hydration_map.dat" )
            
            ## FINDING ALL WORKSPACES WITH THAT
            old_charges_ligands = [ "C11COO", "C11NH3" ]
            
            ## GETTING ALL FOLDER NAMES
            ligand_names = [ extract_most_likely_name(os.path.basename(each_folder))['ligand'] for each_folder in wd_folders]
            
            ## GETTING FILES ONLY IF
            wd_folders_old = [ wd_folders[idx] for idx, lig_name in enumerate(ligand_names) if lig_name in old_charges_ligands ]
            
            ## CREATING MU CHARGED
            mu_charged_list = []
            
            ## LOOPING THROUGH AND GETTING MU VALUE
            for each_folder in wd_folders_old:
                ## PATH TO MAP
                path_map = os.path.join(each_folder, mapping_data)
                ## LOADING THE DATA
                mu_data = load_datafile( path_map )[:,-1]
                ## APPENDING
                mu_charged_list.append(mu_data)
            
        
        #%%
        # if __name__ == "__main__":
        ## CREATING PLOT
        fig, ax = plot_funcs.create_plot()
    
        ## DEFINING COLOR DICT
        # COLORS: https://matplotlib.org/3.1.0/gallery/color/named_colors.html
        color_dict = {
                'dodecanethiol': 'black',
                'C11OH': 'red',
                'C11COOH': 'magenta',
                'C11NH2': 'blue',
                'C11CONH2': 'cyan',
                'C11COO': 'orange',
                'C11NH3': 'green',
                'ROT001': 'purple',
                'ROT002': 'olive',
                'ROT003': 'lightblue',
                'ROT004': 'goldenrod',
                'ROT005': 'magenta',
                'ROT006': 'maroon',
                'ROT007': 'slategrey',
                'ROT008': 'indigo',
                'ROT009': 'chocolate',
                'ROTPE1': 'greenyellow',
                'ROTPE2': 'powderblue',
                'ROTPE3': 'darksalmon',
                'ROTPE4': 'deeppink',
                }
        
        ## DEFINING DIAMETER DICT
        diam_dict = {
                '2.0': '-',
                '6.0': '--',
                '10.0': '--',
                'planar': ':',
                }
        
        ## DEFINING DICT (MARKERS)
        index_dict = {
                '1': '.',
                '200': 'o',
                '300': 'P',
                '401': 'v',
                }    
    
        ## DEFINING HISTOGRAM RANGE
        histogram_range = [ 5, 30 ]
        # histogram_range = [ 5, 30 ]
        
        ## STORING
        ligand_name_list = []
        max_value_list = []
        
        ## DEFINING TYPE
        statistic_type="mode"
        # "median"
        # "average"
        
        ## DEFINING BIN SIZE
        step_size = 0.2
        xs = np.arange( histogram_range[0], histogram_range[1], step_size ) + 0.5 * step_size
        
        ## CREATING STORAGE DICT
        storage_dict={'x_values':xs[:]}
        
        ## LOOPING THROUGH
        for idx, mu in enumerate(mu_list):
            ## GETTING NAME OF THE FILE
            extracted_name = extract_most_likely_name(os.path.basename(wd_folders[idx]))
            ligand_name = extracted_name['ligand']
            ## GETTING DIAMETER
            diam = extracted_name['diam']
            
            ## GETTING INDEX
            try:
                index = extracted_name['index']
            except KeyError:
                index = '1'
            
            ## FINDING COLOR
            color = color_dict[ligand_name]
            
            ## DEFINING TYPE
            linestyle = diam_dict[str(diam)]
            
            ## DEFINING INDEX DICT
            markers = index_dict[index]
                
            ## DEFINING HISTOGRAM
            ps = np.histogram( mu, bins = xs.size, 
                              range = ( histogram_range[0], histogram_range[1] ),
                              density = True )[0]
            
            ## STORING
            storage_dict[ligand_name] = ps[:]
            
            ## DEFINING LABEL
            label = str(diam)+ ' nm-' + ligand_name
            if job_type == "compare_frames":
                label = str(diam)+ ' nm-' + ligand_name + '-' + index
            if extracted_name['type'] == 'planar':
                label = 'planar-' + ligand_name
            ## CREATING HISTOGRAM PLOT
            ax.plot(xs, ps, linestyle = linestyle, linewidth=2, marker = markers, color = color, label = label)
            
            # value = compute_stats(mu, bins =  xs.size, histo_range = histogram_range, step_size = 0.1, density=True, statistic_type="average")
    
            ## PLOTTING VERTICAL LINE TO SHOW MAXIMA
            # hor_line = ax.axvline(value, linestyle = '--', color = color)
            
            ## STORING
            ligand_name_list.append(ligand_name)
        
        ## DESIRED 
        want_planar_sam_addition = True
        
        ## ADDING PLANAR SAM DATA
        if job_type == "6nm" and want_planar_sam_addition is True:
            ## ADDING PLANAR SAM
            planar_sam_path=r"C:\Users\akchew\Box Sync\VanLehnGroup\2.Research Documents\Research_Presentations\191204-Research_presentation\images\From_Brad_Hydrophobicity\planar_mu_dist.csv"
            import pandas as pd
            planar_sam_data = pd.read_csv(planar_sam_path)
            
            ## DEFINING XS
            xs_planar = planar_sam_data['mu'].to_numpy()
            
            ## PLANAR SAM DATA
            mu_planar_array= []
            
            ## GETTING DATA
            for each_lig in desired_ligands:
                ## APPENDING
                mu_planar_array.append( planar_sam_data[each_lig].to_numpy() )
            
            ## LOOPING THROUGH
            for idx, ps in enumerate(mu_planar_array):
                ## GETTING NAME OF THE FILE
                ligand_name = desired_ligands[idx]
                ## FINDING COLOR
                color = color_dict[ligand_name]
                ## DEFINING TYPE
                linestyle = diam_dict['planar']
                
                ## DEFINING LABEL
                label = 'Planar_SAM-' + ligand_name
                
                ## CREATING HISTOGRAM PLOT
                ax.plot(xs_planar, ps, linestyle, linewidth=2, color = color, label = label)
                
        ## COMPARISON TO OLD
        if job_type == "charge_compare_old":
            ## LOOP THROUGH
            for idx, mu in enumerate(mu_charged_list):
                ## GETTING LIGAND BASENAME
                ligand_name = extract_most_likely_name(os.path.basename(wd_folders_old[idx]))['ligand']
                ## GETTING DIAMETER
                diam = extract_most_likely_name(os.path.basename(wd_folders_old[idx]))['diam']
                
                ## FINDING COLOR
                color = color_dict[ligand_name]
                
                ## DEFINING TYPE
                linestyle = '.'
                # diam_dict[str(diam)]
                
                ## DEFINING HISTOGRAM
                ps = np.histogram( mu, bins = xs.size, range = ( histogram_range[0], histogram_range[1] ),
                                   density = True )[0]
                
    
                ## STORING
                storage_dict[ligand_name] = ps[:]
                
                ## DEFINING LABEL
                label = str(diam)+ ' nm-' + ligand_name +'_nocounterions'
                
                ## CREATING HISTOGRAM PLOT
                ax.plot(xs, ps, linestyle, linewidth=2, color = color, label = label)
                
            
        ## DRAWING AX LABEL
        ax.legend()
        
        ## ADDING LABEL
        ax.set_xlabel("mu")
        ax.set_ylabel("P(mu)")
        
        ## DEFINING HISTOGRAM NAME
        histogram_name = "histogram_" + job_type
        
        ## STORING FIG
        plot_funcs.store_figure( fig = fig,
                                 path = os.path.join(store_fig_loc,
                                                     histogram_name), # _charged
                                 save_fig = save_fig
                                 )
    
        #%%
        ## STORING
#        import pandas as pd
#        storage_df = pd.DataFrame(storage_dict)
#        
#        ## CSV
#        csv_location= os.path.join( r"C:\Users\akchew\Box Sync\VanLehnGroup\0.Manuscripts\NP_hydrophobicity_manuscript\Excel_Spreadsheet\outputcsv",
#                                    histogram_name + ".csv")
#        
#        ## OUTPUTING
#        storage_df.to_csv(csv_location)
        
    
        #%%
    
        ## DEFINING DESIRED STATISTIC
        desired_statistic_list = [ 'average', 'mode', 'median', 'range', 'max', 'max_dist', 'std',
                                   'variance', 'skewness', 'kurtosis']
        ## DEFINING STATISTICS
        statistics_dict = {}    
        ## LOOPING THROUGH EACH MU VALUE
        for idx, mu in enumerate(mu_list):
            ## GETTING NAME OF THE FILE
            ligand_name = extract_most_likely_name(os.path.basename(wd_folders[idx]))['ligand']
            ## LOOPING THROUGH STATISTICS
            statistics_results ={ each_stats: compute_stats(mu, 
                                                 bins =  
                                                 xs.size, 
                                                 histo_range = histogram_range, 
                                                 step_size = 0.1, 
                                                 density=True, 
                                                 statistic_type=each_stats)
                                    for each_stats in desired_statistic_list}
            
            statistics_dict[ligand_name] = statistics_results.copy()
            
        
        ## CREATING PLOT
        fig, ax = plot_funcs.create_plot()
        
        ## DEFINING INDICES
        ind = np.arange(len(desired_statistic_list))
        ## NUMBER OF LIGANDS
        num_ligands = len(ligand_name_list)
            
        ## DEFINING WIDTH
        width = 0.1
        
        ## CHANGING WIDTH FOR LARGE NUMBER OF LIGANDS
        if num_ligands >=9:
            width=0.08
        
        ## LOOPING THROUGH EACH LIGAND
        for idx, ligand in enumerate(ligand_name_list):
            ## GETTING VALUES
            stat_values = [ statistics_dict[ligand][each_key] for each_key in desired_statistic_list]
            ## GENERATING X INDEX
            x_indexes = ind + width * idx
            ## DEFINING COLOR
            color = color_dict[ligand]
            ## PLOTTING
            ax.bar(x_indexes, stat_values, width, color = color, label = ligand)
    
        ## GETTING X TICKS
        plt.xticks(ind + (width / 2)*num_ligands, (desired_statistic_list))
        
        ## ROTATING X AXIS
        plt.xticks(rotation=45)
        
        ## ADDING Y AXIS
        ax.set_ylabel("Statistic value")
        
        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        
        ## TIGHT LAYOUT
        fig.tight_layout()
        
        
        ## STORING FIG
        plot_funcs.store_figure( fig = fig,
                                 path = os.path.join(store_fig_loc,
                                                     histogram_name + "_bar"),
                                 save_fig = save_fig
                                 )


        
        #%%
        ### WITH RESPECT TO LIGAND
#        ## DEFINING INDICES
#        ind = np.arange(len(ligand_name_list))
#        
#        ## DEFINING NUMBER OF STATISTICS
#        num_statistics = len(statistics_dict.keys())
#        
#        ## DEFINING WIDTH
#        width = 0.1
#        
#        ## LOOPING THROUGH EACH STATISTICS
#        for idx, each_stat in enumerate(desired_statistic_list):
#            ## GETTING VALUES
#            stat_values = [ statistics_dict[each_key][each_stat] for each_key in statistics_dict]
#            ## GENERATING X INDEX
#            x_indexes = ind + width * idx
#            ## PLOTTING
#            ax.bar(x_indexes, stat_values, width, label = each_stat)
#        
#        ## GETTING X TICKS
#        plt.xticks(ind + (width / 2)*num_statistics, (ligand_name_list))
#        
#        ## ADDING Y AXIS
#        ax.set_ylabel("Statistic value")
#        
#        # Put a legend to the right of the current axis
#        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
#        
#        ## TIGHT LAYOUT
#        fig.tight_layout()
#        
#        
        #%%
