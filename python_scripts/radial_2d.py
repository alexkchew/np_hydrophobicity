# -*- coding: utf-8 -*-
"""
sam_analysis.py
script to run analysis tools on SAMs

"""
##############################################################################
# Imports
##############################################################################
import sys, os
if "linux" in sys.platform and "DISPLAY" not in os.environ:
    import matplotlib
    matplotlib.use('Agg') # turn off interactive plotting

if r'R:/analysis_scripts' not in sys.path:
    sys.path.append(r'R:/analysis_scripts')

import numpy as np
import mdtraj as md
import matplotlib.pyplot as plt

#%%
def rdf_2d( center, points, r_cutoff = 2. ):
    r'''
    '''

#%%

##############################################################################
# Load analysis inputs and trajectory
##############################################################################   
if __name__ == "__main__":
    # --- TESTING ---
    ## DEFINE VARIABLES
    # simuation system variables and paths
    wd = r'R:/simulations/polar_sams/unbiased/sam_single_12x12_single_300K_dodecanethiol0.993_C13OH0.007_tip3p_nvt_CHARMM36/'    
#    wd = sys.argv[1]
    in_file = 'sam_prod'
#    in_file = sys.argv[2]
    solvents = [ "HOH", "MET" ]
    
    # technical variables
    step_size = 0.1
    n_procs = 10
    n_frames = 50000
    n_chunks = 5
    chunk_size = n_frames // n_chunks
        
    # output paths
    out_path = wd + 'output_files/'
    datfilename = in_file + '_hydration_map.dat'
            
##############################################################################
# Execute/test the script
##############################################################################
    #%%   
#    tail_group = "name N41 H42 H43"
#    tail_group = "name C38 O39 N40 H41 H42"
    tail_group = "name O41 H42"
#    com = [ ]
#    
#    n = 1
#    for traj in md.iterload( wd + in_file + '.xtc', top = wd + in_file + '.gro', chunk = chunk_size ):
#        print( "%d of %d" %( n, n_chunks ) )
#        group_ndx = traj.topology.select( tail_group )
#        positions = traj.xyz[:, group_ndx, : ]
#        atom_mass = np.array([ traj.topology.atom(atom_ndx).element.mass for atom_ndx in group_ndx ])
#        com.append( np.mean( ( positions * atom_mass[np.newaxis,:,np.newaxis] ).sum(axis=1) / atom_mass.sum(), axis=0 )[:2] )
#        n += 1
#    
#    com = np.array(com)
#    com = np.mean( com, axis=0 )
    
    traj = md.load( wd + in_file + '_10ns.xtc', top = wd + in_file + '.gro' )
    group_ndx = traj.topology.select( tail_group )
    positions = traj.xyz[:, group_ndx, : ]
    atom_mass = np.array([ traj.topology.atom(atom_ndx).element.mass for atom_ndx in group_ndx ])
    com = np.mean( ( positions * atom_mass[np.newaxis,:,np.newaxis] ).sum(axis=1) / atom_mass.sum(), axis=0 )[:2]

    with open( out_path + datfilename ) as raw_data:
        data = raw_data.readlines()
    
    data = np.array( [ [ float(el) for el in line.split(',') ] for line in data[2:] ] )
    avg_z = data[:,2].mean()
    mask = data[:,2] < avg_z
    data = data[mask,:]
#%%     
    ## acct for pbcs
    box = traj.unitcell_lengths[:,:2].mean(axis=0)
    shape = list(data.shape)
    shape[0] = 0
    new_data = np.empty( shape = shape, dtype = float )
    for x in [-1,0,1]:
        new_x = data[:,0] + x*box[0]
        for y in [-1,0,1]:
            new_y = data[:,1] + y*box[1]
            
            new_xy = np.vstack( ( new_x, new_y, data[:,2], data[:,3] ) ).transpose()
            new_data = np.vstack(( new_data, new_xy ))
    
    dist = np.sqrt( ( new_data[:,0] - com[0] )**2. + ( new_data[:,1] - com[1] )**2. )
    
    r = np.arange( 0, np.ceil( dist.max() ), step_size )
    rdf = np.zeros( r.size )
    counts = np.zeros( r.size )
    
    for ii, d in enumerate( dist ):
        ndx = np.floor( d / step_size ).astype('int')
        counts[ndx] += 1
        rdf[ndx] += new_data[ii,3]
    
    rdf[counts > 0.] /= counts[counts > 0.]
    
    rdf = rdf[r<=2.5]
    r =  r[r<=2.5]
    
    plt.figure()
    plt.plot( r, rdf )
        
        
    
    
    