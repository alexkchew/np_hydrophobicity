# -*- coding: utf-8 -*-
"""
cluster_grid_points.py
The purpose of this code is to cluster grid points so that we have a coarsed-grain 
representations of the grid points. The main idea is that we want a lower 
resolution of the surface without sacrificing accuracy. As a result, 
it may be fruitful to re-analyze the surface properties based on coarsed-grain 
representations of the surface. Instead of single points acting as a grid point, 
we will have a collection of points that act as a single hydrophobicity metric. 

INPUTS:
    - grid file
    
OUTPUTS:
    - grid file with a coarser representation
    - output a pdb file for the points

ALGORITHM:
    - Load grid file
    - Figure out how many clusters you want
    - Use a clustering algorithm based on distances between points to find 
    the best representation
    - Output the grid in PDB file
    - Output the grid as a list
    
NOTES:
    - This code should have a way of testing the gridding 
    - We should be able to vary the grouping size

Written by: Alex K. Chew (12/06/2019)

REFERENCES:
    Spectral clustering from scratch:
        https://towardsdatascience.com/unsupervised-machine-learning-spectral-clustering-algorithm-implemented-from-scratch-in-python-205c87271045

"""

## IMPORTING MODULES
import os
import sys
import numpy as np
## PLOTTING
import matplotlib.pyplot as plt
## 3D PLOTTING
from mpl_toolkits.mplot3d import Axes3D
## IMPORTING CORE FUNCTIONS
from core_functions import create_grid, calc_mu, load_datafile, flatten_list_of_list, split_list

## PLOTTING FUNCTIONS
import plot_functions as plot_funcs

## IMPORTING SPECTRAL CLUSTERING DETAILS
import numpy as np
float_formatter = lambda x: "%.3f" % x
np.set_printoptions(formatter={'float_kind':float_formatter})
from sklearn.cluster import SpectralClustering, KMeans
from sklearn.metrics import pairwise_distances

## LOADING PICKLE FUNCTIONS
import pickle_functions as pickle_funcs

## CHECK TESTING TOOL
from check_tools import check_testing

## IMPORTING TIME DETAILS
import time_functions as time_funcs

### FUNCTION TO CREATE 3D PLOT
def create_3d_plot():
    ''' Function creates 3D plots '''
    ## CREATING PLOT
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    ## ADDING LABELS
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    return fig, ax

### FUNCTION TO PLOT THE GRID POINTS
def plot_grid_points(grid_xyz,
                     scatter_style = {
                             's'         : 400,
                             'linewidth' : 1,
                             },
                     ):
    '''
    The purpose of this function is to plot the grid points
    INPUTS:
        grid_xyz: [np.array, (N, 3)]
            grid points in x, y, z cartersian coordinates
        scatter_style: [dict]
            dictionary for the scatter style
    OUTPUTS:
        fig, ax: figure and axis for gridding
    '''        
    ## CREATING FIGURE
    fig, ax = create_3d_plot()
    
    ## SCATTER PLOT
    ax.scatter(grid_xyz[:,0],
               grid_xyz[:,1],
               grid_xyz[:,2],
               **scatter_style)
    
    return fig, ax

### FUNCTION TO PLOT GRID BASED ON CLUSTERING
def plot_grid_clusters(grid_xyz,
                       sc_clustering,
                       scatter_style = {'s'         : 400,
                                 'linewidth' : 1,
                                   },
                           ):
    '''
    The purpose of this function is to plot the clustering
    INPUTS:
        grid_xyz: [np.array]
            grid in x,y,z positions
        sc_clustering: [object]
            output labels from spectral clustering
        scatter_style: [dict]
            dictionary for the scatter style
    OUTPUTS:
        fig, ax: figure and axis
    '''
    ## CREATING FIGURE
    fig, ax = create_3d_plot()
    
    ## SCATTER PLOT
    ax.scatter(grid_xyz[:,0],
               grid_xyz[:,1],
               grid_xyz[:,2],
               c = sc_clustering.labels_,
               cmap = 'jet', # 'rainbow'
               alpha = 0.7,
               edgecolors='k',
               **scatter_style
               )
    
    ## ADDING TITLE
    ax.set_title("Number of clusters: %d"%(sc_clustering.n_clusters) )
    
    return fig, ax

### FUNCTION TO FIT CLUSTER
def Spectral_Cluster_Grid_Points( grid, 
                                  num_clusters = 3,
                                  random_state = 0,
                                  n_jobs = -1,
                                  assign_labels = 'kmeans'):
    '''
    The purpose of this function is to cluster a group of grid points.
    The main idea is to cluster grid points of interest, then run another analysis 
    based on the clustered grid points. 
    INPUTS:
        grid: [np.array, num_atoms, 3]
            grid points in x, y, z positions
        num_clusters: [int]
            number of clusters desired
        random_state: [int]
            random state
        n_jobs: [int]
            number of parallel jobs
        assign_labels: [str]
            method for assigning labels
    '''
    
    ## RUNNING SPECTRAL CLUSTERING
    sc = SpectralClustering(n_clusters=num_clusters, 
                            affinity='nearest_neighbors', 
                            random_state=random_state,
                            n_jobs = n_jobs,
                            assign_labels = assign_labels,
                            ) # 'kmeans')
    
    ## FITTING THE SPECTRAL CLUSTERING
    sc_clustering = sc.fit(grid)

    return sc_clustering

## COMPUTING NUMBER OF CLUSTERS BY NUM WITHIN GROUP
def compute_cluster_from_num_within_group(grid, num_within_group = 5):
    '''
    The purpose of this function is to compute the total of number of clusters 
    required given that you want a specified number of atoms within a group. 
    INPUTS:
        grid: [np.array]
            grid array
        num_within_group: [int]
            number of atoms you want within a single group
    OUTPUTS:
        num_clusters: [int]
            number of clusters required
    '''
    ## NUM GRID POINTS
    num_grid = len(grid)        
    ## GETTING NUMBER OF CLUSTERS
    num_clusters = np.ceil(num_grid / num_within_group).astype('int')
    return num_clusters

### FUNCTION TO GET THE PICKLE FILE NAME
def find_pickle_name_based_on_clustering(num_within_group, num_clusters):
    '''
    The purpose of this function is to get the pickle file name 
    based on clustering.
    INPUTS:
        num_within_group: [int]
            number of atoms within each group
        num_clusters: [int]
            number of clusters
    '''
    ## DEFINING PICKLE PATH
    if num_within_group is not None:
        pickle_name = "cluster_within_group-%d.pickle"%(num_within_group)
    else:
        pickle_name = "num_cluster-%d.pickle"%(num_clusters)
    return pickle_name

### MAIN CODE 
def main_cluster_points(full_path_to_sim,
                        hyd_analysis_folder,
                        grid_folder,
                        grid_dat_file,
                        num_within_group = None,
                        num_clusters = None,
                        want_pickle = True,
                        n_jobs = -1,
                        verbose = True):
    '''
    Main function used to cluster points
    INPUTS:
        full_path_to_sim: [str]
            full path to simulation
        hyd_analysis_folder: [str]
            folder for analysis
        grid_folder: [str]
            folder for griding
        grid_dat_file: [str]
            file for gridding data files
        num_within_group: [int]
            number of atoms within a cluster
        num_clusters: [int]
            number of clusters
        want_pickle: [logical]
            True if you want to save pickle
            All pickles will be saved within gridding folder
        n_jobs: [int]
            number of parallel jobs
        verbose: [logical]
            True if you want to verbosely print output
    OUTPUTS:
        grid: [np.array]
            grid array
        sc_clustering: [labels]
            labels for clustering

    '''
    ## PATH TO GRID FOLDER
    path_grid_folder = os.path.join(full_path_to_sim,
                                    hyd_analysis_folder,
                                    grid_folder)
    
    ## DEFINING PATH
    path_grid_file = os.path.join(path_grid_folder,
                                  grid_dat_file,
                                  )
    
    ## GETTING PICKLE NAME
    pickle_name = find_pickle_name_based_on_clustering(num_within_group = num_within_group, 
                                                       num_clusters = num_clusters)

    ## DEFINING PICKLE PATH
    path_pickle = os.path.join( path_grid_folder, pickle_name )
    
    ## LOADING GRID FILE
    grid = load_datafile(path_to_file = path_grid_file)            
    

    
    ## COMPUTING CLUSTERS
    if num_within_group is not None:
        num_clusters = compute_cluster_from_num_within_group(grid = grid,
                                                             num_within_group = num_within_group)

    
    ## PRINTING
    if verbose is True:
        print("Running spectral clustering for %d grid points"%(len(grid)))
        print("Number of clusters desired: %d"%(num_clusters) )
        print("Number of cores ran in parallel: %d"%(n_jobs) )
        
    ## TRACKING TIME
    time_tracker = time_funcs.track_time()
    
    ## CLUSTERING
    sc_clustering = Spectral_Cluster_Grid_Points(grid = grid, 
                                                 num_clusters = num_clusters,
                                                 n_jobs=n_jobs)
    
    ## PRINTING TIME
    time_tracker.time_elasped()
    
    ## STORING THE CLUSTER LABELS
    if want_pickle is True:
        pickle_funcs.pickle_results(results = [ sc_clustering ],
                                    pickle_path = path_pickle,
                                    verbose = True
                                    )
    return grid, sc_clustering

#%% MAIN FUNCTION
if __name__ == "__main__":
    
    ## SEE IF TESTING IS ON
    testing = check_testing()
    
    ## RUNNING TESTING    
    if testing == True:
    
        ## DEFINING PATH
        path_sim = r"S:\np_hydrophobicity_project\simulations\NP_SPHERICAL"
        ## DEFINING SPECIFIC SIM
        specific_sim = r"MostlikelynpNVT_EAM_300.00_K_2_nmDIAM_C11COOH_CHARMM36jul2017_Trial_1_likelyindex_1"
        ## DEFINING PATH TO SIMULATION
        full_path_to_sim = os.path.join(path_sim,
                                        specific_sim)
    
        ## DEFINING FIGURE LOCATION AND DETAILS
        store_fig_loc = r"C:\Users\akchew\Box Sync\VanLehnGroup\2.Research Documents\Alex_RVL_Meetings\20191202\Images\spectral_clustering_output"
        save_fig = True
    
        ## DEFINING GRID
        hyd_analysis_folder = "hyd_analysis"
        grid_folder = r"grid-0_1000-0.1"
        grid_dat_file = r"out_willard_chandler.dat"
    
        ## DEFINING NUMBER OF CLUSTERS
        num_clusters = 1
        num_within_group = 200
        
        ## DEFINING NUMBER OF JOBS
        n_jobs = -1
    
    else:
        ## ADDING OPTIONS 
        from optparse import OptionParser # for parsing command-line options
        ## RUNNING COMMAND LINE PROMPTS
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        
        ## FULL PATH TO SIMULATION
        parser.add_option('--path', dest = 'full_path_to_sim', help = 'Path of simulation', default = '.', type=str)
        
        ## ANALYSIS FOLDER
        parser.add_option('--analysis', dest = 'hyd_analysis_folder', help = 'Folder for analysis', default = 'hyd_analysis', type=str)
        ## GRIDING FILE
        parser.add_option('--gridfolder', dest = 'grid_folder', help = 'Folder for gridding', default = 'grid-0_1000-0.1', type=str)
        ## GRIDDING DAT FILE
        parser.add_option('--griddat', dest = 'grid_dat_file', help = 'File for gridding', default = 'out_willard_chandler.dat', type=str)
        ## CLUSTERING 
        parser.add_option('--ncluster', dest = 'num_clusters', help = 'Number of clusters', default = None, type=int)
        parser.add_option('--nwithingroup', dest = 'num_within_group', help = 'Number of atoms within each cluster', default = None, type=int)
        
        ## DEFINING NUMBER OF CORES
        parser.add_option('--num_cores', dest = 'n_jobs', help = 'Number of parallel jobs', default = -1, type=int)
        
        ### GETTING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## STORING
        full_path_to_sim = options.full_path_to_sim
        hyd_analysis_folder = options.hyd_analysis_folder
        grid_folder = options.grid_folder
        grid_dat_file = options.grid_dat_file
        
        ## CLUSTERING
        num_clusters = options.num_clusters
        num_within_group = options.num_within_group
        
        ## DEFINING NUMBER OF JOBS
        n_jobs = options.n_jobs
        
    
    ## DEFINING INPUT VARIABLES
    input_vars = {'full_path_to_sim': full_path_to_sim,
                  'hyd_analysis_folder': hyd_analysis_folder,
                  'grid_folder': grid_folder,
                  'grid_dat_file': grid_dat_file,
                  'num_clusters': num_clusters,
                  'num_within_group': num_within_group,
                  'n_jobs': n_jobs,
                  }
    
    ## RUNNING MAIN FUNCTION
    grid, sc_clustering = main_cluster_points(**input_vars)
    

    #%%
    
    ## RUNNING TESTING    
    if testing == True:
    
        first_instance = True
        num_within_group = None
        ## DEFINING NUMBER OF CLUSTERS
        for num_clusters in [ 3, 10, 100 ]:
        
            ## DEFINING INPUT VARIABLES
            input_vars = {'full_path_to_sim': full_path_to_sim,
                          'hyd_analysis_folder': hyd_analysis_folder,
                          'grid_folder': grid_folder,
                          'grid_dat_file': grid_dat_file,
                          'num_clusters': num_clusters,
                          'num_within_group': num_within_group,
                          'n_jobs': n_jobs,
                          }
            
            ## RUNNING MAIN FUNCTION
            grid, sc_clustering = main_cluster_points(**input_vars)
    
            if first_instance is True:
                first_instance = False
                scatter_style = {
                                 's'         : 10,
                                 'linewidth' : .05,
                                 'color'     : 'black',   
                                 }
                fig, ax = plot_grid_points(grid_xyz = grid,
                                           scatter_style = scatter_style)
                plot_funcs.store_figure( fig = fig,
                                         path = os.path.join(store_fig_loc,
                                                             'no_clustering'),
                                         save_fig = save_fig,
                                         bbox_inches = 'tight',
                                         )
                
            ## PLOTTING CLUSTER
            fig, ax = plot_grid_clusters(grid_xyz = grid,
                                        sc_clustering = sc_clustering,
                                        scatter_style = {'s'         : 10,
                                                        'linewidth' : 0.5,
                                                        },
                                                         )
            ## DEFINING FIG NAME PREFIX
            fig_name_prefix="cluster"
            ## STORING FIGURE
            plot_funcs.store_figure( fig = fig,
                                     path = os.path.join(store_fig_loc,
                                                         fig_name_prefix + "_%d"%(num_clusters)),
                                     save_fig = save_fig,
                                     bbox_inches = 'tight',
                                     )

    ## DEFINING SCATTER STYLE
    ''' # VISUALIZE GRID POINTS
    scatter_style = {
                     's'         : 10,
                     'linewidth' : .05,
                     'color'     : 'black',   
                     }
    
    ## PLOTTING GRID POINTS
    fig, ax = plot_grid_points(grid_xyz = grid,
                               scatter_style = scatter_style)
    
    ## STORING FIGURE
    plot_funcs.store_figure( fig = fig,
                             path = os.path.join(store_fig_loc,
                                                 'no_clustering'),
                             save_fig = save_fig
                             )
    '''


    ''' # VISUALIZE CLUSTER OF POINTS
    ## PLOTTING
    fig, ax = plot_grid_clusters(grid_xyz = grid,
                                sc_clustering = sc_clustering,
                                scatter_style = {'s'         : 10,
                                                'linewidth' : 0.5,
                                                },
                                                 )
    
    ## DEFINING FIG NAME PREFIX
    fig_name_prefix="cluster"
    
    ## STORING FIGURE
    plot_funcs.store_figure( fig = fig,
                             path = os.path.join(store_fig_loc,
                                                 fig_name_prefix + "_%d"%(num_clusters)),
                             save_fig = save_fig
                             )
    '''
    
