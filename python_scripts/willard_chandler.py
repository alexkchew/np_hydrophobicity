# -*- coding: utf-8 -*-
# NOCW
# This file is part of the hydrophobicity project

R"""
The :class:`interfacial_hydrophobicity.willard_chandler` module contains tools to  
calculate the willard-chandler dividing surface.

FUNCTIONS:
    willard_chandler: computes willard-chandler interface
    
"""

##############################################################################
# Imports
##############################################################################

from __future__ import print_function
from datetime import datetime
from skimage import measure
import numpy as np
import platform
if platform.system() not in [ 'Windows', 'Darwin' ]:
    np.set_printoptions(legacy=False)  # fixes problem with skimage
from scipy.stats import gaussian_kde
from scipy.spatial import cKDTree

try:
    marching_cubes = measure.marching_cubes
except AttributeError:
    marching_cubes =  measure.marching_cubes_lewiner

try:
    import matplotlib.pyplot as plt
    import scipy.interpolate as interpolate
    _PYPLOT_AVAILABLE = True
except ImportError:
    plt.plot = None
    msg = ( 'plt.plot not available (requires display), '
            'will only generate image files' )
    _PYPLOT_AVAILABLE = False

__all__ = [ 'willard_chandler', 'plot_surface', 'density_field', 'wc_interface' ]

##############################################################################
# willard_chandler class
##############################################################################
class willard_chandler:
    R'''
    This class function computes willard-chandler surfaces given the trajectory. 
    Good reference for WC surfaces using MDAnalysis:
        https://marcello-sega.github.io/pytim/WillardChandler.html
    INPUTS:
        alpha: [float]
            xi value used to describe the width Gaussian functions
        mesh: [list]
            mesh size in nanometers in x, y, z dimensions
    '''
    def __init__( self, alpha = 0.24, mesh = [ 0.1, 0.1, 0.05 ] ):
        ### PRINTING
        print("**** CLASS: %s ****"%(self.__class__.__name__)) 
        
        if not _PYPLOT_AVAILABLE:
            print( 'Display not available, figures will be generated'
                   'as image files only' )
        ## STORING DEFAULT VARIABLES
        self.alpha = alpha
        self.mesh = mesh
        
    def compute( self, traj ):
        R'''
        
        INPUT:
            traj: [mdtraj.trajectory] 
                trajectory imported from mdtraj
            periodic: [bool] 
                include pbc (default is True)
        '''
        print("--- Calculating density field for %s simulations windows ---" % (str(traj.time.size)) )
        
        n_frames = traj.time.size
        avg_df, avg_spacing = density_field( traj, 0, alpha = self.alpha, mesh = self.mesh )
        
        for frame in range(n_frames-1):
            df, spacing = density_field( traj, frame+1, alpha = self.alpha, mesh = self.mesh )
            avg_df += df
            avg_spacing += spacing

        avg_df /= n_frames
        avg_spacing /= n_frames

        return [ avg_df, avg_spacing ]

## GENERATING COMPATIBLE MESH PARAMETETRS
def compute_compatible_mesh_params(mesh, box):
    """ given a target mesh size and a box, return the number of grid elements
        and spacing in each direction, which are commensurate with the box
    """
    n = np.array([ np.ceil( b / mesh[ii] ) for ii, b in enumerate( box ) ])
    d = np.array([ b / n[ii] for ii, b in enumerate( box ) ])
    return n, d

#
#def compute_compatible_mesh_params(mesh, box):
#    """ 
#    Given a target mesh size and a box, return the number of grid elements
#    and spacing in each direction, which are commensurate with the box
#    INPUTS:
#        mesh: [float]
#            mesh size in nm
#    OUTPUTS:
#        box: [list]
#            list of box array sizes in nm        
#    """
#    n = np.array([np.ceil(b / mesh) for b in box])
#    d = box / n
#    return n, d

def generate_grid_in_box(box, npoints, order='zxy'):
    """ 
    Generate an homogenous grid of npoints^3 points that spans the
    complete box.
    INPUTS:
        box: [list]
            the simulation box edges
        npoints: [list]
            the number of points along each direction
    OUTPUTS:
        grid: [np.array]
            grid that has been resizes to fit the box. 
    """
    xyz = []
    for i in range(3):
        xyz.append(np.linspace(0., box[i] - box[i] / npoints[i], npoints[i]))
        
    if order == 'zyx':
        z, y, x = np.meshgrid(xyz[0], xyz[1], xyz[2], indexing='ij')
    else:
        x, y, z = np.meshgrid(xyz[2], xyz[1], xyz[0], indexing='ij')

    grid = np.append(x.reshape(-1, 1), y.reshape(-1, 1), axis=1)
    grid = np.append(grid, z.reshape(-1, 1), axis=1)
    return grid.T

### FUNCTION TO GENERATE DENSITY MAPS
def density_map(pos, grid, sigma, box):
    '''
    This function generates density maps. The bw_method is the type of method 
    used as a parameter in the gaussian_kde function. 
    INPUTS:
        pos: [np,array, shape=(N,3)]
            positions
        grid: DEPRECIATED INPUT
        sigma: [float]
            sigma vlaues
        box: [list]
            the simulation box edges
    OUTPUTS:
        kernel: 
            kernel outputs
        values.std(ddof=1):
            values with a standrad deviation of 1
    '''
    values = np.vstack([pos[::, 0], pos[::, 1], pos[::, 2]])
    kernel = gaussian_kde_pbc(values, bw_method=sigma / values.std(ddof=1))
    kernel.box = box
    kernel.sigma = sigma
    return kernel, values.std(ddof=1)

class gaussian_kde_pbc(gaussian_kde):
    '''
    These functions evaluate PBC fast. See reference:
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.gaussian_kde.html
    '''
    # note that here "points" are those on the grid

    def evaluate_pbc_fast(self, points):
        grid = points
        pos = self.pos
        box = self.box
        d = self.sigma * 2.5
        results = np.zeros(grid.shape[1], dtype=float)
        gridT = grid[::-1].T[:]
        tree = cKDTree(gridT, boxsize=box)
        # the indices of grid elements within distane d from each of the pos
        scale = 2. * self.sigma**2
        indlist = tree.query_ball_point(pos, d)
        for n, ind in enumerate(indlist):
            dr = gridT[ind, :] - pos[n]
            cond = np.where(dr > box / 2.)
            dr[cond] -= box[cond[1]]
            cond = np.where(dr < -box / 2.)
            dr[cond] += box[cond[1]]
            dens = np.exp(-np.sum(dr * dr, axis=1) / scale)
            results[ind] += dens

        return results
   
### FUNCTION TO GENERATE DENSITY FIELD
def density_field(traj, 
                  frame, 
                  alpha = 0.24, 
                  mesh = [0.1, 0.1, 0.1],
                  residue_list = ['HOH', 'MET']):
    R'''
    This function creates a density field given the trajectory.
    INPUTS:
        traj: [obj]
            trajectory object
        alpha: [float]
            alpha balue used in WC interface
        mesh: [float]
            mesh increment value
        residue_list: [list]
            list of resiudes to check for density field, default is water and methanol
    OUTPUTS:
        volume: 
            volume
        spacing:
            spacing
    '''
    box = traj.unitcell_lengths[ frame, : ]
    ndx_solv = np.array( [ [ atom.index for atom in residue.atoms if 'H' not in atom.name ] 
                             for residue in traj.topology.residues if residue.name in residue_list ] ).flatten()
    pos = traj.xyz[ frame, ndx_solv, : ]
    
    ## FINDING COMPATIBLE MESH PARAMETERS
    ngrid, spacing = compute_compatible_mesh_params( mesh, box )
    grid = generate_grid_in_box( box, ngrid, order = 'xyz' )
    kernel, _ = density_map( pos, grid, alpha, box )
    kernel.pos = pos.copy()
    density_field = ( 2 * np.pi * alpha**2 )**(-1.5) * kernel.evaluate_pbc_fast(grid)
    
    # Thomas Lewiner, Helio Lopes, Antonio Wilson Vieira and Geovan
    # Tavares. l. Journal of Graphics Tools 8(2) pp. 1-15
    # (december 2003). DOI: 10.1080/10867651.2003.10487582
    volume = density_field.reshape( tuple( np.array( ngrid[::-1] ).astype(int) ) )    
            
    return volume, spacing

### FUNCTION TO COMPUTE THE WILLARD-HANDLER INTERFACE
def wc_interface( density_field, spacing, contour = 16. ):
    R'''
    Returns (z,y,x)
    '''
    ## USING MARCHING CUBES
    verts, faces, normals, values = marching_cubes( density_field, level = contour, spacing = tuple( spacing ) )
    # note that len(normals) == len(verts): they are normals
    # at the vertices, and not normals of the faces
    triangulated_surface = [verts, faces, normals]
    
    return triangulated_surface[0]

## PLOTTING THE SURFACE
def plot_surface( data, out_path ):
    '''
    '''
    x, y, z = data[:,0], data[:,1], data[:,2]
    xi, yi = np.linspace( x.min(), x.max(), 500 ), np.linspace( y.min(), y.max(), 500 )
#        xi, yi = np.linspace( x.min(), x.max(), nx_bins ), np.linspace( y.min(), y.max(), ny_bins )
    xi, yi = np.meshgrid( xi, yi )
    zi = interpolate.griddata( (x, y), z, (xi, yi), method = 'cubic' )
    
    zi[zi<0.] = 0. # Converts all z values that are negative to zero
    z_min = 0.1 # np.min(zi)
    z_max = 0.3 # np.max(zi)
    print( 'min z: %s; max z: %s' %( z_min, z_max ) )
    print( 'min z: %s; max z: %s' %( np.min(zi), np.max(zi) ) )
    
    plt.figure()
    plt.imshow( zi, vmin = z_min, vmax = z_max, origin = 'lower', extent = [ x.min(), x.max(), y.min(), y.max() ], 
                aspect = 'auto', cmap = plt.get_cmap( 'coolwarm' ) )
    
    plt.xlabel( 'x (nm)' )
    plt.ylabel( 'y (nm)' )
    cbar = plt.colorbar( )
    cbar.set_label( 'z (nm)' )
    cbar.set_ticks([ 0.12, 0.16, 0.20, 0.24, 0.28 ])
    cbar.set_ticklabels([ 0.12, 0.16, 0.20, 0.24, 0.28 ])
    plt.savefig( out_path + 'wc_surface.png', format='png', dpi=1000, bbox_inches='tight' )
    plt.savefig( out_path + 'wc_surface.svg', format='svg', dpi=1000, bbox_inches='tight' )
    