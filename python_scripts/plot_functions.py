# -*- coding: utf-8 -*-
"""
plot_functions.py
This script contains all functions that have plotting (e.g. saving figures, etc.)

USAGE: import plot_functions

GLOBAL VARIABLES:
    LABELS: label for plots
    LINE_STYLE: dictionary for line style
    ATOM_DICT: atom dictionary for plotting purposes
    

FUNCTIONS:
    create_plot: creates a general plot
    save_fig_png: saves current figure as a png
    store_figure: stores figure as any format
    cm2inch: function that converts cm to inches
    create_fig_based_on_cm: function that creates figure  based on input cms
    color_y_axis: changes color of y-axis
    create_3d_axis_plot: creates 3D axis plot
    get_cmap: generates a color map for plot with a lot of colors
    plot_solute_atom_index: plots solute with atom index
    plot_3d_molecule: plots 3D molecule
    
Authors:
    Alex K. Chew (alexkchew@gmail.com)

** UPDATES **
20180511 - AKC - added color_y_axis function
20180622 - AKC - creates 3D axis plot
20181003 - AKC - adding get_cmap function to get colors
"""
## IMPORTING IMPORTANT MODULES
import matplotlib.pyplot as plt
import numpy as np

## DEFINING GLOBAL PLOT PROPERTIES
LABELS = {
            'fontname': "Arial",
            'fontsize': 16
            }
## DEFINING LINE STYLE
LINE_STYLE={
            "linewidth": 1.6, # width of lines
            }

### DEFINING ATOM AND BOND TYPES
ATOM_DICT = { 
        'C': {'color': 'black','size':4},
        'O': {'color': 'red','size':4},
        'H': {'color': 'gray','size':3},
        'N': {'color': 'blue','size':1},
        'Au': {'color': 'orange','size':6},
        'S': {'color': 'yellow','size':5},
        } 



### FUNCTION TO CREATE PLOT
def create_plot():
    '''
    The purpose of this function is to generate a figure.
    Inputs:
        fontSize: Size of font for x and y labels
        fontName: Name of the font
    Output:
        fig: Figure to print
        ax: Axes to plot on
    '''
    ## CREATING PLOT
    fig = plt.figure() 
    ax = fig.add_subplot(111)
    return fig, ax


## FUNCTION TO CONVERT FIGURE SIZE
def cm2inch(*tupl):
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i/inch for i in tupl[0])
    else:
        return tuple(i/inch for i in tupl)

## FUNCTION TO CREATE FIG BASED ON CM
def create_fig_based_on_cm(fig_size_cm = (16.8, 16.8)):
    ''' 
    The purpose of this function is to generate a figure based on centimeters 
    INPUTS:
        fig_size_cm: [tuple]
            figure size in centimeters 
    OUTPUTS:
        fig, ax: 
            figure and axis
    '''
    ## FINDING FIGURE SIZE
    figsize=cm2inch( *fig_size_cm )
    ## CREATING FIGURE
    fig = plt.figure(figsize = figsize) 
    ax = fig.add_subplot(111)
    return fig, ax


### FUNCTION TO SAVE FIGURE AS A PNG
def save_fig_png(fig, label, save_fig = True, dpi=600, bbox_inches = 'tight'):
    '''
    The purpose of this function is to save figure as a png
    INPUTS:
        fig: Figure
        label: The label you want to save your figure in
        save_fig: True or False. If True, then save the figure
        dpi: [OPTIONAL, default = 600] resolution of image
        bbox_inches: [OPTIONAL, default='tight'] how tight you want your image to be
    OUTPUTS:
        png figure
    '''
    if save_fig is True:
        label_png = label + '.png'
        ## SAVING FIGURE
        fig.savefig( label_png, format='png', dpi=dpi, bbox_inches=bbox_inches)
        ## PRINTING
        print("EXPORTING TO: %s"%(label_png))
    return

### FUNCTION THAT DEALS WITH SAVING FIGURE
def store_figure(fig, path, fig_extension = 'png', save_fig=False, dpi=1200,
                 bbox_inches = None):
    '''
    The purpose of this function is to store a figure.
    INPUTS:
        fig: [object]
            figure object
        path: [str]
            path to location you want to save the figure (without extension)
        fig_extension:
        want_tight: [logical]
            True if you want a tight plot
    OUTPUTS:
        void
    '''
    ## STORING FIGURE
    if save_fig is True:
        ## DEFINING FIGURE NAME
        fig_name =  path + '.' + fig_extension
        print("Printing figure: %s"%(fig_name) )
        fig.savefig( fig_name, 
                     format=fig_extension, 
                     dpi = dpi,    
                     bbox_inches = bbox_inches,
                     )
    return


### FUNCTION TO CHANGE THE COLOR OF Y AXIS
def color_y_axis(ax, color):
    """Color your axes."""
    for t in ax.get_yticklabels():
        t.set_color(color)
    return None

### FUNCTION TO CREATE 3D PLOTS
def create_3d_axis_plot():
    '''
    The purpose of this function is to create a 3D axis plot
    INPUTS:
        void
    OUTPUTS:
        fig, ax: figure and axis of the plot
    '''
    ## IMPORTING TOOLS
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D # For 3D axes
    ## CREATING FIGURE
    fig = plt.figure(); ax = fig.add_subplot(111, projection='3d', aspect='equal')
    return fig, ax

### FUNCTION TO CREATE 3D PLOTS WITH XYZ COORDINATES
def create_3d_axis_plot_labels(labels=['x', 'y', 'z']):
    '''
    The purpose of this function is to create a 3D axis plot with x, y, z
    INPUTS:
        labels: [list, size=3] list of labels for the 3D axis
    OUTPUTS:
        fig, ax
    '''
    ## CREATING FIGURE
    fig, ax = create_3d_axis_plot()
    
    ## SETTING LABELS
    ax.set_xlabel(labels[0])
    ax.set_ylabel(labels[1])
    ax.set_zlabel(labels[2])
    
    return fig, ax
    
### FUNCTION TO GET CMAP
def get_cmap(n, name='hsv'):
    '''Returns a function that maps each index in 0, 1, ..., n-1 to a distinct 
    RGB color; the keyword argument name must be a standard mpl colormap name.
    This function is useful to generate colors between red and purple without having to specify the specific colors
    USAGE:
        ## GENERATE CMAP
        cmap = get_cmap(  len(self_assembly_coord.gold_facet_groups) )
        ## SPECIFYING THE COLOR WITHIN A FOR LOOP
        for ...
            current_group_color = cmap(idx) # colors[idx]
            run plotting functions
    '''
    ## IMPORTING FUNCTIONS
    import matplotlib.pyplot as plt
    return plt.cm.get_cmap(name, n + 1)
    
### FUNCTION TO VISUALIZE SOLUTE ATOM INDEX
def plot_solute_atom_index( positions, atom_elements, atom_names, bond_atomnames = None, atom_dict = ATOM_DICT ):
    '''
    The purpose of this function is to plot the solute atom index
    INPUTS:
        positions: [np.array, shape=(N_atoms, 3)]
            positions in cartesian coordinates
        atom_elements: [np.array, shape=(N_atoms, 1)]
            atom elements of each position
        atom_names: [list, shape=(N_atoms, 1)]
            atom names, e.g. 'O1', etc.
        bond_atomnames: [list, optional]
            bond atomnames 
        atom_dict: [dict]
            dictionary of color and size for the atom
    OUTPUTS:
        plot with solute atom index
        fig, ax for plot
    '''
    ## CREATING FIGURE
    fig, ax = create_3d_axis_plot_labels()
    
    ## FINDING UNIQUE ATOM LIST
    unique_atom_list = list(set(atom_elements))
    
    ## LOOPING THROUGH EACH UNIQUE ATOM LIST
    for current_atom_type in unique_atom_list:
        ## FINDING ALL ELEMENTS THAT MATCH THE CRITERIA
        index_matched = np.array([ index for index, atom in enumerate(atom_elements) if atom == current_atom_type ])
        ## DEFINING CURRENT ATOM DICT
        current_atom_dict = { your_key: atom_dict[current_atom_type][your_key] for your_key in ['color'] }
        ## ADDING TO PLOT
        ax.scatter( positions[index_matched, 0], positions[index_matched, 1], positions[index_matched, 2], s = 50, **current_atom_dict )
    ## ADDING ANNOTATION
    for idx, each_label in enumerate(atom_names):
        ax.text( positions[idx][0], positions[idx][1], positions[idx][2], each_label )
        
    ## ADDING BONDS
    if bond_atomnames is not None:
        for current_bond in bond_atomnames:
            ## FINDING COORDINATES FOR EACH ATOM
            bond_coordinates = np.array( [ positions[index] for index in [ atom_names.index(eachAtom) for eachAtom in current_bond ] ] )
            ## PLOTTING
            ax.plot( bond_coordinates[:,0], bond_coordinates[:,1], bond_coordinates[:,2], color='k' )
            
    return fig, ax


### FUNCTION TO PLOT ATOM AND BONDS USING MATPLOTLIB
def plot_3d_molecule( atom_positions, atom_symbols, bonds, atom_names = None ):
    '''
    The purpose of this function is to plot in 3D the molecule of your interest.
    INPUTS:
        atom_positions: [np.array, shape=(num_atoms, 3)]
            atomic positions in Cartesian coordinates
        atom_symbols: [np.array, shape=(num_atoms, 1)]
            atomic symbols (e.g. 'H', 'O', etc.)
        bonds: [np.array, shape=(num_bonds, 2)]
            bonds between atoms , e.g. [1,2] means that atoms 1 and 2 are bonded
        atom_names: [np.array, shape=(num_atoms), default=None]
            Optional, will print atom names if you supply it
    '''
    
    ### ASSIGNING ATOM SIZES AND COLOR FOR EACH TYPE OF ATOM
    ATOM_PLOT_INFO={
            'H':
                { "color"   :"gray",
                  "s"       : 50,
                        },
            'C':
                { "color": "black",
                  "s"       : 100,
                 },
            'O':
                { "color": "red",
                  "s"       : 100,
                 },
            
            }
                
    ### DICTIONARY FOR BONDS
    BOND_PLOT_INFO = {
            'color': 'black',
            'linestyle': '-',
            'linewidth': 2,
            }
    
    ## CREATING 3D AXIS
    fig, ax = create_3d_axis_plot_labels(labels=['x', 'y', 'z'])
    
    ## GENERATING SIZES
    sizes = [ ATOM_PLOT_INFO[each_symbol]['s'] for each_symbol in atom_symbols ]
    
    ## GENERATING ARRAY OF COLORS
    color_array = [ ATOM_PLOT_INFO[each_symbol]['color'] for each_symbol in atom_symbols ]
    
    ## SETTING X, Y, Z
    X = atom_positions[:,0]
    Y = atom_positions[:,1]
    Z = atom_positions[:,2]
    
    ## FINDING LIMITS
    minX, maxX = np.min(X), np.max(X)
    minY, maxY = np.min(Y), np.max(Y)
    minZ, maxZ = np.min(Z), np.max(Z)
    
    ## FINDING LARGEST INTERVAL / 2
    largest_interval = np.max( [maxX - minX,
                               maxY - minY,
                               maxZ - minZ,]
                              ) / 2.0
    
    ## CREATING NEW AXIS LIMITS
    xlimits = [ np.mean( [minX,maxX] ) - largest_interval, np.mean( [minX,maxX] ) + largest_interval ]
    ylimits = [ np.mean( [minY,maxY] ) - largest_interval, np.mean( [minY,maxY] ) + largest_interval ]
    zlimits = [ np.mean( [minZ,maxZ] ) - largest_interval, np.mean( [minZ,maxZ]) + largest_interval ]
    
    
    ## SETTING AXIS LIMITS
    ax.set_xlim( xlimits  )
    ax.set_ylim( ylimits  )
    ax.set_zlim( zlimits  )
    
    ## PLOTTING ATOM
    ax.scatter( X, Y, Z, s = sizes , color= color_array  )  # scatter = 
    
    ## DRAWLING BONDS FOR LINES
    for current_bond in bonds:
        ## FINDING COORDINATES
        atom_coord_1 = atom_positions[current_bond[0]] 
        atom_coord_2 = atom_positions[current_bond[1]] 
        ## PLOTTING BOND
        ax.plot( [ atom_coord_1[0], atom_coord_2[0] ] , # x
                 [ atom_coord_1[1], atom_coord_2[1] ], # y
                 [ atom_coord_1[2], atom_coord_2[2] ], # z
                 **BOND_PLOT_INFO
                 )
#            
    ## ADDING LABELS
    if atom_names is not None:
        for x,y,z,i in zip(X,Y,Z,atom_names):
            ax.text(x,y,z,i)
    
    return fig, ax
