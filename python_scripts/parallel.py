##############################################################################
# hydrophobicity: A Python Library for analysis of an water-soft material 
#                 interface
#
# Author: Bradley C. Dallin
# email: bdallin@wisc.edu
# Edited by AKC
'''
FUNCTIONS:
    parallel_analysis_by_splitting_traj: function that runs analysis by splitting trajectories.
    

'''
##############################################################################

##############################################################################
# Imports
##############################################################################

#import sys
import multiprocessing
import numpy as np
import datetime
# from core_functions import split_list

__all__ = [ 'parallel' ]

##############################################################################
# Monolayer info class
##############################################################################
def worker( arg ):
    R''' '''
    obj, traj = arg
    return obj.compute( traj )

class parallel:
    R'''
    This class parallelizes functions
    INPUTS:
        traj: [traj]
            trajectory information
        func: [function]
            function to run trajectory on
        args: [dict]
            dictionary of arguments
        average: [logical]
            True if you care about the average ensemble value of a result. 
            If false, the reuslts will just summed up, which is typical 
            for distributions.
    OUTPUTS:
        self.split_traj: [list]
            lits of split trajectories
        self.results: [list or value]
            results outputted for your function
    '''
    def __init__( self, traj, func, args, n_procs = -1, average = True ):
        R''' '''
        ### PRINTING
        print("**** CLASS: %s ****"%(self.__class__.__name__))
        
        ## COMPUTING NUBMER OF PROCESSORS
        if n_procs < 0:
            n_procs = multiprocessing.cpu_count()
        print( "\n--- Running %s on %s cores ---\n" %( func.__name__, str(n_procs) ) )
        
        if traj.time.size < n_procs:
            n_procs = traj.time.size
        
        ## SPLITTING TRAJECTORIES
        traj_list = self.split_traj( traj, n_procs )
        
        ## DELETE ORIGINAL TRAJECTORY REDUCE MEMORY
        del traj 
        
        ## DEFINING OBJECT LIST        
        object_list = self.create_obj_list( func, n_procs, traj_list, args )
        ## CREATING POOL OF WORKERS
        pool = multiprocessing.Pool( processes = n_procs )
        
        ## GENERATE RESULTS LIST
        result_list = pool.map( worker, ( ( obj, trj ) for obj, trj in zip( object_list, traj_list ) ) )
        
        ## closing pools and joins the information
        pool.close()
        pool.join()
        
        ## EITHER AVERAGING OR PRINTING THE RESULTS
        if average: # average outputs, typically what you want for ensemble averages
            self.results = result_list[0] 
            for result in result_list[1:]:
                self.results += result
            self.results /= len(result_list)
        else: # sum outputs, typically what you want for distributions
            self.results = result_list[0] 
            for result in result_list[1:]:
                self.results += result
        return
    
    @staticmethod
    def split_traj( traj, n_procs ):
        R'''
        The purpose of this function is to split the trajectory for multiple 
        processors.
        INPUTS:
            traj: [obj]
                trajectory object from md.load
            n_procs: [int]
                number of processors to divide your object
        OUTPUTS:
            traj_list: [list]
                list of trajectories that are evenly divided
        '''
        traj_list = []
        ## SEE IF NUMBER OF PROCESSORS IS GREATER THAN ONE
        if n_procs > 1:
            ## SPLIT TRAJ INTO N PROCESSES
            len_split = len(traj) // n_procs
            remainder = len(traj) % n_procs
            splits = []
            ## LOOPING THROUGH EACH PROCESSORS AND SPLITTING
            for n in range( n_procs ):
                if n < remainder:
                    splits.append( len_split + 1 )
                else:
                    splits.append( len_split )
            
            ## LOOPING AND STORING THE SPLITS
            current = 0
            for split in splits:
                traj_list.append( traj[current:current+split] )
                current += split
        else:
            traj_list.append( traj )
            
        return traj_list
    
    @staticmethod
    def create_obj_list( func, n_procs, traj_list, args ):
        R'''
        This function creates an object list and runs the function across different 
        processors.
        INPUTS:
            func: [function]
                function that you are trying to run
            n_procs: [int]
                number of processors to run the function on
            traj_list: [list]
                list of trajectories that has been split
            args: [dict]
                dictionary of arguments for the function
        OUTPUTS:
            object_list: [list]
                list of results from the functions
        '''
        ## CREATES A LIST OF CLASS OBJECTS
        object_list = [ func( **args ) for ii in range( 0, n_procs ) ]        
        ## CHECKING IF OBJECT AND TRAJ LIST MATCHES
        if len(object_list) != len(traj_list):
            raise RuntimeError( '\n  ERROR! More objects created than split trajectories' )
        
        return object_list


### FUNCTION THAT SPLITS A LIST INTO MULTIPLE PARTS
def split_list(alist, wanted_parts=1):
    '''
    The purpose of this function is to split a larger list into multiple parts
    INPUTS:
        alist: [list] original list
        wanted_parts: [int] number of splits you want
    OUTPUTS:
        List containing chunks of your list
    Reference: https://stackoverflow.com/questions/752308/split-list-into-smaller-lists?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
    '''
    length = len(alist)
    return [ alist[i*length // wanted_parts: (i+1)*length // wanted_parts] 
             for i in range(wanted_parts) ]

### GENERALIZED PARALLEL FUNCTION
def parallel_analysis_by_splitting_traj(traj, 
                                        class_function, 
                                        input_details = None, 
                                        n_procs = 1,
                                        combine_type="sum"):
    '''
    The purpose of this script is to generalize the code for splitting trajectories. 
    We assume that you want to initialize the class function, then run a class function for it.
    ASSUMPTION:
        - output is a numpy array that could be merged
    INPUTS:
        traj: [traj]
            trajectory details
        class_function: [func]
            function that you want to parallelize
        input_details: [dict]
            input details that will be inserted into the function
        n_procs: [int]
            number of processors. This will also be the extent of splitting trajectories.
        combine_type: [str]
            combination type, default is sum
                This will just add all the details into a single sum file
                Alternative:
                    "None" -- No summing, just output everything and you'll figure it out later
    OUTPUT:
        results_output: [tuple]
            output from your function
    '''
    ## COMPUTING NUBMER OF PROCESSORS
    if n_procs < 0:
        n_procs = multiprocessing.cpu_count()
    
    
    ## TRACKING THE TIME
    start = datetime.datetime.now()
    
    ## RUNNING MULTIPROCESSING DETAILS USING COSOLVENT MAPPING PROTOCOL
    if n_procs != 1:
        ## PRINTING
        print( "\n--- Running %s on %d cores ---\n" %( class_function.__name__, n_procs ) )
        ## DEFINING TRAJ SPLIT
        list_splitted = split_list(alist = traj, wanted_parts = n_procs)
        ## GETTING MULTIPROCESSING
        pool = multiprocessing.Pool( processes = n_procs )
        ## RUNNING FUNCTION
        results = pool.map(class_function, list_splitted)
        ## SUMMARY
        if combine_type == "sum":
            results_output = np.sum(results,axis = 0)
        elif combine_type == "None":
            results_output = results
        ## DECOMPOSING LIST -- error, leaving alone for now
        # results_output = np.array(calc_tools.flatten_list_of_list( results ))
    
    else:
        print("\n--- Running code serially on one core! ---\n")
        ## SERIAL APPROACH
        results_output = class_function(traj = traj)
    
    ## PRINTING TIME ELAPSED
    time_elapsed = datetime.datetime.now() - start
    print( 'Time elapsed (hh:mm:ss.ms) {}\n'.format(time_elapsed) )
    
    return results_output
    