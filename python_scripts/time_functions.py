# -*- coding: utf-8 -*-
"""
time_functions.py
This contains all time functions.

FUNCTIONS:
    track_time: 
        keeps track of time

"""

from datetime import datetime

### CLASS FUNCTION TO TRACK TIME
class track_time:
    '''
    The purpose of this function is to track time.
    INPUTS:
        void
        
    FUNCTIONS:
        time_elasped: 
            function to print total time elapsed
    '''
    def __init__(self):
        ## START TRACKING TIME
        self.start = datetime.now()
        return
    ## FUNCTION TO PRINT TIME
    def time_elasped(self):
        ''' Function to print time elapsed '''
        time_elapsed = datetime.now() - self.start
        print( 'Time elapsed (hh:mm:ss.ms) {}\n'.format(time_elapsed) )
        return