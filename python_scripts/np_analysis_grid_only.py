# -*- coding: utf-8 -*-
"""
np_analysis.py
This is the main code to run analysis on nanoparticles coated with SAMs.

INSTALLATION NOTES:
    - May need ski-kit image
        pip install scikit-image

Updated by Alex K. Chew (10/24/2019)

"""
##############################################################################
# Imports
##############################################################################
import sys, os
if "linux" in sys.platform and "DISPLAY" not in os.environ:
    import matplotlib
    matplotlib.use('Agg') # turn off interactive plotting

import os
import mdtraj as md

## IMPORTING GLOBAL VARIABLES
from global_vars import WC_DEFAULTS

## CUSTOM IMPORTS
from core_functions import create_grid, get_list_args
from check_tools import check_testing 


#%%
##############################################################################
# Load analysis inputs and trajectory
##############################################################################   
if __name__ == "__main__":
    # --- TESTING ---
    
    ## SEE IF TESTING IS ON
    testing = check_testing()
    
    ## WILLARD-CHANDLER VARIABLES
    alpha = WC_DEFAULTS['alpha']
    contour = WC_DEFAULTS['contour']
    
    ## RUNNING TESTING    
    if testing == True:
        ## DEFINING MAIN SIMULATION
        main_sim=r"S:\np_hydrophobicity_project\simulations\191210-annealing_try4"
        ## DEFINING SIM NAME
        sim_name=r"np_spherical_300K_2_nm_C11COOH_CHARMM36_mlc"
        sim_name=r"FrozenGoldPlanar_300.00_K_dodecanethiol_10x10_CHARMM36jul2017_intffGold_Trial_1-50000_ps"
        ## DEFINING WORKING DIRECTORY
        wd = os.path.join(main_sim, sim_name)
        ## DEFINING GRO AND XTC
        gro_file = r"sam_prod-0_1000-watO_grid.gro"
        # r"sam_prod.gro"
        xtc_file = r"sam_prod-0_1000-watO_grid.xtc"
        # r"sam_prod.xtc"
        
        ## DEFINING PREFIX
        output_prefix = "sam_prod"
        
        ## DEFINING NUMBER OF PROCESSORS
        n_procs = 2
        
        ## DEFINING MESH
        mesh = WC_DEFAULTS['mesh']
        
        ## DEFINING OUTPUT FILE
        output_file = "output_files_test"
        
    else:
        ## ADDING OPTIONS 
        from optparse import OptionParser # for parsing command-line options
        ## RUNNING COMMAND LINE PROMPTS
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        
        ## REPRESENTATION TYPE
        parser.add_option('--path', dest = 'simulation_path', help = 'Path of simulation', default = '.', type=str)
        
        ## DEFINING GRO AND XTC FILE
        parser.add_option('--gro', dest = 'gro_file', help = 'Name of gro file', default = 'sam.gro', type=str)
        parser.add_option('--xtc', dest = 'xtc_file', help = 'Name of xtc file', default = 'sam.xtc', type=str)
        
        ## OUTPUT PREFIX
        parser.add_option('--output_file', dest = 'output_file', help = 'Output directory name', default = 'output', type=str)
        parser.add_option('--output_prefix', dest = 'output_prefix', help = 'Output prefix', default = 'output', type=str)

        parser.add_option('--n_procs', dest = 'n_procs', help = 'Number of processors', default = 20, type=int)
        
        ## DEFINING MESH SIZE
        # parser.add_option('--mesh', dest = 'mesh', help = 'Mesh size', default = 0.1, type=float)
        parser.add_option("--mesh", dest="mesh", action="callback", type="string", callback=get_list_args,
                  help="Mesh separated by comma (no whitespace)", default = [ 0.1, 0.1, 0.1 ] )
        
        ### GETTING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## DEFINING OUTPUTS
        # WORKING DIRECTORY
        wd = options.simulation_path
        # GRO/XTC
        gro_file = options.gro_file
        xtc_file = options.xtc_file
        
        ## OUTPUT PREFIX
        output_prefix = options.output_prefix

        ## TECHNICAL DETAILS
        n_procs = options.n_procs
        
        ## DEFINING MESH SIZE
        mesh = options.mesh
        
        ## CONVERTING ALL MESH TO POINT
        mesh = [float(i) for i in mesh]
        
        ## DEFINING OPTION DIRECTORY
        output_file = options.output_file
        
    ## LOGICALS
    write_pdb = True # Writes PDB file
    
    ## DEFINING PATHS
    path_gro = os.path.join(wd, gro_file)
    path_xtc = os.path.join(wd, xtc_file)
    
    ## OUTPUT PATHS
    out_path = os.path.join( wd, output_file)
    wcdatfilename = output_prefix + '_willard_chandler.dat'
    wcpdbfilename = output_prefix + '_willard_chandler.pdb'
    ##############################################################################
    # Execute/test the script
    ##############################################################################
    #%%
    ## PRINTING
    print("Loading trajectory")
    print(" --> XTC file: %s"%(path_xtc) )
    print(" --> GRO file: %s"%(path_gro) )
    ## LOADING TRAJECTORY
    traj = md.load(path_xtc, top = path_gro)
    
    ## PRINTING
    print("Generating grid")
    print(" --> Output path: %s"%(out_path) )
    print(" --> WC file: %s"%(wcdatfilename) )
    print(" --> N_Procs: %s"%(n_procs) )
    print(" --> Mesh: %s"%(', '.join([str(x) for x in mesh]) )  )
    print(" --> Total trajectory frames: %s"%(len(traj)) )
        
    ## GENERATING GRID
    grid = create_grid(traj, 
                       out_path, 
                       wcdatfilename, 
                       wcpdbfilename, 
                       alpha = alpha, 
                       mesh = mesh, 
                       contour = contour, 
                       write_pdb = write_pdb, 
                       n_procs = n_procs,
                       verbose = True)    