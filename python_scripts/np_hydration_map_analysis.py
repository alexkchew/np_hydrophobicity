# -*- coding: utf-8 -*-
"""
np_hydration_hydration_map_only.py
This script generates hydration maps based on the output of the GMX select commands 
The algorithm is as follows:
    - load all xvg files
    - load all grid points
    - make sure there is a correspondence between the xvg file and the data points
    - generate a numpy array
    - could pickle the numpy array if you would like (or if it's fast, don't do it)
    - use hydration map code to generate probabilities for each surface atom
    - output as a PDB file, should match the current python code

Updated by Alex K. Chew & Brad C. Dallin (10/25/2019)

"""
import sys
import os
import glob
import numpy as np
import pickle
## IMPORTING CHECK TOOLS FUNCTIONS
from check_tools import check_testing, check_path 
from core_functions import create_grid, calc_mu, load_datafile, flatten_list_of_list, split_list
## IMPORTING TOOLS
import import_functions as import_funcs

## IMPORTING SCRIOPT
import stored_parallel_scripts

## IMPORTING MULTIPROCESSING FUNCTION
import multiprocessing

## IMPORTING DATE AND TIME
from datetime import datetime

## GLOBAL VARIABLES
from global_vars import MAX_N, MU_MAX, MU_MIN
    
## IMPORTING CORE FUNCTIONS
from core_functions import count_atoms_in_sphere, calc_unnorm_dist

## IMPORTING PICKLE FUNCTIONS
from pickle_functions import pickle_results, load_pickle_results

## FUNCTION TO EXTRACT INDEX LABEL
def extract_index_label_from_grid_xvg(file_name):
    ''' 
    The purpose of this function is to extract the label for a xvg file
    INPUTS:
        file_name: [str]
            file name, e.g. '3160_size.xvg'
    OUTPUTS:
        index_label: [int]
            index label
    '''
    ## GETTING INT
    index_label = int(file_name.split('_')[0])
    return index_label

### FUNCTION TO COMPUTE RESULTS LIST
def extract_num_occurances( loop_list, n_procs =2 ):
    '''
    This function extracts number of occurances
    INPUTS:
        xvg_file_list_sorted: [list]
            list of xvg file sorted
    OUTPUTS:
        results_list: [list]
            list of list for each core
    '''
    ## SPLITTING LINES
    list_splitted = split_list(alist = loop_list, wanted_parts = n_procs)
    
    ## COMPUTING NUBMER OF PROCESSORS
    if n_procs < 0:
        n_procs = multiprocessing.cpu_count()
    
    print( "\n--- Running %s on %s cores ---\n" %( stored_parallel_scripts.get_num_occurances.__name__, str(n_procs) ) )
    
    if n_procs != 1:
        ## GETTING MULTIPROCESSING
        pool = multiprocessing.Pool( processes = n_procs )
    
        ## RUNNING FUNCTION
        results_list = pool.map(stored_parallel_scripts.get_num_occurances, list_splitted)

        ## DECOMPOSING LIST
        results_list_flatten = flatten_list_of_list( results_list )
    else:
        ## SERIAL APPROACH
        results_list_flatten = stored_parallel_scripts.get_num_occurances(loop_list)
    return results_list_flatten

## EXTRATION OF RESULTS TO A NUMPY ARRAY
def get_occurances_array(num_grid_points, num_occurances_list ):
    '''
    This function generates an array based on the number of grid points and 
    number of occurances list
    INPUTS:
        num_grid_points: [int]
            number of grid points
        num_occurances_list: [list]
            list of each atom and its occupancy, e.g.
             ['98_size.xvg', array([4, 4, 5, ..., 5, 4, 6])],
             ['99_size.xvg', array([6, 5, 5, ..., 5, 5, 5])]]
    OUTPUTS:
        num_occurances_array: [np.array]
            number of occurances array (size = num_frames, grid opints)
    '''
    ## FINDING NUMER OF FRAMES
    num_frames = len(num_occurances_list[0][1])
    ## CREATING NUMPY ARRAY
    num_occurances_array = np.zeros( (num_frames, num_grid_points ))
    
    ## LOOPING THROUGH GRID POINTS AND GENERATING 
    for each_item in num_occurances_list:
        ## DEFINING XVG
        xvg_name = each_item[0]
        ## DEFINING LABEL
        label = extract_index_label_from_grid_xvg(file_name = xvg_name)
        ## STORING
        num_occurances_array[:, label] = each_item[1][:]
    return num_occurances_array
    
### FUNCTION TO GET XVG FILE LIST
def get_xvg_file_list(path_select_output):
    '''
    This function gets the xvg file list
    INPUTS:
        path_select_output: [str]
            path to selected output
    OUTPUTS:
        xvg_file_list_sorted: [list]
            list of xvg file sorted based on their names
    '''
    ## GLOBBING XVG FILES
    xvg_file_list = glob.glob( path_select_output + '/*.xvg'  )
    
    ## GETTING ALL BASE NAME
    xvg_file_basenames = [os.path.basename(each_file) for each_file in xvg_file_list]

    ## GETTING ALL BASE NAME
    xvg_file_index_label = [extract_index_label_from_grid_xvg(each_file) for each_file in xvg_file_basenames]    

    ## SORTTING LABELS
    indexes_sorted = np.argsort(xvg_file_index_label)
    
    ## GETTING FILE LIST SORTED
    xvg_file_list_sorted = [xvg_file_list[each_index] for each_index in indexes_sorted]
    # [0:100] <-- for debugging purposes
    
    return xvg_file_list_sorted

### MAIN FUNCTION TO GET NUMBER OF OCCURANCES ARRAY
def main_get_occurances_array(grid,
                              path_pickle,
                              n_procs = 3,
                              path_select_output = None,
                              rewrite = False):
    '''
    The purpose of this function is to get the occurances array that looks like:
        array([[5., 5., 5., ..., 0., 0., 0.],
                [5., 6., 5., ..., 0., 0., 0.],
                [5., 6., 6., ..., 0., 0., 0.]
    Shape: NUM_FRAMES x NUM_ATOMS
    INPUTS:
        grid: [np.array]
            grid values
        path_select_output: [str]
            path to selected output
        n_procs: [int]
            number of cores
        path_pickle: [str]
            path to specific pickle
        rewrite: [logical]
            True if you want to rewrite all pickling
    '''
    ## SEEING IF WE COULD GET THE ARRAY
    if os.path.isfile(path_pickle) is True and rewrite is False:
        ## PRINTING
        print("Loading %s"%(path_pickle) )
        ## LOADING PICKLE RESULTS
        num_occurances_array = load_pickle_results(path_pickle)[0][0]
    else:
        
        ## GENERATING ARRAY
        num_grid_points = grid.shape[0]
            
        ## GETTING XVG FILE LIST
        xvg_file_list_sorted = get_xvg_file_list(path_select_output = path_select_output)
        
        ## CHECKING IF LENGTHS MAKE SENSE
        if len(xvg_file_list_sorted) != num_grid_points:
            print("Error! Number of xvgs not equal number of grid points")
            print("Number of XVG: %d"%(len(xvg_file_list_sorted)) )
            print("Number of Grid points: %d"%(num_grid_points) )
        
        ## TRACKING THE TIME
        start = datetime.now()
        num_occurances_list = extract_num_occurances(loop_list = xvg_file_list_sorted,
                                                      n_procs = n_procs)    
        time_elapsed = datetime.now() - start
        print( 'Parallel time elapsed (hh:mm:ss.ms) {}\n'.format(time_elapsed) )
        
        ## GETTING NUMBER OF OCCURANCES ARRAY
        num_occurances_array = get_occurances_array( num_grid_points = num_grid_points,
                                                     num_occurances_list = num_occurances_list)
        try:
            ## STORING PICKLE
            pickle_results(results = [num_occurances_array],
                           pickle_path = path_pickle,
                           verbose = True,
                           )
        except MemoryError:
            pass
    return num_occurances_array

### FUCTION TO COMPUTE UNNORMALIZED P(N) DIST
def compute_p_N_array(grid, num_occurances_array, max_N, ):
    '''
    The purpose of this function is to compute the unnormalize distribution
    INPUTS:
        grid: [np.array]
            np.array of the grid values
        num_occurances_array: [np.array]
            array of occurences, shape = (num_frames, num_atoms)
    OUTPUTS:
        unnorm_p_N: [np.array, shape=(num_atoms, n_max)]
            unnormalized histogram of each occurances and its maxima
            e.g.:
                array([[3.0000e+00, 5.6000e+01, 9.4600e+02, ..., 1.1100e+03, 9.4000e+01,
                        3.0000e+00],
    '''
    ## GENERATING DISTRIBUTION CONTAINING THE NUMBER OF GRID POINTS AND MAX NUMBER OF ITEMS
    unnorm_p_N = np.zeros( shape = ( len(grid), max_N) )
    
    ## LOOPING THROUGH THE GRID
    for idx in range(len(grid)):
        ## DEFINING N
        N = num_occurances_array[:,idx]
        ## COMPUTING DISTRIBUTION AND STORING IT INTO THE INDEX
        unnorm_p_N[idx,:] = calc_unnorm_dist( N, d = max_N )
    return unnorm_p_N

### FUNCTION TO OUTPUT MU VALUES
def output_mu_values_dat(grid, mu, path_output_mu, verbose = True):
    '''
    This function outputs the data file given grids and mu file
    INPUTS:
        grid: [np.array]
            grid containing all xyz positions of atoms
        mu: [np.array]
            mu value corresponding to each grid point
        verbose: [logical]
            True if you want to print the details
    OUTPUTS:
        Data file with x, y, z, and mu values
    '''
    ## PRINTING
    if verbose is True:
        print( '--- Outputs written to %s' %(path_output_mu) )
    with open( os.path.join( path_output_mu) , 'w+' ) as outfile:
        outfile.write( '# x y z p_0\n\n' )                      
        for ii in range(len(mu)):
            outfile.write( '{:0.3f},{:0.3f},{:0.3f},{:0.3e}\n'.format( grid[ii,0], grid[ii,1], grid[ii,2], mu[ii] ) )
    return

## WRITING PDB FILE
def write_pdb_with_mu_values( path_pdb_file, grid, mu, mu_min, mu_max, box_dims  ):
    '''
    The purpose of this function is to write mu values for a PDB file. Note that 
    this only prints out values that are greater than 0. All values less than 
    0 are omitted from the PDB file
    
    INPUTS:
        path_pdb_file: [str]
            path to pdb file
        grid: [np.array]
            grid numpy array
        mu: [np.array]
            values for mu
        mu_min: [float]
            minimum mu values
        mu_max: [float]
            mu max values
        box_dims: [list]
            list of box dimensions
    OUTPUTS:
        PDB file stored in path pdb file
    
    '''
    # WRITE PDB FILE WITH PROBES COLORED BY MU    
    mu = ( mu - mu_min ) / ( mu_max - mu_min ) # red = hydrophobic; blue = hydrophilic
    print( '--- PDB file written to %s' %(  os.path.join( path_pdb_file) ) )
    pdbfile = open( os.path.join( path_pdb_file ), 'w+' )
    pdbfile.write( 'TITLE     frame t=1.000 in water\n' )
    pdbfile.write( 'REMARK    THIS IS A SIMULATION BOX\n' )
    pdbfile.write( 'CRYST1{:9.3f}{:9.3f}{:9.3f}{:>7s}{:>7s}{:>7s} P 1           1\n'.format( box_dims[0]*10, 
                                                                                             box_dims[1]*10,
                                                                                             box_dims[2]*10, 
                                                                                             '90.00', '90.00', '90.00' ) )
    pdbfile.write( 'MODEL        1\n' )
    for ndx, coord in enumerate( grid ):
        if mu[ndx] > 0.:
            line = "{:6s}{:5d} {:^4s}{:1s}{:3s} {:1s}{:4d}{:1s}   {:8.3f}{:8.3f}{:8.3f}{:6.2f}{:6.2f}          {:>2s}{:2s}\n".format( \
                    'ATOM', ndx+1, 'C', '', 'SUR', '', 1, '', coord[0]*10, coord[1]*10, coord[2]*10, 1.00, mu[ndx], '', '' )
            pdbfile.write( line )
        
    pdbfile.write( 'TER\n' )
    pdbfile.write( 'ENDMDL\n' )
    pdbfile.close()
    
    return
    
### FUNCTION TO GET DETAILS
def main_get_unnorm_p_N_array( grid, 
                               path_mu_pickle,
                               max_N,
                               n_procs = 3,
                               path_num_occurances_pickle = None,
                               path_select_output = None,
                               rewrite = False):
    '''
    The purpose of this function is to output the unnormalizes P(N) values. 
    INPUTS:
        grid: [np.array]
            grid values
        path_mu_pickle: [str]
            path to specific pickle for mu values
        max_N: [int]
            maximum N for p_N
        n_procs: [int]
            number of processors
        path_select_output: [str]
            path to selected output
        path_num_occurances_pickle: [str]
            path to specific pickle -- used for number of occurances
        rewrite: [logical]
            True if you want to rewrite all pickling
    '''

    ## SEEING IF AVAILABLE
    if os.path.isfile(path_mu_pickle) is True and rewrite is False:
        ## PRINTING
        print("Loading %s"%(path_mu_pickle) )
        ## LOADING PICKLE RESULTS
        unnorm_p_N = load_pickle_results(path_mu_pickle)[0][0]
        
    else:
        ## GETTING GRID AND NUMBER OF OCCURANCES
        num_occurances_array = main_get_occurances_array(grid = grid,
                                                         n_procs = n_procs,
                                                         path_pickle = path_num_occurances_pickle,
                                                         path_select_output = path_select_output,
                                                         rewrite = False)
        
        ## COMPUTING PROBABILITY OF FINDING POINTS
        unnorm_p_N = compute_p_N_array( grid = grid,
                                        num_occurances_array = num_occurances_array,
                                        max_N = max_N)
    
        ## STORING PICKLE
        pickle_results(results = [unnorm_p_N],
                       pickle_path = path_mu_pickle,
                       verbose = True,
                       )
    return unnorm_p_N

#%%
if __name__ == "__main__":
    
    ## SEE IF TESTING IS ON
    testing = check_testing()
    
    ## DEFINING MAXIMUM DISTRIBUTION
    max_N = MAX_N

    ## RUNNING TESTING    
    if testing == True:
        ## DEFINING MAIN SIMULATION
        main_sim=r"S:\np_hydrophobicity_project\simulations\NP_SPHERICAL"
        ## DEFINING SIMULATION NAME
        sim_name=r"MostlikelynpNVT_EAM_300.00_K_2_nmDIAM_C11CONH2_CHARMM36jul2017_Trial_1_likelyindex_1"
        ## DEFINING WORKING DIRECTORY
        wd = os.path.join(main_sim, sim_name)
        
        ## DEFINING GMX SELECTION OUTPUT WORKSLACE
        gmx_select_workspace = "gmx_select_output"
        
        ## DEFINING FOLDER WITHIN
        output_folder="output"
        
        ## DEFINING PATH TO OUTPUT
        path_select_output = os.path.join(wd, gmx_select_workspace, output_folder)
        
        ## DEFINING PATH TO GRID
        grid_folder="hydra-0_50000-0.1"
        
        ## DEFINING GRID FILE
        grid_file = "out_willard_chandler.dat"
        
        ## DEFINING PICKLE NAME
        pickle_name = "hydration.pickle" # FOR NUMBER OF OCCURANCES
        pickle_name_mu = "mu.pickle" # For MU VALUES
        
        ## DEFINING PICKLE PATH
        path_num_occurances_pickle = os.path.join(wd, gmx_select_workspace, pickle_name)
        ## DEFINING PATH MU PICKLE
        path_mu_pickle = os.path.join(wd, gmx_select_workspace, pickle_name_mu)
        
        ## DEFINING PATH TO GRID
        path_grid=os.path.join(wd, grid_folder, grid_file)
        
        ## DEFINING DETAILS TO OUTPUT FILE
        output_mu_value="out_mu.dat"
        path_output_mu = os.path.join(path_grid, output_mu_value)
        
        ## DEFINING REPRESENTATIVE GRO FILE
        gro_file = "sam_prod-0_50000-watO.gro"
        
        ## DEFINING PDB FILE
        output_pdb_file="out_mu.pdb"
        path_pdb_file = os.path.join(path_grid, output_pdb_file)
        
        ## DEFINING PATH TO GRO
        path_gro = os.path.join(wd, gro_file)
        
        ## DEFINING NUMBER OF CORES
        n_procs=2
       
        ## DEFINING REWRITE
        rewrite=False
    else:
        ## ADDING OPTIONS 
        from optparse import OptionParser # for parsing command-line optionsnp_hydration_hydration_map_only
        ## RUNNING COMMAND LINE PROMPTS
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        
        ## REPRESENTATION TYPE
        parser.add_option('--path_gro', dest = 'path_gro', help = 'Path of GRO file', default = '.', type=str)
        parser.add_option('--path_pdb_file', dest = 'path_pdb_file', help = 'Path of output PDB file', default = '.', type=str)
        
        ## SELECT FILE        
        parser.add_option('--path_select_output', dest = 'path_select_output', help = 'Path select output directory name', default = 'output', type=str)
        ## SELECT FILE        
        parser.add_option('--path_grid', dest = 'path_grid', help = 'Path to willard chandler', default = 'output', type=str)        
        ## PATH TO MU PICKLE    
        parser.add_option('--path_mu_pickle', dest = 'path_mu_pickle', help = 'Path to mu pickle', default = 'output', type=str)      
        ## PATH TO MU OUTPUT   
        parser.add_option('--path_mu_output', dest = 'path_mu_output', help = 'Path to mu output', default = 'output', type=str)
        ## PATH TO MU PICKLE    
        parser.add_option('--path_num_occurances_pickle', dest = 'path_num_occurances_pickle', help = 'Path num occurances pickle', default = 'output', type=str)

        ## REWRITE
        parser.add_option("--rewrite", dest="rewrite", default = False, type = str )

        ## DEFINING PROCS
        parser.add_option('--n_procs', dest = 'n_procs', help = 'Number of processors', default = 20, type=int)

        ### GETTING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## EXTRACTING DETAILS
        path_mu_pickle = options.path_mu_pickle
        path_num_occurances_pickle = options.path_num_occurances_pickle
        n_procs = options.n_procs
        path_select_output = options.path_select_output
        path_grid = options.path_grid
        path_gro = options.path_gro
        path_mu_output = options.path_mu_output
        path_pdb_file = options.path_pdb_file
        
        ## DEFINING REWRITE
        rewrite = options.rewrite

        ## CORRECTING FOR PLANAR SAM
        if type(rewrite) == str:
            if rewrite == "true":
                rewrite = True
            elif rewrite == "false":
                rewrite = False
            else:
                print("Error! planar_sam can only be True or False")
                print(rewrite)
                sys.exit()
        
    #%%
    #################
    ### MAIN CODE ###
    #################
    
    ## SEEING IF AVAILABLE
    ## LOADING GRID
    grid = load_datafile(path_to_file = path_grid)

    ## GETTING UNNORM PN
    unnorm_p_N = main_get_unnorm_p_N_array( grid = grid, 
                                            path_mu_pickle = path_mu_pickle,
                                            max_N = max_N,
                                            n_procs = n_procs,
                                            path_num_occurances_pickle = path_num_occurances_pickle,
                                            path_select_output = path_select_output,
                                            rewrite = rewrite)
    
    
    #%%
    ## CALCULATING MU
    mu = calc_mu( unnorm_p_N, d = max_N )
        
    ## OUTPUTTING MU VALUES
    output_mu_values_dat(grid, 
                         mu = mu, 
                         path_output_mu = path_mu_output, 
                         verbose = True)
    
    #%%
    ## LOADING GRO FILE
    gro_file = import_funcs.extract_gro(gro_file_path = path_gro)
    
    ## WRITING PDB FILE WITH MU VALUES
    write_pdb_with_mu_values(path_pdb_file = path_pdb_file,
                             grid = grid,
                             mu = mu,
                             mu_min = MU_MIN,
                             mu_max = MU_MAX,
                             box_dims = gro_file.Box_Dimensions)
    
    ## TODO: FUNCTION TO PLOT MU DISTRIBUTION
        
        


                                          
        