# -*- coding: utf-8 -*-
"""
check_tools.py
This script contains checking tools codes.

FUNCTIONS:
    check_path: checks path to ensure it is correct
    check_testing: checks to see if you are running in spyder


"""
import os
import sys

### FUNCTION TO GET THE PATH ON THE SERVER
def check_path(path):
    '''
    The purpose of this function is to change the path of analysis based on the current operating system. 
    INPUTS:
        path: [str]
            Path to analysis directory
    OUTPUTS:
        path (Corrected)
    '''
    ## IMPORTING MODULES
    import getpass
    
    ## CHANGING BACK SLASHES TO FORWARD SLASHES
    backSlash2Forward = path.replace('\\','/')
    
    ## CHECKING PATH IF IN SERVER
    if sys.prefix == '/usr' or sys.prefix == '/home/akchew/envs/cs760': # At server
        ## CHECKING THE USER NAME
        user_name = getpass.getuser() # Outputs $USER, e.g. akchew, or bdallin
        
        # Changing R: to /home/akchew
        path = backSlash2Forward.replace(r'R:','/home/' + user_name)
        path = path.replace(r'/Volumes/','/home/' )
    
    ## AT THE MAC
    elif '/Users/' in sys.prefix:
        ## GETTING SPLIT
        path_split = path.split('\\')
        
        ## DEFINING S AND R
        initial_path = path_split[0]            
        
        ## LISTING ALL VOLUMES
        volumes_list = os.listdir("/Volumes")
        ## LOOPING UNTIL WE FIND THE CORRECT ONE
        final_user_name =[each_volume for each_volume in volumes_list if 'akchew' in each_volume ][-1]
        if initial_path != 'S:':
            final_user_name =[each_volume for each_volume in volumes_list if 'akchew' in each_volume ][-1]
        else:
            final_user_name = 'shared'
        ## CHANGING R: to /Volumes
        path = backSlash2Forward.replace(path_split[0],'/Volumes/' + final_user_name)
    
    ## OTHERWISE, WE ARE ON PC -- NO CHANGES
    
    return path

### FUNCTION TO SEE IF TESTING SHOULD BE TURNED ON
def check_testing():
    '''
    The purpose of this function is to turn on testing if on SPYDER
    INPUTS:
        void
    OUTPUTS:
        True or False depending if you are on the server
    '''
    ## CHECKING PATH IF IN SERVER
    # if sys.prefix != '/Users/alex/anaconda' and sys.prefix != r'C:\Users\akchew\AppData\Local\Continuum\Anaconda3' and sys.prefix != r'C:\Users\akchew\AppData\Local\Continuum\Anaconda3\envs\py35_mat': 
    if any('SPYDER' in name for name in os.environ):
        print("*** TESTING MODE IS ON ***")
        testing = True
    else:
        testing = False
    return testing