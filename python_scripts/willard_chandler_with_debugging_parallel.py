# -*- coding: utf-8 -*-
"""
willard_chandler_with_debugging_parallel.py
This functions debug the WC interface using a parallel code. This uses all previously 
generated code from willard_chandler_with_debugging.py and tries to parallelize.

Written by: Alex K. Chew (12/12/2019)
"""

## IMPORTING MODULES
import os
import mdtraj as md
import numpy as np

## IMPORTING GLOBAL VARIABLES
from global_vars import WC_DEFAULTS

## IMPORTING FUNCTIONS
from willard_chandler_with_debugging import wc_interface

## IMPORTING PARALLEL CODE
from parallel import parallel, parallel_analysis_by_splitting_traj

## IMPORTONG TRACKING TIME
from time_functions import track_time

### FUNCTION TO COMPUTE GRID
def compute_wc_grid(traj, 
                    sigma,
                    mesh, 
                    contour,
                    n_procs = 1,
                    residue_list = ['HOH'],
                    print_freq = 1):
    '''
    The purpose of this function is to compute the WC grid in parallel. 
    INPUTS:
        traj: [md.traj]
            trajectory object
        sigma: [float]
            standard deviation of the gaussian distributions
        mesh: [list, shape = 3]
            list of mesh points
        contour: [float]
            contour level that is desired
        n_procs: [int]
            number of processors desired
        residue_list: [list]
            list of residues to look for when generating the WC surface
        print_freq: [int]
            frequency for printint output
    OUTPUTS:
        interface_points: [np.array, shape = (N, 3)]
            interfacial points
        interface: [obj]
            interfacial object
        avg_density_field: [np.array]
            average density field
    '''
    ## TRACKING TIME
    timer = track_time()
    ## DEFINING INPUTS FOR COMPUTE
    kwargs = {'traj'  : traj,
              'mesh'  : mesh,
              'sigma' : sigma,
              'residue_list': residue_list,
              'verbose': True, 
              'print_freq': print_freq, 
              'norm': False  }
    
    ## GETTING NORMALIZATION
    if n_procs == 1:
        kwargs['norm'] = True
    
    ## DEFINING INTERFACE
    interface = wc_interface(**kwargs)
    
    ## PARALELLIZING CODE
    if n_procs != 1:
        ## GETTING AVERAGE DENSITY FIELD
        avg_density_field = parallel_analysis_by_splitting_traj(traj = traj, 
                                                                class_function = interface.compute, 
                                                                n_procs = n_procs,
                                                                combine_type="sum")
        ## NORMALIZING
        avg_density_field /= traj.time.size
    else:
        ## GETTING AVERAGE DENSITY FIELD
        avg_density_field = interface.compute(traj = traj)
        
    ## AFTER COMPLETION, GET INTERFACE
    interface_points = interface.get_wc_interface_points(density_field = avg_density_field, 
                                                         contour = contour)
        
    ## OUTPUTTING TIME
    timer.time_elasped()
    return interface_points, interface, avg_density_field


#%% RUN FOR TESTING PURPOSES 
if __name__ == "__main__":
    
    ## DEFINING sigma, CONTOUR, AND MESH
    sigma = WC_DEFAULTS['sigma']
    contour = WC_DEFAULTS['contour']
    ## DEFINING MESH
    mesh = WC_DEFAULTS['mesh']
    #%%
    ##########################
    ### LOADING TRAJECTORY ###
    ##########################
    ## DEFINING MAIN SIMULATION
    main_sim=r"S:\np_hydrophobicity_project\simulations\191210-annealing_try4"
    ## DEFINING SIM NAME
    sim_name=r"FrozenGoldPlanar_300.00_K_dodecanethiol_10x10_CHARMM36jul2017_intffGold_Trial_1-50000_ps"
    ## DEFINING WORKING DIRECTORY
    wd = os.path.join(main_sim, sim_name)
    ## DEFINING GRO AND XTC
    gro_file = r"sam_prod-0_1000-watO_grid.gro"
    xtc_file = r"sam_prod-0_1000-watO_grid.xtc"
    ## DEFINING PATHS
    path_gro = os.path.join(wd, gro_file)
    path_xtc = os.path.join(wd, xtc_file)
    
    ## PRINTING
    print("Loading trajectory")
    print(" --> XTC file: %s"%(path_xtc) )
    print(" --> GRO file: %s"%(path_gro) )
    ## LOADING TRAJECTORY
    traj = md.load(path_xtc, top = path_gro)
    #%%
    
    ## GETTING INTERFACE POINTS
    interface_points, interface, avg_density_field = compute_wc_grid(traj = traj[0:10], 
                                                                     sigma = sigma, 
                                                                     mesh = mesh, 
                                                                     contour = contour,
                                                                     n_procs = 1,
                                                                     residue_list = ['HOH'],
                                                                     print_freq = 1,
                                                                     )
    #%%
    ## GETTING INTERFACE POINTS
    interface_points_2, interface_2, avg_density_field_2 = compute_wc_grid(traj = traj[0:10], 
                                                                           sigma = sigma, 
                                                                           mesh = mesh, 
                                                                           contour = contour,
                                                                           n_procs = 2,
                                                                           residue_list = ['HOH'],
                                                                           print_freq = 1,
                                                                           )
    
    ## TOTALLING
    total = np.sum(interface_points_2 - interface_points)
    if total != 0.0:
        print("Error! Something is not right between serial and parallel codes!")
    # THIS VALUE SHOULD BE 0. OTHERWISE, THE MULTIPROCESSING DID NOT WORK!
    
    #%%
    
    ## GETTING WC INTERFACE
    interface_points = interface.get_wc_interface_points(density_field = avg_density_field, 
                                                         contour = contour)
    
    ## PLOTTING
    interface.mlab_plot_density_field(density_field = avg_density_field,
                                      interface_points = interface_points,
                                      num_grid_pts = interface.num_grid_pts,
                                      grid = interface.grid,
                                      pos = None, 
                                      )
    
    #%%
    
    
    import matplotlib
    # matplotlib.use('TkAgg')
    matplotlib.use('Qt4Agg')
    import matplotlib.pyplot as plt
    # import plot_functions as plot_funcs
    
    
    ## GETTING RESHAPES
    avg_field_reshaped = avg_density_field.reshape(interface.num_grid_pts)
    
    ## AVERAGING ALONG THE X, Y AXIS
    avg_field_z = np.mean(np.mean(avg_field_reshaped, axis = 0),axis=0)
    
    ## GENERATING NUMPY ARRAY
    z_array = np.linspace(0, interface.box[-1], int(interface.num_grid_pts[-1]) )
    
    ## CREATING PLOT
    fig, ax = plot_funcs.create_plot()
    
    ## CREATING HISTOGRAM PLOT
    ax.plot(z_array, avg_field_z, color="k")
    
    
    
    
    
    