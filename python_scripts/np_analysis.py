# -*- coding: utf-8 -*-
"""
np_analysis.py
This is the main code to run analysis on nanoparticles coated with SAMs.

INSTALLATION NOTES:
    - May need ski-kit image
        pip install scikit-image
        

"""
##############################################################################
# Imports
##############################################################################
import sys, os
if "linux" in sys.platform and "DISPLAY" not in os.environ:
    import matplotlib
    matplotlib.use('Agg') # turn off interactive plotting

#if r'R:/analysis_scripts' not in sys.path:
#    sys.path.append(r'R:/analysis_scripts')
#    
import os
import numpy as np
import mdtraj as md
from datetime import datetime
from parallel import parallel

## IMPORTING GLOBAL VARIABLES
from global_vars import WC_DEFAULTS, TECHNICAL_DEFAULTS, MAX_N,R_CUTOFF

## CUSTOM IMPORTS
from hydration_map import hydration_map
from core_functions import create_grid, calc_mu, load_datafile
from check_tools import check_testing, check_path 

#%%
#################
### FUNCTIONS ###
#################

### FUNCTION TO COMPUTE HYDRATION MAPS
def calc_hydramap( traj, out_path, wcdatfilename, r_cutoff = 0.33, max_N = 10, n_procs = 28 ):
    r"""
    This function computes the hydration maps given the trajectory and data file of 
    WC-interface.
    INPUTS:
        traj: [obj]
            trajectory object
        out_path: [str]
            output path that stores the WC interface
        wcdatfilename: [str]
            datafile for the willard-chandler file name
        r_cutoff: [float]
            cutoff for the hydration maps
        max_N: [int]
            max 
    OUTPUTS:
        hydration map results
    """
    ## LOADING DATA
    grid = load_datafile( os.path.join(out_path, wcdatfilename) )
    ## DEFINING INPUTS
    kwargs = { 'grid': grid, 'r_cutoff': r_cutoff, 'max_N': max_N, 'periodic': True }
    
    ## TRACKING THE TIME
    start = datetime.now()
    hydra_map = parallel( traj, hydration_map, kwargs, n_procs = n_procs, average = False )
    time_elapsed = datetime.now() - start
    print( 'Time elapsed (hh:mm:ss.ms) {}\n'.format(time_elapsed) )
    
    return hydra_map.results

#%%
##############################################################################
# Load analysis inputs and trajectory
##############################################################################   
if __name__ == "__main__":
    # --- TESTING ---
    
    ## SEE IF TESTING IS ON
    testing = check_testing()
    
    ## TECHNICAL DEFAULT VARIABLES
    # n_procs = TECHNICAL_DEFAULTS['n_procs']
    # n_frames = TECHNICAL_DEFAULTS['n_frames']
    n_chunks = TECHNICAL_DEFAULTS['n_chunks']
    
    ## WILLARD-CHANDLER VARIABLES
    alpha = WC_DEFAULTS['alpha']
    contour = WC_DEFAULTS['contour']
    
    ## DEFINING MU MIN AND MAX
    MU_MIN=8
    MU_MAX=12
    
    ## DEFINING MAX NUMBER OF MOLECULES
    max_N = MAX_N # max number of waters in histogram
    r_cutoff = R_CUTOFF # Radius of cavity
    
    ## RUNNING TESTING    
    if testing == True:
        ## DEFINING MAIN SIMULATION
        main_sim=r"S:\np_hydrophobicity_project\simulations\NP_SPHERICAL"
        ## DEFINING SIM NAME
        sim_name=r"np_spherical_300K_2_nm_C11COOH_CHARMM36_mlc"
        sim_name=r"np_spherical_300K_2_nm_dodecanethiol_CHARMM36_mlc"
        ## DEFINING WORKING DIRECTORY
        wd = os.path.join(main_sim, sim_name)
        ## DEFINING GRO AND XTC
        gro_file = r"sam_prod.gro"
        xtc_file = r"sam_prod.xtc"
        
        ## DEFINING PREFIX
        # output_prefix = "out"
        output_prefix = "sam_prod"
        
        ## DEFINING LOGICALS
        planar_sam = False
        
        ## DEFINING NUMBER OF RAMES
        n_frames = 50000
        
        ## DEFINING NUMBER OF PROCESSORS
        n_procs = 2
        ## DEFINING PLOT ONLY
        plot_only = True
        
        ## DEFINING MESH
        mesh = WC_DEFAULTS['mesh']
        
        ## DEFINING OUTPUT FILE
        output_file = "output_files_test"
        
        
    else:
        ## ADDING OPTIONS 
        from optparse import OptionParser # for parsing command-line options
        ## RUNNING COMMAND LINE PROMPTS
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        
        ## REPRESENTATION TYPE
        parser.add_option('--path', dest = 'simulation_path', help = 'Path of simulation', default = '.', type=str)
        
        ## DEFINING GRO AND XTC FILE
        parser.add_option('--gro', dest = 'gro_file', help = 'Name of gro file', default = 'sam.gro', type=str)
        parser.add_option('--xtc', dest = 'xtc_file', help = 'Name of xtc file', default = 'sam.xtc', type=str)
        
        ## OUTPUT PREFIX
        parser.add_option('--output_file', dest = 'output_file', help = 'Output directory name', default = 'output', type=str)
        parser.add_option('--output_prefix', dest = 'output_prefix', help = 'Output prefix', default = 'output', type=str)

        ## OUTPUT PREFIX
        parser.add_option('--n_frames', dest = 'n_frames', help = 'Number of frames', default = 50000, type=int)
        parser.add_option('--n_procs', dest = 'n_procs', help = 'Number of processors', default = 20, type=int)
        
        ## DEFINING MESH SIZE
        parser.add_option('--mesh', dest = 'mesh', help = 'Mesh size', default = 0.1, type=float)
        
        ## DEFINING PLANAR CASE
        parser.add_option("--planar", dest="planar_sam", default = False, type=str )
        # parser.add_option("--planar", dest="planar_sam", action="store_true", default = False )
        
        ### GETTING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        ## DEFINING OUTPUTS
        # WORKING DIRECTORY
        wd = options.simulation_path
        # GRO/XTC
        gro_file = options.gro_file
        xtc_file = options.xtc_file
        
        ## OUTPUT PREFIX
        output_prefix = options.output_prefix
        
        ## LOGICALS
        planar_sam = options.planar_sam
        ## DEFINING PLOT ONLY
        plot_only = False
        
        ## TECHNICAL DETAILS
        n_frames = options.n_frames
        n_procs = options.n_procs
        
        ## DEFINING MESH SIZE
        mesh = options.mesh
        
        ## DEFINING OPTION DIRECTORY
        output_file = options.output_file
        
        ## CORRECTING FOR PLANAR SAM
        if type(planar_sam) == str:
            if planar_sam == "True":
                planar_sam = True
            elif planar_sam == "False":
                planar_sam = False
            else:
                print("Error! planar_sam can only be True or False")
                sys.exit()
            
        
    
    ## COMPUTING CHUNK SIZE
    chunk_size = n_frames // n_chunks
    ## DEFINING ANALYSIS VARIABLE
    analysis = [ 'hydration_map', ]
    
    ## LOGICALS
    write_pdb = True
    make_grid = True
    calc_distribution = True
    
    ## DEFINING PATHS
    path_gro = os.path.join(wd, gro_file)
    path_xtc = os.path.join(wd, xtc_file)
    
    ## OUTPUT PATHS
    out_path = os.path.join( wd, output_file)
    wcdatfilename = output_prefix + '_willard_chandler.dat'
    wcpdbfilename = output_prefix + '_willard_chandler.pdb'
    datfilename = output_prefix + '_hydration_map.dat'
    csvfilename = output_prefix + '_limits.csv'
    pdbfilename = output_prefix + '_hydration_map.pdb'
    distfilename = output_prefix + '_mu_distribution.csv'
    xprojfilename = output_prefix + '_mu_xproj.csv'
            
##############################################################################
# Execute/test the script
##############################################################################
    #%%
    if not plot_only:
        n_traj = 0
        for traj in md.iterload( path_xtc, top = path_gro, chunk = chunk_size ):
            if traj.time.size > 1: # ensure there are more than one frame
                print( "%d of %d" %( n_traj+1, n_chunks ) )
                if n_traj < 1:
                    if 'willard_chandler' in analysis or make_grid:
                        grid = create_grid(traj, 
                                           out_path, 
                                           wcdatfilename, 
                                           wcpdbfilename, 
                                           alpha = alpha, 
                                           mesh = mesh, 
                                           contour = contour, 
                                           write_pdb = write_pdb, 
                                           n_procs = n_procs )
                        make_grid = False
                    else:
                        grid = create_grid(traj, 
                                           out_path, 
                                           wcdatfilename, 
                                           wcpdbfilename, 
                                           alpha = alpha, 
                                           mesh = mesh, 
                                           contour = contour, 
                                           write_pdb = False, 
                                           n_procs = n_procs )
                
                if 'hydration_map' in analysis:
                    if n_traj < 1:
                        data = calc_hydramap( traj, out_path, wcdatfilename, r_cutoff = r_cutoff, max_N = max_N, n_procs = n_procs )
                    else:
                        wcdata = calc_hydramap( traj, out_path, wcdatfilename, r_cutoff = r_cutoff, max_N = max_N, n_procs = n_procs )
                        data += wcdata
        
                n_traj += 1
        
        ## CALCULATING MU
        mu = calc_mu( data, d = 11 )
    
        print( '--- Outputs written to %s' %(out_path) )
        outfile = open( os.path.join( out_path , datfilename) , 'w+' )
        outfile.write( '# x y z p_0\n\n' )                      
        for ii in range(len(mu)):
            outfile.write( '{:0.3f},{:0.3f},{:0.3f},{:0.3e}\n'.format( grid[ii,0], grid[ii,1], grid[ii,2], mu[ii] ) )
            
        outfile.close()
    else:
        traj = md.load_frame( path_xtc, top = path_gro, index = 0 )
        print( '*** Trajectory loaded ***' )
        print( traj )
    
    ## LOADING THE DATA FILE
    data = load_datafile( os.path.join(out_path, datfilename) )
    
    ## DEFINING FOR PLANAR SAMS
    if planar_sam:
        print( "*** SAM is planar ***" )
        avg_z = data[:,2].mean()
        mask = data[:,2] < avg_z
        data = data[mask,:]
    
    ## COMPUTING MIN AND MAX FOR MU
    mu = data[:,3]
    mu_min = mu.min()
    mu_max = mu.max()
    
    print( "min mu_ex: %s, max mu_ex: %s" %( mu_min, mu_max ) )
    outfile = open( os.path.join(out_path, csvfilename), 'w+' )
    outfile.write( "# min mu_ex: %s, max mu_ex: %s\n" %( str(mu.min()), str(mu.max()) ) )
    outfile.close()
    mu_min = MU_MIN # 5.0  # mu.min()
    mu_max = MU_MAX # 17.0 # mu.max()
    
    ##############################
    ### COMPUTING DISTRIBUTION ###
    ##############################
    if calc_distribution:
        step_size = 0.2
        xs = np.arange( 0, 20., step_size ) + 0.5 * step_size
        ps = np.histogram( mu, bins = xs.size, range = ( 0, 20 ) )[0]
        norm_ps = ps / float( ps.sum() )
        print( '--- Distribution data writen to %s' %( os.path.join( out_path , distfilename) ) )
        distfile = open ( os.path.join( out_path , distfilename), 'w+' )
        for x, y in zip( xs, norm_ps ):
            line = "{:8.3f},{:8.3e}\n".format( x, y )
            distfile.write( line )
        
        distfile.close()
        
    
    ########################
    ### WRITING PDB FILE ###
    ########################
    if write_pdb:
        # WRITE PDB FILE WITH PROBES COLORED BY MU    
        mu = ( mu - mu_min ) / ( mu_max - mu_min ) # red = hydrophobic; blue = hydrophilic
        print( '--- PDB file written to %s' %(  os.path.join( out_path , pdbfilename) ) )
        pdbfile = open( os.path.join( out_path , pdbfilename), 'w+' )
        pdbfile.write( 'TITLE     frame t=1.000 in water\n' )
        pdbfile.write( 'REMARK    THIS IS A SIMULATION BOX\n' )
        pdbfile.write( 'CRYST1{:9.3f}{:9.3f}{:9.3f}{:>7s}{:>7s}{:>7s} P 1           1\n'.format( traj.unitcell_lengths[0,0]*10, traj.unitcell_lengths[0,1]*10, traj.unitcell_lengths[0,2]*10, '90.00', '90.00', '90.00' ) )
        pdbfile.write( 'MODEL        1\n' )
        for ndx, coord in enumerate( data ):
            if mu[ndx] > 0.:
                line = "{:6s}{:5d} {:^4s}{:1s}{:3s} {:1s}{:4d}{:1s}   {:8.3f}{:8.3f}{:8.3f}{:6.2f}{:6.2f}          {:>2s}{:2s}\n".format( \
                        'ATOM', ndx+1, 'C', '', 'SUR', '', 1, '', coord[0]*10, coord[1]*10, coord[2]*10, 1.00, mu[ndx], '', '' )
                pdbfile.write( line )
            
        pdbfile.write( 'TER\n' )
        pdbfile.write( 'ENDMDL\n' )
        pdbfile.close()
        
        

