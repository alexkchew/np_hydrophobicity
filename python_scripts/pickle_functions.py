# -*- coding: utf-8 -*-
"""
pickle_functions.py
This code  contains all pickle functioning

FUNCTIONS:
    pickle_results: 
        pickles the results
    load_pickle_results:
        loads the pickle results
    
Updated by Alex K. Chew & Brad C. Dallin (10/28/2019)
"""
import sys
import pickle
import os

### FUNCTION TO STORE RESULTS
def pickle_results(results, pickle_path, pickle_protocol = 4,verbose = False):
    '''
    This function stores the results for pickle.
    INPUTS:
        results: [list]
            list of results
        pickle_path: [str]
            path to pickle location
        verbose: [logical]
            True if you want verbosely print
    OUTPUTS:
        no output text, just store pickle
    '''
    ## VERBOSE
    if verbose is True:
        print("Storing pickle at: %s"%(pickle_path) )
    ## STORING PICKLES
    with open( os.path.join( pickle_path ), 'wb') as f:  # Python 3: open(..., 'wb')
        pickle.dump([results], f, protocol=pickle_protocol)  # <-- protocol 2 required for python2   # -1
    return 

### MAIN FUNCTION TO LOAD PICKLE GIVEN A PICKLE DIRECTORY
def load_pickle_results( file_path, verbose = False ):
    '''
    The purpose of this function is to load pickle for a given file path
    INPUTS:
        file_path: [str]
            path to the pickle file
        verbose: [str]
            True if you want verbose pickling
    OUTPUTS:
        results: [obj]
            results from your pickle
    '''
    # PRINTING
    if verbose is True:
        print("LOADING PICKLE FROM: %s"%(file_path) )    
    ## LOADING THE DATA
    with open(file_path,'rb') as f:
        # file_path = pickle.load(f) ## ENCODING LATIN 1 REQUIRED FOR PYTHON 2 USAGE
        if sys.version_info[0] > 2:
            results = pickle.load(f, encoding='latin1') ## ENCODING LATIN 1 REQUIRED FOR PYTHON 2 USAGE
        elif sys.version_info[0] == 2: ## ENCODING IS NOT AVAILABLE IN PYTHON 2
            results = pickle.load(f) ## ENCODING LATIN 1 REQUIRED FOR PYTHON 2 USAGE
        else:
            print("ERROR! Your python version is not 2 or greater! We cannot load pickle files below Python 2!")
            sys.exit()
    return results
