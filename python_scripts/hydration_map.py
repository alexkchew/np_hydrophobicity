# -*- coding: utf-8 -*-
# NOCW
# This file is part of the hydrophobicity project

R"""
The :class:`interfacial_hydrophobicity.hydration_map` module contains tools to generate a 2d 
hydration map.
"""

##############################################################################
# Imports
##############################################################################

import numpy as np
from core_functions import count_atoms_in_sphere, calc_unnorm_dist

## for figure generation
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import scipy.interpolate as interpolate

__all__ = [ 'hydration_map' ]

##############################################################################
# Hydration map class
##############################################################################
class hydration_map:
    R'''
    This function computes the hydration maps given the grid. 
    INPUTS:
        grid: [np.array]
            grid that contains the interface atoms
        r_cutoff: [float]
            cutoff of radius
        max_N: [int]
            maximum number of atoms within a shell, 
            selected to be 11 by default as it is larger than the largest possible value
        periodic: [logical, default=True]
            True if you want to account for periodic conditions
    OUTPUTS:
    
    FUNCTIONS:
        compute
    
    '''
    def __init__( self, grid, r_cutoff = 0.33, max_N = 10, periodic = True,
                  residue_names = [ 'HOH' ] ): 
        ## STORING INPUT VALUES        
        self.grid = grid
        self.r_cutoff = r_cutoff
        self.max_N = max_N + 1
        self.periodic = periodic
        self.residue_names = residue_names
    
    ## FUNCTION TO COMPUTE HYDRATION MAPS
    def compute( self, traj):
        R"""
        INPUT:
            traj: [mdtraj.trajectory] 
                trajectory imported from mdtraj
            residue_names: [list]
                list of residue names to look at
        OUTPUTS:
            unnorm_p_N: [np.array, shape=(num_grid_points, N_atoms)]
                Unnormalized probability distribution function that computes the 
                number of water molecules within the grid point. 
        """
        n_frames = traj.time.size
        print("--- Calculating hydration map for %s simulations windows ---" % (str(n_frames)) )
              
        # DETERMINE OXYGEN ATOMS IN WATER (THIS ASSUMES WE WANT TO IGNORE HYDROGENS)
        atom_ndx = []
        # STORING ATOMS THAT ARE NOT OXYGEN ATOMS OF WATER, WHICH COULD BE USED AS AN EXAMPLE ATOM FOR GRIDDING
        atom_not_in_ndx = [] 
        ## LOOPING THROUGH ATOMS
        for atom in traj.topology.atoms:
            if 'H' not in atom.name and atom.residue.name in self.residue_names: # == 'HOH'
                atom_ndx.append( atom.index )
            else:
                atom_not_in_ndx.append( atom.index )
        # atom_ndx = [ atom.index for atom in traj.topology.atoms if 'H' not in atom.name and atom.residue.name == 'HOH' ]
        
        ## GENERATING DISTRIBUTION CONTAINING THE NUMBER OF GRID POINTS AND MAX NUMBER OF ITEMS
        unnorm_p_N = np.zeros( shape = ( len(self.grid), self.max_N ) )
        ## LOOPING THROUGH THE GRID
        for idx in range(len(self.grid)):
            ## COPYING TRAJ
            copied_traj = traj[:]
            ## DECIDING WHICH ATOM TO CHANGE, USEFUL FOR MDTRAJ DISTANCES FUNCTION
            atom_index_to_change = atom_not_in_ndx[0] # First atom index that were not interested in
            ## CHANGING THE POSITION OF ONE ATOM IN THE SYSTEM TO BE THE GRID ATOM
            copied_traj.xyz[:,atom_index_to_change,:] = np.tile( self.grid[idx,:], (n_frames, 1) )
            ## GENERATING PAIRS TO THE SYSTEM
            pairs = np.array([ [ atom_index, atom_index_to_change ] for atom_index in atom_ndx ])
            ## COMPUTE NUMBER OF ATOMS WITHIN A SPHERE
            N = count_atoms_in_sphere(traj = copied_traj, 
                                      pairs = pairs, 
                                      r_cutoff = self.r_cutoff, 
                                      d = self.max_N, 
                                      periodic = self.periodic )
            ## COMPUTING DISTRIBUTION AND STORING IT INTO THE INDEX
            unnorm_p_N[idx,:] = calc_unnorm_dist( N, d = self.max_N )
            
#        return np.hstack(( self.grid, p_0[:,np.newaxis] ))
        return unnorm_p_N
        
    ## FUNCTION TO CREATE 2D PLOT
    @staticmethod
    def plot_map( data, out_path, out_name = "hydration_map" ):
        '''
        The purpose of this function is to plot the hydration map details. 
        Note that this only plots 2D hydration map information
        INPUTS:
            data: [np.array]
                data of x, y, z positions
        OUTPUTS:
            
        '''            
        x, y, z = data[:,0], data[:,1], data[:,2]
        print( 'min z = %s' %( z[z>0].min() ) )
        xi, yi = np.linspace( x.min(), x.max(), 500 ), np.linspace( y.min(), y.max(), 500 )
        xi, yi = np.meshgrid( xi, yi )
        zi = interpolate.griddata( (x, y), z, (xi, yi), method = 'cubic' )
        zi[zi<0] = z[z>0].min()
        
        z_min = zi.min()
        z_max = zi.max()
        print( 'min p(0): %s; max p(0): %s' %( z_min, z_max ) )            
        plt.figure()
        plt.imshow( zi, vmin = z_min, vmax = z_max, origin = 'lower', norm = LogNorm(),
                    extent = [ x.min(), x.max(), y.min(), y.max() ], aspect = 'auto', cmap = plt.get_cmap( 'coolwarm' ) )
        plt.xlabel( 'x (nm)' )
        plt.ylabel( 'y (nm)' )
        cbar = plt.colorbar( )
        cbar.set_label( r'$p(N)$' )
#        plt.savefig( out_path + out_name + '_p0_full.png', format='png', dpi=1000, bbox_inches='tight' )
#        plt.savefig( out_path + out_name + '_p0_full.svg', format='svg', dpi=1000, bbox_inches='tight' )

        plt.figure()
        plt.imshow( zi, vmin = z_min, vmax = z_max, origin = 'lower', norm = LogNorm(),
                    extent = [ x.min(), x.max(), y.min(), y.max() ], aspect = 'auto', cmap = plt.get_cmap( 'coolwarm' ) )
        plt.xlabel( 'x (nm)' )
        plt.xlim([ 0.75, 2.75 ])
        plt.xticks( [ 1.0, 1.5, 2.0, 2.5 ] )
        plt.ylabel( 'y (nm)' )
        plt.ylim([ 1.0, 3.0 ])
        plt.yticks( [ 1.25, 1.75, 2.25, 2.75 ] )
        cbar = plt.colorbar( )
        cbar.set_label( r'$p(N)$' )
#        plt.savefig( out_path + out_name + '_p0.png', format='png', dpi=1000, bbox_inches='tight' )
#        plt.savefig( out_path + out_name + '_p0.svg', format='svg', dpi=1000, bbox_inches='tight' )
           
        z_min = 4.0 # np.min(-np.log(zi[zi>0]))
        z_max = 6.0 # np.max(-np.log(zi[zi>0]))
        print( 'min mu: %s; max mu: %s' %( z_min, z_max ) )           
        plt.figure()
        plt.imshow( -np.log(zi), vmin = z_min, vmax = z_max, origin = 'lower',
                   extent = [ x.min(), x.max(), y.min(), y.max() ], aspect = 'auto', cmap = plt.get_cmap( 'RdBu' ) )
        plt.xlabel( 'x (nm)' )
        plt.ylabel( 'y (nm)' )
        cbar = plt.colorbar( )
        cbar.set_label( r'$\mu^{ex} (kT)$' )
        cbar.set_ticks([ 4.2, 4.6, 5.0, 5.4, 5.8 ])
        cbar.set_ticklabels([ 4.2, 4.6, 5.0, 5.4, 5.8 ])
        plt.tight_layout()
#        plt.savefig( out_path + out_name + '_mu_full.png', format='png', dpi=1000, bbox_inches='tight' )
#        plt.savefig( out_path + out_name + '_mu_full.svg', format='svg', dpi=1000, bbox_inches='tight' )
               
        plt.figure()
        plt.imshow( -np.log(zi), vmin = z_min, vmax = z_max, origin = 'lower',
                   extent = [ x.min(), x.max(), y.min(), y.max() ], aspect = 'auto', cmap = plt.get_cmap( 'RdBu' ) )
        plt.xlabel( 'x (nm)' )
        plt.xlim([ 0.75, 2.75 ])
        plt.xticks( [ 1.0, 1.5, 2.0, 2.5 ] )
        plt.ylabel( 'y (nm)' )
        plt.ylim([ 1.0, 3.0 ])
        plt.yticks( [ 1.25, 1.75, 2.25, 2.75 ] )
        cbar = plt.colorbar( )
        cbar.set_label( r'$\mu^{ex} (kT)$' )
        cbar.set_ticks([ 4.2, 4.6, 5.0, 5.4, 5.8 ])
        cbar.set_ticklabels([ 4.2, 4.6, 5.0, 5.4, 5.8 ])
        plt.tight_layout()
#        plt.savefig( out_path + out_name + '_mu.png', format='png', dpi=1000, bbox_inches='tight' )
#        plt.savefig( out_path + out_name + '_mu.svg', format='svg', dpi=1000, bbox_inches='tight' )
        
    #%%
    
    ## DEBUGGING
    if __name__ == "__main__":
        ## LOADING MODULES
        import os
        from core_functions import create_grid, calc_mu, load_datafile
        import mdtraj as md
        
        ## DEFINING MAIN SIMULATION
        main_sim=r"S:\np_hydrophobicity_project\simulations\191010-Most_likely_np_sims_NVT_charged_groups"
        ## DEFINING SIM NAME
        sim_name=r"MostlikelynpNVT_EAM_300.00_K_2_nmDIAM_C11COO_CHARMM36jul2017_Trial_1_likelyindex_1"
        
        ## DEFINING FULL SIM PATH
        path_sim = os.path.join(main_sim, sim_name)
        
        ## DEFINING GRO AND XTC
        gro_file = r"sam_prod.gro"
        xtc_file = r"sam_prod_48000.xtc"
        
        ## DEFINING OUTPUT IFLE
        output_file="hydra-0_50000-0.1"
        output_wc_dat_file="out_willard_chandler.dat"
        
        ## DEFINING WC SURFACE PATH
        path_wc_dat_file = os.path.join(path_sim,
                                        output_file,
                                        output_wc_dat_file)
        
        ## LOADING GRID
        grid = load_datafile(path_to_file = path_wc_dat_file)
        
        ## LOADING TRAJECTORY
        # traj = md.load( )
        
#        
#        ## GETTING GRID
#        grid = create_grid(traj, 
#                           out_path, 
#                           wcdatfilename, 
#                           wcpdbfilename, 
#                           alpha = alpha, 
#                           mesh = mesh, 
#                           contour = contour, 
#                           write_pdb = write_pdb, 
#                           n_procs = n_procs )
        
    
        
