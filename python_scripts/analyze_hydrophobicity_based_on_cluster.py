# -*- coding: utf-8 -*-
"""
analyze_hydrophobicity_based_on_cluster.py
The purpose of this function is to analyze mu values based on the clusters 
that are defined by spectral clustering. 

Written by: Alex K. Chew (12/08/2019)
"""

## IMPORTING MODULES
import os
import sys
import numpy as np

## LOADING PICKLE FUNCTIONS
import pickle_functions as pickle_funcs

## CHECK TESTING TOOL
from check_tools import check_testing

## IMPORTING FUNCTION FROM CLUSTER
from cluster_grid_points import find_pickle_name_based_on_clustering, plot_grid_clusters

## IMPORTING CORE FUNCTIONS
from core_functions import create_grid, calc_mu, load_datafile, flatten_list_of_list, split_list

## PLOTTING FUNCTIONS
import plot_functions as plot_funcs

#%% MAIN FUNCTION
if __name__ == "__main__":
    
    ## SEE IF TESTING IS ON
    testing = check_testing()
    
    ## RUNNING TESTING    
    if testing == True:
        ## DEFINING PATH
        path_sim = r"S:\np_hydrophobicity_project\simulations\NP_SPHERICAL"
        ## DEFINING SPECIFIC SIM
        specific_sim = r"MostlikelynpNVT_EAM_300.00_K_2_nmDIAM_C11COOH_CHARMM36jul2017_Trial_1_likelyindex_1"
        ## DEFINING PATH TO SIMULATION
        full_path_to_sim = os.path.join(path_sim,
                                        specific_sim)
    
        ## DEFINING FIGURE LOCATION AND DETAILS
        store_fig_loc = r"C:\Users\akchew\Box Sync\VanLehnGroup\2.Research Documents\Alex_RVL_Meetings\20191202\Images\spectral_clustering_output"
        save_fig = True
    
        ## DEFINING GRID
        hyd_analysis_folder = "hyd_analysis"
        grid_folder = r"grid-0_1000-0.1"
        grid_dat_file = r"out_willard_chandler.dat"
    
        ## DEFINING NUMBER OF CLUSTERS
        num_clusters = 10
        num_within_group = 5
        # None
        # 200
        
        ## DEFINING NUMBER OF JOBS
        n_jobs = -1
        
        
    ## PATH TO GRID FOLDER
    path_grid_folder = os.path.join(full_path_to_sim,
                                    hyd_analysis_folder,
                                    grid_folder)
    
    ## DEFINING PATH
    path_grid_file = os.path.join(path_grid_folder,
                                  grid_dat_file,
                                  )
    
    ## GETTING PICKLE NAME
    pickle_name = find_pickle_name_based_on_clustering(num_within_group = num_within_group, 
                                                       num_clusters = num_clusters)
    
    ## DEFINING PICKLE PATH
    path_pickle = os.path.join( path_grid_folder, pickle_name )
    
    ## LOADING GRID FILE
    grid = load_datafile(path_to_file = path_grid_file)
    
    ## LOADING CLUSTERING
    sc_clustering = pickle_funcs.load_pickle_results(path_pickle)[0][0]
    
    #%%
    
    ## PLOTTING CLUSTER
    fig, ax = plot_grid_clusters(grid_xyz = grid,
                                sc_clustering = sc_clustering,
                                scatter_style = {'s'         : 10,
                                                'linewidth' : 0.5,
                                                },
                                                 )
    
    #%%
    ## DEFINING FIG NAME PREFIX
    fig_name_prefix="cluster"
    ## STORING FIGURE
    plot_funcs.store_figure( fig = fig,
                             path = os.path.join(store_fig_loc,
                                                 fig_name_prefix + "_%d"%(sc_clustering.n_clusters)),
                             save_fig = save_fig,
                             bbox_inches = 'tight',
                             )
    
    
    
    
    
    