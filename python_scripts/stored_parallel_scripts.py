# -*- coding: utf-8 -*-
"""
stored_parallel_scripts.py
This script parallels all code
Created on Fri Oct 25 13:07:14 2019

@author: akchew
"""

## IMPORTING TOOLS
import import_functions as import_funcs
import os

## DEFINING GLOBAL VARIABLES
XVG_VAR_DEF = [[0, 'frame', float,],
               [1, 'num', float],
               ]

### FUNCTION TO GENERATE NUMBER OF OCCURANCES
def get_num_occurances(xvg_file_path_list, variable_definition = XVG_VAR_DEF, verbose = False):
    '''
    This script goes through each file list and finds the number of occurances
    INPUTS:
        xvg_file_path_name: [str]
            xvg file path name
        each_index: [int]
            index of each xvg file
        variable_definition: [dict]
            variable definitions
    '''
    ## GETTING INDEX LABEL
    # index_label = xvg_file_index_label[each_index]
    ## GETTING PYTHON INDEX
    # python_index = index_label - 1
    
    ## CREATING EMPTY OBJECT
    results_list = []
    
    ## LOOPING
    for xvg_file_path_name in xvg_file_path_list:
        
        ## GETTING BASENAME
        xvg_file_basename = os.path.basename(xvg_file_path_name)
        
        ## PRINTING
        if verbose is True:
            print("Working on: %s"%(xvg_file_basename) )
        
        ## IMPORTING XVG FILE
        xvg_file_output = import_funcs.read_gromacs_xvg(file_path = xvg_file_path_name,
                                                        variable_definition = variable_definition)
        ## GETTING NUMBER OF OCCURANCES
        num_occurances = xvg_file_output.output['num'].astype('int')
        
        ## STORING
        results_list.append([xvg_file_basename, num_occurances])
        
    return results_list