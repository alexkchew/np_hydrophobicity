# -*- coding: utf-8 -*-
"""
import_functions.py
This code contains all importing functions. 

FUNCTIONS:
    read_xvg:
        simply outputs xvg file
    read_gromacs_xvg: 
        reads gromacs xvg files
    extract_gro: 
        class function that extracts gro file

Written by: Alex K. Chew (10/25/2019)
"""

import numpy as np

### FUNCTION TO READ THE XVG FILE
def read_xvg(file_path, verbose = False):
    '''
    The purpose of this function is to read the file and eliminate all comments
    INPUTS:
        file_path: [str] full file path to xvg file
    OUTPUTS:
        self.data_full: [list] full list of the original data
        self.data_extract: [list] extracted data in a form of a list (i.e. no comments)
    '''
    ## PRINTING
    if verbose is True:
        print("READING FILE FROM: %s"%(file_path))
    ## OPENING FILE AND READING LINES
    with open(file_path, 'r') as file:
        data_full= file.readlines()
        
    ## EXTRACTION OF DATA WITH NO COMMENTS (E.G. '@')
    final_index =[i for i, j in enumerate(data_full) if '@' in j][-1]
    data_extract = [ x.split() for x in data_full[final_index+1:] ]
    return data_full, data_extract


########################################################
### DEFINING GENERALIZED XVG READER FOR GMX COMMANDS ###
########################################################
class read_gromacs_xvg:
    '''
    The purpose of this class is to read xvg files in a generalized fashion. Here, you will input the xvg file, 
    define the bounds of the xvg such that the columns are defined. By defining the columns, we will read the xvg file, 
    then extract the information. 
    INPUTS:
        file_path: [str]
            path to xvg file
        variable_definition: [list]
            Here, you will define the variables such that you define the column, name, and type of variable.
            Note: the name of the variable will be used as a dictionary.
    OUTPUTS:
        ## INPUT INFORMATION
            self.variable_definition: [list]
                same as input -- definition of variables
        ## FILE PATHS
            self.file_path: [str]
                full path to the xvg file
        ## FILE INFORMATION
            self.data_full: [list]
                data with full information
            self.data_extract: [list]
                extracted data (no comments)
        ## VARIABLE EXTRACTION
            self.output: [dict]
                output data from defining the variables in a form of a dictionary
    FUNCTIONS:
        define_variables: this function extracts variable details
            
    '''
    ## INITIALIZING
    def __init__(self, file_path, variable_definition, verbose = False):
        
        ## STORING INPUTS
        self.variable_definition = variable_definition
        
        ## DEFINING FULL PATH
        self.file_path = file_path
        
        ## STORING VERBOSE
        self.verbose = verbose
        
        ## READING THE FILE
        self.data_full, self.data_extract = read_xvg(self.file_path, verbose = self.verbose)

        ## VARIABLE EXTRACTION
        self.define_variables()
    
    ## EXTRACTION OF VARIABLES
    def define_variables(self,):
        '''
        The purpose of this function is to extract variables from column data
        INPUTS:
            self: [object]
                class property
        OUTPUTS:
            self.output: [dict]
                output data from defining the variables in a form of a dictionary
        '''
        ## DEFINING EMPTY DICTIONARY
        self.output={}
        ## LOOPING THROUGH EACH CATEGORY
        for each_variable_definition in self.variable_definition:
            ## DEFINING CURRENT INPUTS
            col = each_variable_definition[0]
            name = each_variable_definition[1]
            var_type = each_variable_definition[2]
            
            ## DEFINING THE DATA
            data = [ x[col] for x in self.data_extract]
            
            ## EXTRACTING AND STORING)
            
            self.output[name] = np.array(data).astype(var_type)
        return
        
    
########################################
### CLASS DEFINITION: READS GRO FILE ###
########################################
class extract_gro:
    '''
    In this class, it will take the gro file and extract all the details
    Written by: Alex K. Chew (alexkchew@gmail.com)
    INPUTS:
        gro_file_path: full path to gro file
    OUTPUTS:
        ### FROM GRO FILE
        self.ResidueNum: Integer list containing residue numbers
        self.ResidueName: String name containing residue name (SOL, for example)
        self.AtomName: String containing atom name (C1, H2, etc.)
        self.AtomName_nospaces: Same as before iwthout spaces
        self.AtomNum: Integer list containing atom numbers (1, 2, 3, ...)
        self.xCoord: Float list of x-coordinates
        self.yCoord: Float list of y-coordinates
        self.zCoord: Float list of z-coordinates
        self.Box_Dimensions: List containing box dimensions
        
        ### CALCULATED/MODIFIED RESULTS
        self.unique_resid: Unique residue IDs
        self.total_atoms: total atoms
        self.total_residues: total residues
    '''

    ### INITIALIZATION
    def __init__(self, gro_file_path):
        print("\n--- CLASS: extract_gro ---")
        print("*** EXTRACTING GRO FILE: %s ***"%(gro_file_path))
        with open(gro_file_path, "r") as outputfile: # Opens gro file and make it writable
            fileData = outputfile.readlines()
    
        # Deletion of first two rows, simply the title and atom number
        del fileData[0]
        del fileData[0]

        # We know the box length is the last line. Let's extract that first.
        currentBoxDimensions = fileData[-1][:-1].split() # Split by spaces
        self.Box_Dimensions = [ float(x) for x in currentBoxDimensions ] # As floats
    
        # Deleting last line for simplicity
        del fileData[-1]
        
        # Since we know the format of the GRO file to be:
        '''
            5 Character: Residue Number (Integer)
            5 Character: Residue Name (String)
            5 Character: Atom name (String)
            5 Character: Atom number (Integer)
            8 Character: X-coordinate (Float, 3 decimal places)
            8 Character: Y-coordinate (Float, 3 decimal places)
            8 Character: Z-coordinate (Float, 3 decimal places)
        '''
        # We can extract the data for each line according to that format
        self.ResidueNum = []
        self.ResidueName = []
        self.AtomName = []
        self.AtomNum = []
        self.xCoord = []
        self.yCoord = []
        self.zCoord = []
        
        # Using for-loop to input into that specific format
        for currentLine in fileData:
            self.ResidueNum.append( int(currentLine[0:5]) )
            self.ResidueName.append( str(currentLine[5:10]) )
            self.AtomName.append( str(currentLine[10:15]) )
            self.AtomNum.append( int(currentLine[15:20]) )
            self.xCoord.append( float(currentLine[20:28]) )
            self.yCoord.append( float(currentLine[28:36]) )
            self.zCoord.append( float(currentLine[36:44]) )
        
        # CALCULATING TOTAL ATOMS
        self.total_atoms = len(self.AtomNum)
        
        # FINDING UNIQUE RESIDUE ID's
        self.unique_resid = list(set(self.ResidueNum))
        # CALCULATING TOTAL RESIDUES
        self.total_residues = len(self.unique_resid)
        
        
        ## REMOVING SPACES
        self.AtomName = [ atom.replace(" ", "") for atom in self.AtomName]
        self.ResidueName = [ res.replace(" ", "") for res in self.ResidueName]
        
        ### PRINTING SUMMARY
        print("TOTAL ATOMS: %d"%(self.total_atoms) )
        print("TOTAL RESIDUES: %d"%(self.total_residues))
        print("BOX DIMENSIONS: %s"%( [format(x, ".3f") for x in self.Box_Dimensions] ))