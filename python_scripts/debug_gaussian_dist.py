# -*- coding: utf-8 -*-
"""
debug_gaussian_dist.py
The purpose of this script is to debug the Gaussian Distribution of each 
the grid points


Written by: Alex K. Chew (12/03/2019)
"""
## IMPORTING MODULES
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
## IMPORTING GLOB
import glob

## GLOBAL VARIABLES
from global_vars import MAX_N, MU_MAX, MU_MIN
## IMPORTING PICKLE FUNCTIONS
from pickle_functions import pickle_results, load_pickle_results

## PLOTTING FUNCTIONS
import plot_functions as plot_funcs
## LOADING CORE FUNCTIONS
from core_functions import get_x_y_from_p_N, calc_mu, flatten_list_of_list

## CHECKING PATH
import check_tools

from plot_hydration_histogram import extract_most_likely_name

## PLOTTING HISTOGRAM
def plot_histogram_for_prob_dist( unnorm_p_N, 
                                  grid_index=0,
                                  line_dict = {},
                                  normalize = True,
                                  fig = None,
                                  ax = None,
                                  ):
    '''
    The purpose of this function is to plot the probability distribution for a
    given grid point. 
    INPUTS:
        unnorm_p_N: [np.array, shape=(num_grid, num_occurances)]
            unnormalized number distribution
        grid_index: [int]
            grid index that you are interested in
        normalize: [logical, default=True]
            True if you want to normalize by dividing the total sum
        fig: [default=None]
            Figure object. If available, we will just add to the figure
        ax: [default=None]
            Axis object. If available, we will just add to the figure
    OUTPUTS:
        fig, ax: 
            figure and axis
    '''
    ## DEFINING DISTRIBUTION
    dist = unnorm_p_N[grid_index]
    ## NORMALIZING
    if normalize is True:
        dist = dist / dist.sum()
    
    ## DEFINING DISTRIBUTION
    xs = np.arange(len(dist))
    
    ## CREATING PLOT
    if fig is None or ax is None:
        ## CREATING PLOT
        fig, ax = plot_funcs.create_plot()
        ## ADDING LABELS
        ax.set_xlabel("Number within grid point")
        ax.set_ylabel("Probability density function")
    
    ## CREATING HISTOGRAM PLOT
    ax.plot(xs, dist, **line_dict)
    return fig, ax

### FUNCTION TO PLOT ALL GRID POINTS
def plot_all_histogram_prob_dist( unnorm_p_N,
                                  line_dict = {
                                        "linestyle": '-',
                                        "linewidth": 2,
                                        }
                                  ):
    '''
    The purpose of this function is to plot all histograms into a single plot
    INPUTS:
        unnorm_p_N: [np.array, shape=(num_grid, num_occurances)]
            unnormalized number distribution
        line_dict: [dict]
            line dictionary
    OUTPUTS:
        fig, ax: 
            figure and axis
    '''
    ## DEFINING GRID POINTS
    grid_index_list = np.arange(0, len(unnorm_p_N))
    
    ## GETTING CMAP
    cmap = plot_funcs.get_cmap(  len(grid_index_list) )
    
    ## SET FIG AX TO NONE
    fig, ax = None, None
    ## LOOPING
    for idx, each_grid in enumerate(grid_index_list):
        ## PRINTING
        print("Plotting for grid: %d of %d"%(idx, len(unnorm_p_N) ) )
        ## DEFINING COLOR
        color = cmap(idx)
        ## ADDING TO COLOR
        line_dict['color'] = color
        
        ## PLOTTING FOR GRID
        fig, ax = plot_histogram_for_prob_dist(   unnorm_p_N = unnorm_p_N, 
                                                  grid_index=each_grid,
                                                  normalize = True,
                                                  fig = fig,
                                                  ax = ax,
                                                  line_dict=line_dict,
                                                  )
    return fig, ax

### FUNCTION TO GET DISTRIBUTION FOR A SINGLE INDEX
def plot_gaussian_fit_to_find_mu(unnorm_p_N,
                                 grid_index=0,
                                 d = MAX_N):
    '''
    INPUTS:
        unnorm_p_N: [np.array, shape=(num_grid, num_occurances)]
            unnormalized number distribution
        grid_index: [int]
            grid index that you are interested in
        d: [int]
            maximum
    OUTPUTS:
        fig, ax:
            figure and axis objects
    '''
    ## IMPORTING FUNCTION
    from matplotlib.offsetbox import AnchoredText

    ## DEFINING PROBABILITY
    p_N = unnorm_p_N[grid_index]
    
    ## GETTING VALUES
    x, y, p, p_func = get_x_y_from_p_N(p_N = p_N,
                                       d = d)
    
    ## DEFINING MU VALUE
    mu = p_func(0)
    
    ## CREATING PLOT
    fig, ax = plot_funcs.create_plot()
    ## ADDING LABELS
    ax.set_xlabel("Non-negative occurances")
    ax.set_ylabel("-log(P(N))")
    
    ## CREATING HISTOGRAM PLOT
    ax.plot(x, y, '.-', color="k")
    
    ## GETTING X IMITS
    x_limits = ax.get_xlim()
    
    ## DEFINING LINESPACE
    x_lin = np.linspace(0,x_limits[1],100)
    ax.plot(x_lin, p_func(x_lin), '--', color="blue")
    
    ## ADDING TEXT
    ax.text(x_lin[0], p_func(x_lin[0]), "%.2f"%(mu), 
            horizontalalignment='center',
            verticalalignment='bottom') 
    
    ## GETTING SHAPIRO
    stats, p = get_shapiro( data = y )
    
    ## CREATING BOX TEXT
    box_text = "%s: %.2f"%( "Shapiro-p", p,
                            ) 
    ## ADDING TEXT BOX
    text_box = AnchoredText(box_text, frameon=True, loc=4, pad=0.5)
    plt.setp(text_box.patch, facecolor='white', alpha=0.5)
    ax.add_artist(text_box)
    
    return fig, ax

### FUNCTION TO GET SHAPIRO
def get_shapiro(data, alpha=0.05, verbose = False):
    '''
    The purpose of this function is to get the shapiro value. p values that 
    are greater than 0.05 are approximately gaussian. Otherwise, it does not 
    look Gaussian.
    
    INPUTS:
        data: [np.array]
            data that you want to test if Gaussian
        alpha: [float]
            confidence value you want to test
        verbose: [logical]
            True if you want verbose
    OUTPUTS:
        stat, p: outputs from shapiro
    '''

    # REF: https://machinelearningmastery.com/a-gentle-introduction-to-normality-tests-in-python/
    # REF2: https://en.wikipedia.org/wiki/Shapiro%E2%80%93Wilk_test
    ## TESTING THE SHAPIRO-WILK TEST
    from scipy.stats import shapiro
    
    ## GENERATING TEST
    stat, p = shapiro(data)
    
    ## INTERPRETING
    if verbose is True:
        ## DEFINING STATISTIC
        print('Statistics=%.3f, p=%.3f' % (stat, p))
        if p > alpha:
        	print('Sample looks Gaussian (fail to reject H0)')
        else:
        	print('Sample does not look Gaussian (reject H0)')
    return stat, p

## FUNCTION TO GET SHAPIRO P ARRAY FROM PROBABILITY DISTRIBUTION
def compute_shapiro_p_for_p_N( unnorm_p_N, d = MAX_N ):
    '''
    The purpose of this function is to compute the shapiro p values across 
    probability distribution functions.
    INPUTS:
        unnorm_p_N: [np.array, shape=(num_grid, num_occurances)]
            unnormalized probability distribution function
        d: [int]
            max number of occurances
    OUTPUTS:
        shapiro_p_array: [np.array, shape=(num_grid)]
    '''

    ## GENERATING ARRAY
    shapiro_p_array=np.zeros(len(unnorm_p_N))
    
    ## FINDING ALL P VALUES
    for idx, p_N in enumerate(unnorm_p_N):
        ## COMPUTING P VALUES
        x, y, p, p_func = get_x_y_from_p_N(p_N = p_N,
                                           d = d)
        ## GETTING SHAPIRO P
        stats, p = get_shapiro( data = y )
        
        ## STORING
        shapiro_p_array[idx] = p
    return shapiro_p_array

### FUNCTION TO PLOT
def plot_scatter_shapiro_p_vs_grid_index(shapiro_p_array):
    '''
    The purpose of this function is to plot the shapiro versus grid index
    INPUTS:
        shapiro_p_array: [np.array]
            shapiro p values
    OUTPUTS:
        fig, ax: figure and axis for plot
    '''    
    ## CREATING PLOT
    fig, ax = plot_funcs.create_plot()
    
    ## DEFINING GRID INDEXES
    grid_index = np.arange(0, len(shapiro_p_array))
    
    ## ADDING LABELS
    ax.set_xlabel("Grid index")
    ax.set_ylabel("Shapiro P")
    
    ## CREATING HISTOGRAM PLOT
    ax.scatter(grid_index, shapiro_p_array, color="k")
    
    ## ADDING Y = 0.05 LINE
    ax.axhline(y=0.05, linestyle = '--', color='black')
    
    ## SETTING Y LIMITS
    ax.set_ylim([0,1])
    return fig, ax

### FUNCTION TO PLOT MU VALUES
def plot_scatter_mu_values(unnorm_p_N,
                           d = MAX_N):
    '''
    The purpose of this function is to plot the scatter 
    mu values across grid.
    INPUTS:
        unnorm_p_N: [np.array, shape=(num_grid, num_occurances)]
            unnormalized probability distribution function
        d: [int]
            max number of occurances
    OUTPUTS:
        fig, ax
    '''

    ## COMPUTING ALL MU VALUES
    mu_values = calc_mu(p_N_matrix = unnorm_p_N, d = d)
    
    ## CREATING PLOT
    fig, ax = plot_funcs.create_plot()
    
    ## DEFINING GRID INDEXES
    grid_index = np.arange(0, len(mu_values))
    
    ## ADDING LABELS
    ax.set_xlabel("Grid index")
    ax.set_ylabel("Mu")
    
    ## CREATING HISTOGRAM PLOT
    ax.scatter(grid_index, mu_values, color="k")
    
    return fig, ax

#%% MAIN FUNCTION
if __name__ == "__main__":
        
    ## STORING FIGURE DETAILS
    store_fig_loc = r"C:\Users\akchew\Box Sync\VanLehnGroup\2.Research Documents\Alex_RVL_Meetings\20191216\images\debug_hydrophobicity_diff_grps"
    # r"C:\Users\akchew\Box Sync\VanLehnGroup\2.Research Documents\Alex_RVL_Meetings\20191202\Images\debug_gaussian"
    # r"C:\Users\akchew\Box Sync\VanLehnGroup\2.Research Documents\Research_Presentations\191204-Research_presentation\images"
    save_fig = True
    
    ## DEFINING MAIN DIRECTORY
    MAIN_SIM_FOLDER=r"S:\np_hydrophobicity_project\simulations"
    
    ## CHECKING PATH
    MAIN_SIM_FOLDER = check_tools.check_path(MAIN_SIM_FOLDER) # check_tools
    
    ## DEIFNING DIRECTORY
    directory=r"NP_SPHERICAL"

    ## DEFINING DIRECTORY LOCATIONS
    dir_locations = [ "NP_SPHERICAL", 
                      r"191010-Most_likely_np_sims_NVT_charged_groups",
                      r"190920-Most_likely_np_sims_NVT_mixed_solvent_ROT_ligs",]
    
    dir_locations = ["191205-Planar_SAMs_frozen_trial_2_NH2",]
    # dir_locations = ["191206-Planar_SAMs_frozen_trial_3_translated",]
    dir_locations = [r"191208-Planar_SAMs_trans_unfr",]
    dir_locations = []
    dir_locations = [r"NP_SPHERICAL",
                     r"191210-annealing_try4",
                     r"191214-annealing_try4_ch",
                     r"191010-Most_likely_np_sims_NVT_charged_groups",]
    # [r"191209-anneal"]
    
    ## DEFINING DESIRED DIAM
    diam =[2, 10]
    # [ 2 ]

    ## DEFINING LIGANDS
    desired_ligands=["C11NH3", "C11COO"]
    # "dodecanethiol", "C11COOH", "C11NH2", "C11CONH2", "C11OH"
    # "C11COOH"
    # "ROT001", "ROT002", "ROT003", "ROT004", "ROT005", "ROT006", "ROT007", "ROT008", "ROT009"
    # "C11NH2", "C11COOH", "C11NH3", "C11COO"
    
    ## LOOKING FOR SPECIFIC FOLDERS
    wd_folder_list = flatten_list_of_list([ glob.glob( os.path.join( MAIN_SIM_FOLDER , each_dir) + '/*' ) 
                                             for each_dir in dir_locations ])
    ## GETTING EXTRACTED DIRECTORIES
    extracted_directories = [ extract_most_likely_name(os.path.basename(each_folder)) for each_folder in wd_folder_list]

    
    ## GETTING ALL FOLDER NAMES
    ligand_names = [ each_folder['ligand'] for each_folder in extracted_directories]
    diam_list = [ each_folder['diam'] for each_folder in extracted_directories]
    
    ## GETTING LIGANDS (ORDER)
    ligand_names_desired = [each_ligand for each_ligand in desired_ligands if each_ligand in ligand_names]
    
    ## GETTING FILES ONLY IF
    wd_folders = []
    for idx, lig_name in enumerate(ligand_names_desired):
        ## GETTING THE LIGAND INDEX
        lig_index = [ idx for idx, each_lig in enumerate(ligand_names) if each_lig == lig_name]
        diam_index = [ idx for idx, each_diam in enumerate(diam_list) if each_diam in diam]
        ## GETTING INDEX
        wd_folder_index = np.intersect1d(lig_index, diam_index) # [0]
        ## APPENDING
        for each_index in wd_folder_index:
            wd_folders.append(wd_folder_list[each_index])
    
    ## DEFINING PICKLES
    pickle_name_mu = "mu.pickle" # For MU VALUES
    pickle_name_num_occ="num_occurances.pickle"
    
    ## LOOPING THROUGH FOLDERS
    for wd in wd_folders:
    
        ## GETTING SPECIFIC FOLDER
        specific_folder = os.path.basename(wd)
        
        ## EXTRACTING ANME
        extracted_name = extract_most_likely_name(specific_folder)
        
        ## DEFINING WORKSPACE
        workspace = "hyd_analysis"
        # if specific_folder.split('_')[0] != "FrozenPlanar" and specific_folder.split('_')[0] != "FrozenGoldPlanar":
        #     workspace = "hyd_analysis"
        # else:
        #     workspace = "hyd_analysis_translated"
        
        ## DEFINING PATH
        path_mu_pickle = os.path.join(wd, workspace, pickle_name_mu)
        path_num_occ = os.path.join(wd, workspace, pickle_name_num_occ)
        
        ## DEFINING PATH TO NUM OCCURANCES
        ## LOADING PICKLE RESULTS
        unnorm_p_N = load_pickle_results(path_mu_pickle)[0][0]
        ## OUTPUTS NUM_GRID X NUM OCCURANCES
        
        ## DEFINING PATH
        # num_occurences = load_pickle_results(path_num_occ)[0][0]
        
        ## DEFINING GRID POINT
        grid_index = 0
        
        ## DEFINING STYLES
        line_dict={
                "linestyle": '-',
                "linewidth": 2,
                "alpha": 1,
                "color" :'black',
                }
        '''
        ## PLOTTING ALL HISTOGRAM DISTRIBUTIONS
        fig, ax = plot_all_histogram_prob_dist(unnorm_p_N = unnorm_p_N,
                                               line_dict = line_dict,
                                               )
        '''

        ## DEFINING FIGURE NAME
        fig_name_prefix = specific_folder
        fig_name_prefix =  os.path.basename(os.path.dirname(wd)) + '-' + extracted_name['ligand']

        #####################################
        ## PLOTTING FIGURE AND AXIS
        fig, ax = plot_histogram_for_prob_dist(   unnorm_p_N = unnorm_p_N, 
                                                  grid_index= 0,
                                                  normalize = True,
                                                  line_dict = line_dict,
                                                  )
        
        
        ## STORING FIGURE
        plot_funcs.store_figure( fig = fig,
                                 path = os.path.join(store_fig_loc,
                                                     fig_name_prefix + "_histogram_grid_0"),
                                 save_fig = save_fig
                                 )
        
        #####################################
        ## PLOTTING FUNCTIONS
        fig, ax = plot_gaussian_fit_to_find_mu(unnorm_p_N = unnorm_p_N,
                                               grid_index=0,
                                               d = MAX_N)
        
        ## STORING FIGURE
        plot_funcs.store_figure( fig = fig,
                                 path = os.path.join(store_fig_loc,
                                                     fig_name_prefix + "_fitted_grid_0"),
                                 save_fig = save_fig
                                 )
        
        
        
        #####################################
        ## GETTING SHAPIRO ARRAY
        shapiro_p_array = compute_shapiro_p_for_p_N(unnorm_p_N)
        
        ## PLOTTING SCATTER
        fig, ax = plot_scatter_shapiro_p_vs_grid_index(shapiro_p_array = shapiro_p_array)
        
        ## STORING FIGURE
        plot_funcs.store_figure( fig = fig,
                                 path = os.path.join(store_fig_loc,
                                                     fig_name_prefix + "_shapiro"),
                                 save_fig = save_fig
                                 )
        
        #####################################
        ## PLOTTING MU VALUES
        fig, ax = plot_scatter_mu_values(unnorm_p_N = unnorm_p_N,
                                         d = MAX_N)
        
        ## STORING FIGURE
        plot_funcs.store_figure( fig = fig,
                                 path = os.path.join(store_fig_loc,
                                                     fig_name_prefix + "_mu_dist"),
                                 save_fig = save_fig
                                 )

    #%%
#    
#    ### FUNCTION TO DRAW HEAT MAP
#    def heatmap(data, row_labels, col_labels, ax=None,
#                cbar_kw={}, cbarlabel="", **kwargs):
#        """
#        Create a heatmap from a numpy array and two lists of labels.
#    
#        Parameters
#        ----------
#        data
#            A 2D numpy array of shape (N, M).
#        row_labels
#            A list or array of length N with the labels for the rows.
#        col_labels
#            A list or array of length M with the labels for the columns.
#        ax
#            A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
#            not provided, use current axes or create a new one.  Optional.
#        cbar_kw
#            A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
#        cbarlabel
#            The label for the colorbar.  Optional.
#        **kwargs
#            All other arguments are forwarded to `imshow`.
#        """
#    
#        if not ax:
#            ax = plt.gca()
#    
#        # Plot the heatmap
#        im = ax.imshow(data, **kwargs)
#    
#        # Create colorbar
#        cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
#        cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")
#    
##        # We want to show all ticks...
##        ax.set_xticks(np.arange(data.shape[1]))
##        ax.set_yticks(np.arange(data.shape[0]))
##        # ... and label them with the respective list entries.
##        ax.set_xticklabels(col_labels)
##         ax.set_yticklabels(row_labels)
##    
##        # Let the horizontal axes labeling appear on top.
##        ax.tick_params(top=True, bottom=False,
##                       labeltop=True, labelbottom=False)
##    
##        # Rotate the tick labels and set their alignment.
##        plt.setp(ax.get_xticklabels(), rotation=-30, ha="right",
##                 rotation_mode="anchor")
##    
##        # Turn spines off and create white grid.
##        for edge, spine in ax.spines.items():
##            spine.set_visible(False)
#    
##        ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
##        ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
##        ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
##        ax.tick_params(which="minor", bottom=False, left=False)
#    
#        return im, cbar
#    
#    import matplotlib.pyplot as plt
#    fig, ax = plt.subplots()
#    
#    ## DEFINING ROW AND COL
#    n_row, n_col =  num_occurences.shape
#    
#    ## DEFINING GRID
#    row_labels = np.arange(0, n_row, 5000)
#    col_labels = np.arange(0, n_col, 500)
#    
#    im, cbar = heatmap(num_occurences, row_labels, col_labels, ax=ax,
#                   cmap="jet", cbarlabel="Number of occurrences")
#    
#    fig.tight_layout()
#    plt.show()  
    
    # plt.imshow(num_occurences, cmap='hot', interpolation='nearest')
    # plt.show()

    #%%

    
    
    
    
    #%%

    
    ## GETTING FIT
    # y_fit = 
    
    

    