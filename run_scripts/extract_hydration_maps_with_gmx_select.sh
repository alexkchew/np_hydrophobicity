#!/bin/bash

# extract_hydration_maps_with_gmx_select.sh
# This script runs the python code to extract hydration maps. 
# Here, we will use the path of the python script to correctly run the nanoparticle analysis.

## DEFINING VARIABLES
# SCRIPTS
#   _PYTHONSCRIPT_ <-- python script
#   _BASHGRID_ <-- grid code
#   _BASHSELECT_ <-- bash select code

# FILE DETAILS
#   _GROFILE_ <-- gro path
#   _XTCFILE_ <-- xtc path
#   _TPRFILE_ <-- tpr file


## PROCESSORS
#   _NGRIDPROCS_ <-- number of procs for gridding
#   _SIMPATH_ <-- simulation path
#   _NFRAMES_ <-- number of frames in trajectory
#   _NPROCS_ <-- number of processors
#   _MESH_ <-- mesh size
#   _OUTPUTFILE_ <-- output file
#   _OUTPUTPREFIX_ <-- output prefix
#   _WANTPLANAR_ <-- True if you want planar

#   _INDEXFILE_ <-- index file
#   _REWRITE_ <-- rewrite true/false
#   _COMBINEDNAME_ <-- combined name
#   _BEGINTRAJ_ <-- begining trajectory
#   _ENDTRAJ_ <-- ending trajectory
#   _ANALYSISDIR_ <-- analysis directory
#   _ENDGRIDTRAJ_ <-- end grid trajectory
##############
### INPUTS ###
##############

## EXPORTING PYTHON PATH
export PYTHONPATH="${HOME}/bin/pythonfiles/modules:$PYTHONPATH"

## DEFINING BASH SCRIPT LOCATION
bash_grid_code="_BASHGRID_"

## BASH CODE TO GENERATE SELECT
bash_select_code="_BASHSELECT_"

## DEFINING ANALYSIS DETAILS
mesh="_MESH_"

## DEFINING FRAMES
n_procs="_NPROCS_"

## DEFINING FRAMES
begin_traj="_BEGINTRAJ_"
end_traj="_ENDTRAJ_"

## DEFINING GRID END TRAJ
end_traj_grid="_ENDGRIDTRAJ_"

## PROCS FOR GRIDDING
grid_procs="_NGRIDPROCS_"

## DEFINING IF PLANAR
planar_sam="_WANTPLANAR_"

## DEFINING FILE DETAILS
path_sims="_SIMPATH_"
gro_file="_GROFILE_"
xtc_file="_XTCFILE_"
tpr_file="_TPRFILE_"

## DEFINING INDEXFILE
index_file="_INDEXFILE_"

## DEFINING REWRITE
rewrite="_REWRITE_"

## DEFINING LIGAND-GOLD RESNAME
combined_name="_COMBINEDNAME_"

## DEFINING PYTHON SCRIPT
python_script_compute_mu="_PYTHONSCRIPT_"

## DEFINING OUTPUT FILE
output_file="_OUTPUTFILE_"

## DEFINING OUTPUT PREFIX
output_prefix="_OUTPUTPREFIX_"

## DEFINING ANLAYSIS DIRECTORY
analysis_dir="_ANALYSISDIR_"

## PICKLE NAMES
mu_file_name="_MUPICKLE_"
mu_file_output="_MUOUTPUT_"
num_occurances_file_name="_NUMOCCPICKLE_"
pdb_file_name="${output_prefix}_hydration.pdb"

## DEFINING INPUT PREFIX
input_prefix="${gro_file%.gro}"

########################
### STEP 1: GRIDDING ###
########################
bash "${bash_grid_code}" "${path_sims}" \
                         "${mesh}" \
                         "${output_prefix}" \
                         "${tpr_file}" \
                         "${xtc_file}" \
                         "${grid_procs}" \
                         "${analysis_dir}" \
                         "${rewrite}" \
                         "${input_prefix}" \
                         "${end_traj_grid}" \
                         

## DEFINING GRID OUTPUT
grid_output="grid-0_${end_traj_grid}" # -${mesh}
wc_output="${output_prefix}_willard_chandler.dat"


##########################################
### STEP 2: GMX TO CREATE TRAJECTORIES ###
##########################################

## GO TO PATH SIM
cd "${path_sims}"

## GETTING !AUNP NAME
combined_name_not="!${combined_name}_&_!SOL_H"

## DEFINING OUTPUT PREFIX FOR TRAJECTORY
output_traj_prefix="${input_prefix}_${begin_traj}_${end_traj}-noNP"

## DEFINING OUTPUT
output_xtc_file="${output_traj_prefix}.xtc"
output_tpr_file="${output_traj_prefix}.tpr"
output_gro_file="${output_traj_prefix}.gro"

## USING TRJCONV TO GET TRUNCATED SYSTEM
if [[ ! -e "${output_xtc_file}" ]] || [[ "${rewrite}" == true ]]; then
## XTC FILE
gmx trjconv -f "${xtc_file}" -s "${tpr_file}" -o "${output_xtc_file}" -pbc mol -b "${begin_traj}" -e "${end_traj}" -n "${index_file}" << INPUTS
${combined_name_not}
INPUTS
fi

if [[ ! -e "${output_tpr_file}" ]] || [[ "${rewrite}" == true ]]; then
## TPR FILE
gmx convert-tpr -s "${tpr_file}" -o "${output_tpr_file}" -n "${index_file}" << INPUTS
${combined_name_not}
INPUTS
fi

if [[ ! -e "${output_gro_file}" ]] || [[ "${rewrite}" == true ]]; then
## DUMPING GRO FILE
gmx trjconv -f "${output_xtc_file}" -s "${output_tpr_file}" -o "${output_gro_file}" -dump "${begin_traj}" << INPUTS
System
INPUTS
fi

###########################
#### STEP 3: GMX SELECT ###
###########################
bash "${bash_select_code}" "${path_sims}" \
                           "${analysis_dir}" \
                           "${grid_output}" \
                           "${wc_output}" \
                           "${output_traj_prefix}" \
                           "${n_procs}" \
                           "${begin_traj}" \
                           "${end_traj}" 

########################################
### STEP 4: CONGLOMERATE ALL DETAILS ###
########################################

## DEFINING INPUTS
path_gro="${path_sims}/${gro_file}"
path_select_output="${path_sims}/${analysis_dir}/gmx_select_output/output"
path_grid="${path_sims}/${analysis_dir}/${grid_output}/${wc_output}"

## PATH PICKLES
path_mu_pickle="${path_sims}/${analysis_dir}/${mu_file_name}"
path_mu_output="${path_sims}/${analysis_dir}/${mu_file_output}"
path_num_occurances_pickle="${path_sims}/${analysis_dir}/${num_occurances_file_name}"
path_pdb_file="${path_sims}/${analysis_dir}/${pdb_file_name}"

# RUNNING PYTHON SCRIPT
python3.6 "${python_script_compute_mu}" --path_gro "${path_gro}" \
                                        --path_select_output "${path_select_output}" \
                                        --path_mu_pickle "${path_mu_pickle}" \
                                        --path_mu_output "${path_mu_output}" \
                                        --path_pdb_file "${path_pdb_file}" \
                                        --path_num_occurances_pickle "${path_num_occurances_pickle}" \
                                        --n_procs "${n_procs}" \
                                        --path_grid "${path_grid}" \
                                        --rewrite "${rewrite}"
