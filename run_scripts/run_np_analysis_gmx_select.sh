#!/bin/bash

# run_np_analysis_gmx_select.sh
# The purpose of this script is to run the nanoparticle analysis.
# This also runs the gmx select algorithm!
# GMX select in conjunction with Brad's code is hypothetically faster!
# 
# Written by: Alex K. Chew & Brad C. Dallin (10/14/2019)
## 
# UPDATES:
#   191023-Updating code to exclude water hydrogens

# MAKE INDEX FILE
# r SOL & ! a H*
# OUTPUT: SOL_&_!H*
# resname SOL CL NA and not name \"H.*\
# r SOL & a O*
#   r SOL & a H*
#   !6 & !2
################################
### LOADING GLOBAL VARIABLES ###
################################
dir_loc="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "${dir_loc}/bin/bashrc.sh"

#########################
### INITIAL VARIABLES ###
#########################

## DEFINING FORCEFIELD
forcefield="${FORCEFIELD}"

## DEFINING LOCATION
sim_folder_name="$1"
# "NP_SPHERICAL"
# "191010-Most_likely_np_sims_NVT_charged_groups"

## DEFINING SPECIFIC FILE
sim_file_name="$2"
# "MostlikelynpNVT_EAM_300.00_K_2_nmDIAM_C11OH_CHARMM36jul2017_Trial_1_likelyindex_1"
# "MostlikelynpNVT_EAM_300.00_K_2_nmDIAM_C11NH2_CHARMM36jul2017_Trial_1_likelyindex_1"

## FINDING LIGAND NAME
lig_name=$(extract_lig_name_from_dir "${sim_file_name}")

## DEFINING TYPE
initial_text=$(cut -d'_' -f"1" <<< "${sim_file_name}")

## GETTING TEXT
if [[ "${initial_text}" == "FrozenPlanar"* ]] || [[ "${initial_text}" == "FrozenGoldPlanar"* ]]; then
    ## DEFINING TYPE
    job_type="planar"
    ## DEFINING PLANAR SAM
    planar_sam="True"
else
    job_type="spherical"
    planar_sam="False"
fi

# "MostlikelynpNVT_EAM_300.00_K_2_nmDIAM_C11COOH_CHARMM36jul2017_Trial_1_likelyindex_1"
# "$1"
# "$2"
# "MostlikelynpNVT_EAM_300.00_K_2_nmDIAM_C11NH3_CHARMM36jul2017_Trial_1_likelyindex_1"
# "MostlikelynpNVT_EAM_300.00_K_2_nmDIAM_C11COO_CHARMM36jul2017_Trial_1_likelyindex_1"

##############################
### USER DEFINED VARIABLES ###
##############################

## DEFINING BEGINNING AND END TRAJ
begin_traj="0"   # 0 ns
end_traj="50000" # 50 ns

## DEFINING PROCS FOR GMX SELECT
n_procs="28"
# "10"
# "28"

## DEFINING DETAILS 
grid_n_procs="20"
# "5"
# "20"

## OUTPUT PREFIX FOR WILLARD CHANDLER
output_prefix="out"

## DEFINING GRID SIZE
mesh="0.1,0.1,0.1"
# "0.1"

## DEFINING REWRITE OPTIONS
rewrite=true
# false if no rewriting

## DEFINING DESIRED TRANSLATION
want_translate=false
# true

## DEFINING ANALYSIS SCRIPT
# output_file="hyd_analysis"
output_file="hyd_analysis" # Translated hyd analysis
# hyd_analysis_translated

#########################
### DEFAULT VARAIBLES ###
#########################
## PYTHON SCRIPT
python_script="${PYTHON_SCRIPTS}/np_hydration_map_analysis.py"
# "${PYTHON_SCRIPTS_NP_ANALYSIS}"

## GETTING END TRAJ GRID
end_traj_grid="1000"

## GRO AND XTC
input_prefix="sam_prod"
input_gro_file="${input_prefix}.gro"
input_tpr_file="${input_prefix}.tpr"
input_xtc_file="${input_prefix}.xtc"

## DEFINING LIGAND ITP FILE
ligand_itp_file="${lig_name}.itp"

## DEFINING INDEX FILE
index_file="gold_ligand.ndx"

## BASH SCRIPT
bash_hydration_map_code="${BASH_HYDRATION_CODE_GMX_SELECT}"
## DEFINING GRID CODE
bash_grid_code="${PATH2SCRIPTS}/generate_grid.sh"
bash_select_code="${PATH2SCRIPTS}/run_gmx_select.sh"

## INPUT SUBMISSION SCRIPT
input_submission_script="${PATH_SUBMISSIONS}/submit_hydration_maps.sh"
output_submission_script="submit.sh"

## DEFINING SLURM 
slurm_output="hydration.out"

## DEFINING PICKLE
mu_file_name="mu.pickle"
mu_file_output="mu.dat"
num_occurances_file_name="num_occurances.pickle"

######################
### DEFINING PATHS ###
######################
## PATH TO SIMS
path_sims="${PATH_SIMULATIONS}/${sim_folder_name}/${sim_file_name}"

## CHECKING IF EXISTS
stop_if_does_not_exist "${path_sims}"

## GOING INTO DIRECTORY
cd "${path_sims}"

## CREATING DIRECTORY
if [ ! -e "${output_file}" ]; then
    mkdir -p "${output_file}"

else
    if [[ "${rewrite}" == true ]]; then
        create_dir "${output_file}" -f
    fi
fi

## DEFINING GOLD RESIDUE NAME
if [[ "${job_type}" != "planar" ]]; then
    gold_residue_name="AUNP"
    lig_residue_name=$(itp_get_resname ${ligand_itp_file})
else
    gold_residue_name="AUI"
    lig_residue_name=$(itp_get_resname ${forcefield}/${ligand_itp_file})
fi

## CHECKINGI RESIDUE NAME FOUND
if [ -z "${lig_residue_name}" ]; then
    echo "Error! Missing ligand residue name"
    echo "Check itp file: ${lig_residue_name}"
    sleep 5
    exit
fi

## CREATING GOLD AND LIGAND RESIDUE NAME
make_ndx_gold_with_ligand "${path_sims}/${input_tpr_file}" \
                          "${path_sims}/${index_file}"   \
                          "${lig_residue_name}"   \
                          "${gold_residue_name}"  
                          
## ADDING NOT COMBINED NAME
gmx make_ndx -n "${path_sims}/${index_file}" -f "${path_sims}/${input_tpr_file}" -o "${path_sims}/${index_file}" << INPUTS
r SOL & a H*
name 3 SOL_H
!2 & !3
q
INPUTS

###################################
### TRANSLATING FOR PLANAR CASE ###
###################################
if [[ "${job_type}" == "planar" ]] && [[ "${want_translate}" == true ]]; then
    ## DEFINING DISTANCE
    dist_xvg="distxyz.xvg"
    
## GMX DISTANCE TO GET Z DIMENSION
gmx distance -f "${input_xtc_file}" -s "${input_tpr_file}" -seltype res_com -select "cog of resname ${gold_residue_name} plus [0,0,0]" -b 0 -e 0 -oxyz "${dist_xvg}" >/dev/null 2>&1
    ## GETTING DISTANCE
    z_dist="$(tail -n1 ${dist_xvg} | awk '{print $NF}')"
    
    ## DEFINING NEW PREFIX
    new_input_prefix="${input_prefix}_translated"
    new_input_gro_file="${new_input_prefix}.gro"
    new_input_xtc_file="${new_input_prefix}.xtc"
    new_input_tpr_file="${new_input_prefix}.tpr"
    
    ## GMX TRJCONV
    trans_z_value=$(awk -v z_dim=${z_dist} 'BEGIN{ printf "%.3f", 0 - z_dim}')
    
    ## PRINTING
    echo "*** TRANSLATING PLANAR CASE ***"
    echo "Z-distance of gold: ${z_dist}"
    echo "Translating z-dim: ${trans_z_value}"
    
    ## TRANSLATING XTC
    if [ ! -e "${new_input_xtc_file}" ] || [[ "${rewrite}" == true ]]; then
gmx trjconv -s "${input_tpr_file}" -f "${input_xtc_file}" -o "${new_input_xtc_file}" -trans 0 0 ${trans_z_value} -pbc mol << INPUTS
System
INPUTS
    fi

    ## TRANSLATING GRO
gmx trjconv -s "${input_tpr_file}" -f "${input_gro_file}" -o "${new_input_gro_file}" -trans 0 0 ${trans_z_value} -pbc mol << INPUTS
System
INPUTS

    ## COPYING TPR
    cp "${input_tpr_file}" "${new_input_tpr_file}"
    
    ## RENAMING
    input_tpr_file="${new_input_tpr_file}"
    input_xtc_file="${new_input_xtc_file}"
    input_gro_file="${new_input_gro_file}"

fi

## REMOVING EXTRAS
find . -type f -name "\#*" -delete

## DEFINING COMBINED NAME
combined_name="${gold_residue_name}_${lig_residue_name}"

# !AUNP_NH2_&_!SOL_o

## DEFINING OUPTUT SIMS
path_sims_output="${path_sims}/${output_file}"

## GOING INTO ANALYSIS
cd "${path_sims_output}"

## COPYING OVER FILE
cp -r "${bash_hydration_map_code}" "${path_sims_output}"

## GETTING BASENAME
bash_hydration_file_name="$(basename ${bash_hydration_map_code})"

## USING SED TO EDIT DETAILS FOR BASH SCRIPT
sed -i "s/_MESH_/${mesh}/g" "${bash_hydration_file_name}"
sed -i "s/_NPROCS_/${n_procs}/g" "${bash_hydration_file_name}"
sed -i "s/_NGRIDPROCS_/${grid_n_procs}/g" "${bash_hydration_file_name}"
sed -i "s/_WANTPLANAR_/${planar_sam}/g" "${bash_hydration_file_name}"
sed -i "s#_SIMPATH_#${path_sims}#g" "${bash_hydration_file_name}"
sed -i "s/_GROFILE_/${input_gro_file}/g" "${bash_hydration_file_name}"
sed -i "s/_XTCFILE_/${input_xtc_file}/g" "${bash_hydration_file_name}"
sed -i "s/_TPRFILE_/${input_tpr_file}/g" "${bash_hydration_file_name}"
sed -i "s#_PYTHONSCRIPT_#${python_script}#g" "${bash_hydration_file_name}"
sed -i "s#_OUTPUTFILE_#${output_file}#g" "${bash_hydration_file_name}"
sed -i "s#_OUTPUTPREFIX_#${output_prefix}#g" "${bash_hydration_file_name}"
## EDITING BASH GRID
sed -i "s#_BASHGRID_#${bash_grid_code}#g" "${bash_hydration_file_name}"
sed -i "s#_ENDGRIDTRAJ_#${end_traj_grid}#g" "${bash_hydration_file_name}"

## EDITING GMX SELECT
sed -i "s#_BASHSELECT_#${bash_select_code}#g" "${bash_hydration_file_name}"

sed -i "s#_REWRITE_#${rewrite}#g" "${bash_hydration_file_name}"
sed -i "s#_INDEXFILE_#${index_file}#g" "${bash_hydration_file_name}"
sed -i "s#_COMBINEDNAME_#${combined_name}#g" "${bash_hydration_file_name}"

sed -i "s#_BEGINTRAJ_#${begin_traj}#g" "${bash_hydration_file_name}"
sed -i "s#_ENDTRAJ_#${end_traj}#g" "${bash_hydration_file_name}"

sed -i "s#_ANALYSISDIR_#${output_file}#g" "${bash_hydration_file_name}"

sed -i "s#_MUPICKLE_#${mu_file_name}#g" "${bash_hydration_file_name}"
sed -i "s#_NUMOCCPICKLE_#${num_occurances_file_name}#g" "${bash_hydration_file_name}"
sed -i "s#_MUOUTPUT_#${mu_file_output}#g" "${bash_hydration_file_name}"


####################################
### GENERATING SUBMISSION SCRIPT ###
####################################
## DEFINING JOB NAME
job_name="${sim_file_name}_${output_file}"

## DEFINING OUTPUT SUBMISSION FILE
output_submit_path="${path_sims_output}/${output_submission_script}"

## COPYING SUBMISSION FILE
cp -r "${input_submission_script}" "${output_submit_path}"

## EDITING SUBMISSION FILE
sed -i "s#_USER_#${USER}#g" "${output_submit_path}"
sed -i "s#_JOBNAME_#${job_name}#g" "${output_submit_path}"
sed -i "s#_BASHSCRIPT_#${bash_hydration_file_name}#g" "${output_submit_path}"
sed -i "s#_SLURMOUT_#${slurm_output}#g" "${output_submit_path}"

## ADDING TO JOB LIST
echo "${output_submit_path}" >> "${JOB_LIST}"

