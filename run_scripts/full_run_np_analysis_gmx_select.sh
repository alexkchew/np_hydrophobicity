#!/bin/bash

# full_run_np_analysis_gmx_select.sh
# The purpose of this script is to run full nanoparticle analysis
# This uses gmx select protocols
# Written by: Alex K. Chew & Brad C. Dallin (10/26/2019)

################################
### LOADING GLOBAL VARIABLES ###
################################
dir_loc="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "${dir_loc}/bin/bashrc.sh"

#########################
### DEFAULT VARIABLES ###
#########################

## BASH FILE
bash_file="${PATH2SCRIPTS}/run_np_analysis_gmx_select.sh"

## PREFIX
folder_prefix="MostlikelynpNVT_EAM_300.00_K_2_nmDIAM"
# folder_prefix="MostlikelynpNVT_EAM_300.00_K_6_nmDIAM"
folder_suffix="CHARMM36jul2017_Trial_1_likelyindex_1"
# folder_suffix="CHARMM36jul2017_Trial_1_likelyindex_401"
#folder_suffix="CHARMM36jul2017_Trial_1_likelyindex_200"
# folder_suffix="CHARMM36jul2017_Trial_1_likelyindex_300"

## PLANAR
# folder_prefix="FrozenPlanar_300.00_K"
# folder_prefix="FrozenGoldPlanar_300.00_K"
# folder_suffix="10x10_CHARMM36jul2017_intffGold_Trial_1-50000_ps"

############################
### DEFINING INPUT ARRAY ###
############################

## MAIN SIMULATION FOLDER
# main_sim_folder="191121-last_index_most_likely"
main_sim_folder="191103-PEG_most_likely"
# "191025-Most_likely_np_sims_6_nm"
# "191214-Planar_SAM_temp_annealing_try4_unfrozen"
# 191010-Most_likely_np_sims_NVT_charged_groups
# "191213-Planar_SAM_temp_annealing_try4_unfrozen"
# "NP_SPHERICAL"
# ------- UPDATED ONES ABOVE
# "191210-annealing_try4"
# "191208-Planar_SAM_temp_annealing_try4_unfrozen"
# "191208-Other_most_likely"
# "191208-Planar_SAM_temp_annealing"
# "191206-Planar_SAMs_frozen_trial_3_translated_unfrozen"
# "191206-Planar_SAMs_frozen_trial_3_translated"
# "191206-Planar_SAMs_frozen_trial_3_translated"
# "191205-Planar_SAMs_frozen_trial_2_NH2"
# "191123-Frozen_SAM_gold"

# main_sim_folder="191103-PEG_most_likely"
# "191025-Most_likely_np_sims_6_nm"
# main_sim_folder="190920-Most_likely_np_sims_NVT_mixed_solvent_ROT_ligs"
# "191010-Most_likely_np_sims_NVT_charged_groups"
# "NP_SPHERICAL"
# "191010-Most_likely_np_sims_NVT_charged_groups"
# "190920-Most_likely_np_sims_NVT_mixed_solvent_ROT_ligs"
# "NP_SPHERICAL"

## DEFINING LIGAND ARRAY
declare -a ligand_names=("ROTPE1" "ROTPE2" "ROTPE3" "ROTPE4") #  "C11COOH"
# "dodecanethiol" "C11OH"
# "C11COO" "C11NH3"
# "C11COOH" "C11NH2" "C11CONH2" "C11OH"
# 
#   "C11NH2"
# C11COOH
# "dodecanethiol"
# "dodecanethiol" "C11OH" "C11COOH" "C11CONH2"
# "C11NH2"
# "dodecanethiol" "C11OH" "C11NH2" "C11COOH" "C11CONH2") #  "ROT002" "ROT005" C11OH
# "dodecanethiol" "C11OH" "C11NH2" "C11COOH"
# "C11CONH2"


#  "C11COOH"
# "ROTPE1" "ROTPE2" "ROTPE3" "ROTPE4"
# "ROT003" 
# "dodecanethiol"
# "ROT002" "ROT004" "ROT007" "ROT008" "ROT009"
# "ROT002" "ROT003" "ROT004" "ROT005" "ROT006" "ROT007" "ROT008" "ROT009"
# "C11COO" "C11NH3"
# "C11NH2" "C11CONH2" "dodecanethiol"

# "C11OH"
# "C11COOH"
# "dodecanethiol"
# "C11COO" "C11NH3"
# "C11COOH" "dodecanethiol"
# "ROT001" "ROT002"
# "C11OH" "C11NH2"

## LOOPING
for each_ligand in "${ligand_names[@]}"; do
	## CREATING SIM FOLDER
	current_sim_folder="${folder_prefix}_${each_ligand}_${folder_suffix}"

	## PRINTING
	echo "Running: bash "${bash_file}" "${main_sim_folder}" "${current_sim_folder}""; sleep 2

	## RUNNING BASH SCRIPT
	bash "${bash_file}" "${main_sim_folder}" "${current_sim_folder}"

done



