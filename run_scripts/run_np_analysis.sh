#!/bin/bash

# run_np_analysis.sh
# The purpose of this script is to run the nanoparticle analysis.
# 
# Written by: Alex K. Chew & Brad C. Dallin (10/14/2019)
## 
# UPDATES:
#   191023-Updating code to exclude water hydrogens

# MAKE INDEX FILE
# r SOL & ! a H*
# OUTPUT: SOL_&_!H*

################################
### LOADING GLOBAL VARIABLES ###
################################
dir_loc="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "${dir_loc}/bin/bashrc.sh"

#########################
### INITIAL VARIABLES ###
#########################

## DEFINING LOCATION
sim_folder_name="$1"
# "191010-Most_likely_np_sims_NVT_charged_groups"

## DEFINING SPECIFIC FILE
sim_file_name="$2"
# "MostlikelynpNVT_EAM_300.00_K_2_nmDIAM_C11NH3_CHARMM36jul2017_Trial_1_likelyindex_1"
# "MostlikelynpNVT_EAM_300.00_K_2_nmDIAM_C11COO_CHARMM36jul2017_Trial_1_likelyindex_1"

##############################
### USER DEFINED VARIABLES ###
##############################

## DEFINING BEGINNING AND END TRAJ
begin_traj="0"   # 0 ns
end_traj="50000" # 50 ns

## DEFINING DETAILS 
n_procs="20"
# "5"
# "20"

## OUTPUT PREFIX
output_prefix="out"

## DEFINING GRID SIZE
mesh="0.1"

## DEFINING OUTPUT NAME
output_file="hydra-${begin_traj}_${end_traj}-${mesh}"

## DEFINING PLANAR SAM
planar_sam="False"

## DEFINING REWRITE OPTIONS
rewrite=true
# false if no rewriting

#########################
### DEFAULT VARAIBLES ###
#########################
## PYTHON SCRIPT
python_script="${PYTHON_SCRIPTS_NP_ANALYSIS}"

## GRO AND XTC
gro_file="sam_prod.gro"
input_tpr_file="sam_prod.tpr"
input_xtc_file="sam_prod.xtc"
output_traj_prefix="sam_prod-${begin_traj}_${end_traj}-watO"
output_xtc_file="${output_traj_prefix}.xtc"
output_gro_file="${output_traj_prefix}.gro"
output_tpr_file="${output_traj_prefix}.tpr"

## BASH SCRIPT
bash_hydration_map_code="${BASH_HYDRATION_CODE}"

## INPUT SUBMISSION SCRIPT
input_submission_script="${PATH_SUBMISSIONS}/submit_hydration_maps.sh"
output_submission_script="submit.sh"

## DEFINING SLURM 
slurm_output="hydration.out"

## DEFINING WATER AND NOT HYDROGEN
water_not_hydrogen="r SOL & a H*"
## DEFINING OUTPUT INDEX NAME
water_not_hydrogen_name="!SOL_&_H*"

## DEFINING INDEX FILE
index_file="no_water_hyd.ndx"

######################
### DEFINING PATHS ###
######################
## PATH TO SIMS
path_sims="${PATH_SIMULATIONS}/${sim_folder_name}/${sim_file_name}"

## CHECKING IF EXISTS
stop_if_does_not_exist "${path_sims}"

#################
### MAIN CODE ###
#################

## GOING TO PATH
cd "${path_sims}"

## MAKING INDEX
if [[ ! -e "${index_file}" ]] || [[ "${rewrite}" == true ]]; then
gmx make_ndx -f "${input_tpr_file}" -o "${index_file}" << INPUTS
keep 0
keep 1
${water_not_hydrogen}
!0
q
INPUTS

fi

## USING TRJCONV TO GET TRUNCATED SYSTEM
if [[ ! -e "${output_xtc_file}" ]] || [[ "${rewrite}" == true ]]; then
## XTC FILE
gmx trjconv -f "${input_xtc_file}" -s "${input_tpr_file}" -o "${output_xtc_file}" -pbc mol -b "${begin_traj}" -e "${end_traj}" -n "${index_file}" << INPUTS
${water_not_hydrogen_name}
INPUTS

## TPR FILE
gmx convert-tpr -s "${input_tpr_file}" -o "${output_tpr_file}" -n "${index_file}" << INPUTS
${water_not_hydrogen_name}
INPUTS

## DUMPING GRO FILE
gmx trjconv -f "${output_xtc_file}" -s "${output_tpr_file}" -o "${output_gro_file}" -dump "${begin_traj}" << INPUTS
System
INPUTS

fi
# System

## CREATING DIRECTORY
if [ ! -e "${output_file}" ]; then
    if [[ "${rewrite}" == true ]]; then
        create_dir "${output_file}" -f
    else
        mkdir -p "${output_file}"
    fi
fi

## DEFINING OUPTUT SIMS
path_sims_output="${path_sims}/${output_file}"

## REMOVING ANY EXTRA 
rm \#*

## GOING INTO DIRECTORY
cd "${path_sims_output}"

## COPYING OVER FILE
cp -r "${bash_hydration_map_code}" "${path_sims_output}"

## GETTING BASENAME
bash_hydration_file_name="$(basename ${bash_hydration_map_code})"

## USING SED TO EDIT DETAILS FOR BASH SCRIPT
sed -i "s/_MESH_/${mesh}/g" "${bash_hydration_file_name}"
sed -i "s/_NFRAMES_/${end_traj}/g" "${bash_hydration_file_name}"
sed -i "s/_NPROCS_/${n_procs}/g" "${bash_hydration_file_name}"
sed -i "s/_WANTPLANAR_/${planar_sam}/g" "${bash_hydration_file_name}"
sed -i "s#_SIMPATH_#${path_sims}#g" "${bash_hydration_file_name}"
sed -i "s/_GROFILE_/${output_gro_file}/g" "${bash_hydration_file_name}"
sed -i "s/_XTCFILE_/${output_xtc_file}/g" "${bash_hydration_file_name}"
sed -i "s#_PYTHONSCRIPT_#${python_script}#g" "${bash_hydration_file_name}"
sed -i "s#_OUTPUTFILE_#${output_file}#g" "${bash_hydration_file_name}"
sed -i "s#_OUTPUTPREFIX_#${output_prefix}#g" "${bash_hydration_file_name}"

####################################
### GENERATING SUBMISSION SCRIPT ###
####################################
## DEFINING JOB NAME
job_name="${sim_file_name}_${output_file}"

## DEFINING OUTPUT SUBMISSION FILE
output_submit_path="${path_sims_output}/${output_submission_script}"

## COPYING SUBMISSION FILE
cp -r "${input_submission_script}" "${output_submit_path}"

## EDITING SUBMISSION FILE
sed -i "s#_USER_#${USER}#g" "${output_submit_path}"
sed -i "s#_JOBNAME_#${job_name}#g" "${output_submit_path}"
sed -i "s#_BASHSCRIPT_#${bash_hydration_file_name}#g" "${output_submit_path}"
sed -i "s#_SLURMOUT_#${slurm_output}#g" "${output_submit_path}"

## ADDING TO JOB LIST
echo "${output_submit_path}" >> "${JOB_LIST}"


