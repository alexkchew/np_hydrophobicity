#!/bin/bash

# run_gmx_select.sh
# The purpose of this script is to run gmx select across many willard chandler surfaces. 
# 
# Written by: Alex K. Chew (10/24/2019)
#
# DEFINING VARIABLES:
#   $1: simulation dir
#   $2: analysis directory
#   $3: hydra file name
#   $4: output willard channel file
#   $5: prefix, e.g. sam_prod...
#   $6: max_cores
#   $7: initial frame
#   $8: final frame

################################
### LOADING GLOBAL VARIABLES ###
################################
dir_loc="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "${dir_loc}/bin/bashrc.sh"

#########################
### INITIAL VARIABLES ###
#########################

## DEFINING WORKING DIRECTORY
path_sim_dir="$1"
# "/home/shared/np_hydrophobicity_project/simulations/NP_SPHERICAL/MostlikelynpNVT_EAM_300.00_K_2_nmDIAM_C11CONH2_CHARMM36jul2017_Trial_1_likelyindex_1"

## DEFINING ANALYSIS DIRECTORY
analysis_dir="${2-hyd_analysis}"

## DEFINING HYDRA FILE
output_hydra_dir="${3-grid-0_1000-0.1}"
# "hydra-0_50000-0.1"

## DEFINING OUTPUT WILLARD CHANDLER
output_willard_chandler="${4-out_willard_chandler.dat}"

## DEFINING GRO AND TPR
prefix="${5-sam_prod-0_50000-watO}"
tpr_file="${prefix}.tpr"
gro_file="${prefix}.gro"
xtc_file="${prefix}.xtc"

## DEFINING NUMBER OF CORES
max_cores="${6:-8}"
# "28"
# "4"
# "2"

## DEFINING INITIAL AND FINAL FRAME
initial_frame="${7-0}"
final_frame="$8" # ${8-50000}

## GETTING FINAL FRAMES WITH DECIMALS
final_frame_decimals="$(printf "%0.3f\n" ${final_frame})"

## DEFINING CUTOFF
cutoff="0.33" # nanometers
selection_name="all and within ${cutoff} of "
# selection_name="resname SOL and name OW and within ${cutoff} of "

########################
### OUTPUT VARIABLES ###
########################

## DEFINING DIRECTORY
log_dir="gmx_select_output"

## DEFINING LOG FILE
output_log_list="gmx_selection_log_list.txt"
output_log_complete="gmx_selection_complete_list.txt"

## PATH
path_log="${path_sim_dir}/${analysis_dir}/${log_dir}"
## DEFINING PATH LOG SIM
path_log_sim="${path_log}/output"
path_log_list="${path_log}/${output_log_list}"
path_log_complete="${path_log}/${output_log_complete}"
path_willard="${path_sim_dir}/${analysis_dir}/${output_hydra_dir}"

## CHECKING SIMS
stop_if_does_not_exist "${path_sim_dir}"
## GOING TO DIRECTORY
cd "${path_sim_dir}"

## LOOPING AND CREATING A LOG FILE
if [ ! -e "${path_log}" ]; then
    mkdir -p "${path_log}"
fi

## CREATING SIM FILE
if [ ! -e "${path_log_sim}" ]; then
    mkdir -p "${path_log_sim}"
fi

## CREATING LOG DETAILS
stop_if_does_not_exist "${path_willard}/${output_willard_chandler}"

## CREATING A LOG FILE
if [ ! -e "${path_log_list}" ]; then
    ## PRINTING CREATING LOG FILE
    echo "Creating log file at: ${path_log_list}"
    ## CREATING INDEX
    index=0
    ## LOOPING THROUGH EACH LINE
    for each_line in $(tail -n +3 "${path_willard}/${output_willard_chandler}"); do

        ## DEFINING OUTPUT FILE NAME
        output_size_name="${job_index}_size.xvg"
        
        ## PRINTING TO LOG FILE IF THE FILE DOES NOT EXIST
        if [ ! -e "${path_log_sim}/${output_size_name}" ]; then
            echo "${index} ${each_line}" >> "${path_log_list}"
        fi
        index=$((${index}+1))
    done
fi

## FINDING TOTAL NUMBER OF ATOMS
total_grid_points=$(($(cat "${path_willard}/${output_willard_chandler}" | wc -l)-3))

## COMPUTING NUMBER OF JOBS IN QUEUE
num_jobs_in_queue=$(ps -U $USER | grep 'gmx' | wc -l)

# Sleep times
sleepTime="0" # 1
# 10 # Seconds for sleep in until loop

# Using an until loop to keep running the script
until [[ -z $(head -1 "${path_log_list}") ]]; do 
    ## CHECKING IF USER IS AVAILABLE
    while [[ -z "$USER" ]]; do
        echo "USER not defined, sleeping until this is true. "
        sleep 1
    done
    ## COMPUTING NUMBER OF JOBS IN QUEUE
    num_jobs_in_queue=$(ps -U $USER | grep 'gmx' | wc -l)
    
    ## LOOPING THROUGH ALL NUMBER OF JOBS
    while [[ "${num_jobs_in_queue}" -lt "${max_cores}" ]]; do
        ## GETTING FIRST LINE
        first_line=$(head -1 "${path_log_list}")
        
        ## CHECKING IF FIRST LINE IS THERE
        if [ -z "${first_line}" ]; then
            echo "No more jobs in ${path_log_list}"
            echo "Stopping script here!"
            exit
        fi
        ## GETTING THE INDEX
        job_index=$(awk '{print $1}' <<< "${first_line}")
        job_position=$(awk '{print $2}' <<< "${first_line}")
        # "$(echo ${first_line} | )"
        
        ## DEFINING OUTPUT FILE NAME
        output_size_name="${job_index}_size.xvg"
        # output_size_log=">/dev/null"
        # "${job_index}_size.log"
        
        ## DEFINING RUN CODE
        run_code=false
        
        ## CHECKING IF FILE EXISTS
        if [ ! -e "${path_log_sim}/${output_size_name}" ]; then
            run_code=true
        else
            ## CHECK IF LAST FRAME MATCHES
            last_frame=$(tail -n1 "${path_log_sim}/${output_size_name}"| awk '{print $1}')
            ## IF LAST FRAME DOESN'T MATCH DESIRED, THEN RERUN THE CODE
            if [[ "${last_frame}" != "${final_frame_decimals}" ]]; then
                run_code=true
            fi  
        fi
        
        ## CHECKING IF RUN CODE IS TRUE
        if [[ "${run_code}" == true ]]; then
            ## PRINTING
            echo "Running ${output_size_name} out of ${total_grid_points} total grid points"
            ## RUNNING GMX SELECT "${path_log_sim}/${output_size_log}"
            gmx select -f "${xtc_file}" -s "${tpr_file}" -b "${initial_frame}" -e "${final_frame}" -select "${selection_name} [${job_position}]" -os "${path_log_sim}/${output_size_name}" > /dev/null 2>&1 &
        else
            echo "${output_size_name} out of ${total_grid_points} is complete!"
        fi
        
        
        ## COPYING FIRST LINE TO COMPLETION
        echo "${first_line}" >> ${path_log_complete}

        ## DELETING JOB TO AVOID RE-DOING JOB
        sed -i "1d" "${path_log_list}"
        
        ## GETTING NUMBER OF JOBS IN QUEUE
        num_jobs_in_queue=$(ps -U $USER | grep 'gmx' | wc -l)
        
        ## SLEEPING TO TEST
        # echo "Sleeping for 5 seconds ..."
        # sleep 5
    done
    
# echo "Maxed queue (${max_cores}) -- Sleeping for ${sleepTime} at $(date)"
sleep ${sleepTime}

done

## AFTER COMPLETE, WAIT FOR COMMANDS TO FINISH
wait


